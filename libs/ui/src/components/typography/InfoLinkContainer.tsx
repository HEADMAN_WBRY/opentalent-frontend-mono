import InfoIcon from '@mui/icons-material/Info';
import { Grid } from '@mui/material';
import { makeStyles } from '@mui/styles';
import React from 'react';

type InfoLinkProps = React.PropsWithChildren<{
  centered?: boolean;
}>;

const useStyles = makeStyles((theme) => ({
  info: {
    fontStyle: 'italic',
    justifyContent: ({ centered }: InfoLinkProps) =>
      centered ? 'center' : 'inherit',

    '& svg': {
      color: theme.palette.tertiary.main,
    },
  },

  iconContainer: {
    display: 'flex',
    alignItems: 'center',
  },

  icon: {
    width: 16,
    height: 16,
  },
}));

const InfoLinkContainer = (props: InfoLinkProps) => {
  const { children } = props;
  const classes = useStyles(props);

  return (
    <Grid className={classes.info} container wrap="nowrap" spacing={1}>
      <Grid className={classes.iconContainer} item>
        <InfoIcon className={classes.icon} fontSize="small" />
      </Grid>
      <Grid item>{children}</Grid>
    </Grid>
  );
};

export default InfoLinkContainer;
