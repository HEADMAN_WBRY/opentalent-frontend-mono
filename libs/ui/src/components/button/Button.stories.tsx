import Box from '@mui/material/Box';
import Grid from '@mui/material/Grid';
import React from 'react';

import Button, { ButtonProps } from './Button';

export default {
  title: 'Components/Button',
  component: Button,
};

const renderButtons = (
  color: ButtonProps['color'],
  variant: ButtonProps['variant'],
) => (
  <Grid container spacing={3}>
    {['small', 'medium', 'large'].map((size) => (
      <Grid key={size} item>
        <Button
          size={size as ButtonProps['size']}
          variant={variant}
          color={color}
        >
          {variant}
        </Button>
      </Grid>
    ))}
    <Grid item>
      <Button size="large" disabled variant={variant} color={color}>
        Disabled {variant}
      </Button>
    </Grid>
  </Grid>
);

export const Default = () => (
  <>
    {renderButtons('primary', 'contained')}
    <br />
    {renderButtons('secondary', 'contained')}
    <br />
    <Box padding="20px" bgcolor="darkgray">
      {renderButtons('primary', 'outlined')}
    </Box>
    <br />
    {renderButtons('secondary', 'outlined')}
  </>
);
