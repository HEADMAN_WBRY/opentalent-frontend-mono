import { makeStyles } from '@mui/styles';

const useStyles = makeStyles((theme) => ({
  moreButton: {
    color: theme.palette.tertiary.main,
    cursor: 'pointer',
  },
}));

export default useStyles;
