import { Box, Grid } from '@mui/material';
import { Formik } from 'formik';
import React from 'react';
import { noop } from 'utils/common';
import * as yup from 'yup';

import Button from '@libs/ui/components/button';
import SnackbarProvider from '@libs/ui/components/snackbar/SnackbarProvider';

import ConnectedImageUpload from './ConnectedImageUpload';
import ImageUpload from './ImageUpload';

export default {
  title: 'Form/ImageUpload',
  component: ImageUpload,
};

export const Default = () => {
  return (
    <SnackbarProvider>
      <Box mb={2}>
        <ImageUpload label="Upload avatar" />
      </Box>
    </SnackbarProvider>
  );
};

const validation = yup.object().shape({
  image: yup.string().required(),
});

export const BasicConnectedDropzone = () => {
  return (
    <SnackbarProvider>
      <Formik
        onSubmit={noop}
        validationSchema={validation}
        initialValues={{ image: null, alo: null }}
      >
        {({ handleSubmit }) => {
          return (
            <Grid direction="column" container>
              <Grid item>
                <ConnectedImageUpload name="image" />
              </Grid>
              <br />
              <Grid item>
                <Button variant="contained" onClick={handleSubmit}>
                  Submit
                </Button>
              </Grid>
            </Grid>
          );
        }}
      </Formik>
    </SnackbarProvider>
  );
};
