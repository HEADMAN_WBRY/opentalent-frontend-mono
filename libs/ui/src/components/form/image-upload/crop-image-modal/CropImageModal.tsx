import {
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Grid,
  Slider,
} from '@mui/material';
import React, { useCallback, useState } from 'react';
import Cropper from 'react-easy-crop';

import Button from '@libs/ui/components/button';

import useStyles from './styles';
import getCroppedImg from './utils';

interface CropImageModalProps {
  open: boolean;
  handleClose: VoidFunction;
  image: string;
  changeImage: any;
}

const CropImageModal = (props: CropImageModalProps) => {
  const classes = useStyles(props);
  const { open, handleClose, image, changeImage } = props;
  const [crop, onCropChange] = useState({ x: 0, y: 0 });
  const [zoom, onZoomChange] = useState(1);
  const [croppedAreaPixels, setCroppedAreaPixels] = useState(null);

  const onCropComplete = useCallback((croppedArea, cap) => {
    setCroppedAreaPixels(cap);
  }, []);
  const getCroppedImage = useCallback(async () => {
    try {
      const finalImage = await getCroppedImg(image, croppedAreaPixels as any);
      changeImage({
        target: { value: [finalImage] },
        preventDefault: () => undefined,
        persist: () => undefined,
      });
      handleClose();
    } catch (e) {
      // eslint-disable-next-line no-console
      console.error(e);
    }
  }, [changeImage, croppedAreaPixels, handleClose, image]);

  return (
    <Dialog
      classes={{ paper: classes.modal }}
      open={open}
      onClose={handleClose}
    >
      <DialogTitle>Move and scale to crop</DialogTitle>
      <DialogContent>
        <Grid spacing={4} direction="column" container>
          <Grid className={classes.cropContainer} item>
            <Cropper
              image={image}
              crop={crop}
              zoom={zoom}
              onCropChange={onCropChange}
              onZoomChange={onZoomChange}
              cropShape="round"
              cropSize={{ width: 200, height: 200 }}
              minZoom={0.5}
              onCropComplete={onCropComplete}
            />
          </Grid>
          <Grid item>
            <Slider
              className={classes.slider}
              color="secondary"
              value={zoom}
              min={0.5}
              max={3}
              step={0.1}
              onChange={(e, z) => onZoomChange(z as number)}
            />
          </Grid>
        </Grid>
      </DialogContent>
      <DialogActions>
        <Button
          fullWidth
          onClick={handleClose}
          variant="outlined"
          color="secondary"
        >
          Cancel
        </Button>
        <Button
          fullWidth
          onClick={getCroppedImage}
          variant="contained"
          color="primary"
          autoFocus
        >
          Choose
        </Button>
      </DialogActions>
    </Dialog>
  );
};

export default CropImageModal;
