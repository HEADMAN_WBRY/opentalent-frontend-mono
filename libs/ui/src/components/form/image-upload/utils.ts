export const getBase64Img = (
  files: FileList | null | undefined | File[],
): Promise<string> => {
  if (files?.[0]) {
    return new Promise((res, rej) => {
      const reader = new FileReader();

      reader.onload = (e) => {
        res((e?.target?.result as string) ?? '');
      };

      reader.onerror = (e) => {
        rej(e);
      };

      reader.readAsDataURL(files[0]);
    });
  }

  return Promise.resolve('');
};
