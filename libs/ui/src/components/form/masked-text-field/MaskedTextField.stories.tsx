import { Box, Grid } from '@mui/material';
import { Formik } from 'formik';
import React from 'react';
import { noop } from 'utils/common';
import * as yup from 'yup';

import Button from '@libs/ui/components/button';

import ConnectedMaskedTextField from './ConnectedMaskedTextField';
import MaskedTextField from './MaskedTextField';

export default {
  title: 'Form/MaskedTextField',
  component: MaskedTextField,
};

export const Default = () => {
  return (
    <Box mb={2}>
      <MaskedTextField
        variant="filled"
        label="Some masked field"
        maskedProps={{ mask: '9999' }}
      />
    </Box>
  );
};

const phoneRegExp =
  /^(\+1\s?)?((\([0-9]{3}\))|[0-9]{3})[\s-]?[0-9]{3}[\s-]?[0-9]{4}$/;
const validation = yup.object().shape({
  phone: yup
    .string()
    .nullable()
    .matches(phoneRegExp, 'Phone number is not valid')
    .required('Required'),
});

export const BasicConnectedMaskedTextField = () => {
  return (
    <>
      <Box mb={2}>
        <Formik
          onSubmit={noop}
          validationSchema={validation}
          initialValues={{ phone: null }}
        >
          {({ handleSubmit }) => {
            return (
              <Grid direction="column" container>
                <Grid item>
                  <ConnectedMaskedTextField
                    label="Connected"
                    name="phone"
                    helperText="Phone"
                    variant="filled"
                    maskedProps={{ mask: '+9 (999) 999-9999' }}
                  />
                </Grid>
                <br />
                <Grid item>
                  <Button variant="contained" onClick={handleSubmit}>
                    Submit
                  </Button>
                </Grid>
              </Grid>
            );
          }}
        </Formik>
      </Box>
    </>
  );
};
