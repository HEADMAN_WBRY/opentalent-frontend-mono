import { Box, Grid } from '@mui/material';
import { Formik } from 'formik';
import React from 'react';
import { noop } from 'utils/common';
import * as yup from 'yup';

import Button from '@libs/ui/components/button';

import ConnectedTextField from './ConnectedTextField';
import TextField from './TextField';

export default {
  title: 'Form/TextField',
  component: TextField,
};

export const DefaultTextField = () => {
  return (
    <>
      <Box mb={2}>
        <TextField label="Line" helperText="Line" />
      </Box>
      <Box mb={2}>
        <TextField label="Outlined" variant="outlined" helperText="Outlined" />
      </Box>
      <Box mb={2}>
        <TextField label="Filled" variant="filled" helperText="Filled" />
      </Box>
      <Box mb={2}>
        <TextField disabled label="Line" helperText="Line" />
      </Box>
      <Box mb={2}>
        <TextField
          disabled
          label="Outlined"
          variant="outlined"
          helperText="Outlined"
        />
      </Box>
      <Box mb={2}>
        <TextField
          disabled
          label="Filled"
          variant="filled"
          helperText="Filled"
        />
      </Box>
      <Box mb={2}>
        <TextField error label="Line" helperText="Line" />
      </Box>
      <Box mb={2}>
        <TextField
          error
          label="Outlined"
          variant="outlined"
          helperText="Outlined"
        />
      </Box>
      <Box mb={2}>
        <TextField error label="Filled" variant="filled" helperText="Filled" />
      </Box>
    </>
  );
};

export const DenseTextFields = () => {
  return (
    <>
      <Box mb={2}>
        <TextField margin="dense" label="Line" helperText="Line" />
      </Box>
      <Box mb={2}>
        <TextField
          margin="dense"
          label="Outlined"
          variant="outlined"
          helperText="Outlined"
        />
      </Box>
      <Box mb={2}>
        <TextField
          margin="dense"
          label="Filled"
          variant="filled"
          helperText="Filled"
        />
      </Box>
      <Box mb={2}>
        <TextField disabled margin="dense" label="Line" helperText="Line" />
      </Box>
      <Box mb={2}>
        <TextField
          disabled
          margin="dense"
          label="Outlined"
          variant="outlined"
          helperText="Outlined"
        />
      </Box>
      <Box mb={2}>
        <TextField
          disabled
          margin="dense"
          label="Filled"
          variant="filled"
          helperText="Filled"
        />
      </Box>
      <Box mb={2}>
        <TextField error margin="dense" label="Line" helperText="Line" />
      </Box>
      <Box mb={2}>
        <TextField
          error
          margin="dense"
          label="Outlined"
          variant="outlined"
          helperText="Outlined"
        />
      </Box>
      <Box mb={2}>
        <TextField
          error
          margin="dense"
          label="Filled"
          variant="filled"
          helperText="Filled"
        />
      </Box>
    </>
  );
};

const validation = yup.object().shape({
  email: yup.string().email('Invalid email').required('Required'),
});

export const BasicConnectedTextField = () => {
  return (
    <Formik
      onSubmit={noop}
      validationSchema={validation}
      initialValues={{ email: null }}
    >
      {({ handleSubmit }) => {
        return (
          <Grid direction="column" container>
            <Grid item>
              <ConnectedTextField
                label="Connected"
                name="email"
                helperText="Email"
              />
            </Grid>
            <br />
            <Grid item>
              <Button variant="contained" onClick={handleSubmit}>
                Submit
              </Button>
            </Grid>
          </Grid>
        );
      }}
    </Formik>
  );
};
