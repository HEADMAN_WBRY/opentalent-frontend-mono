declare module 'material-ui-phone-number' {
  interface DefaultPhoneInputProps {
    excludeCountries: string[];
    onlyCountries: string[];
    preferredCountries: string[];
    defaultCountry: string;
  }

  export type PhoneFieldInputProps = TextFieldProps & DefaultPhoneInputProps;

  export default function TextField(props: PhoneFieldInputProps): JSX.Element;
}
