export type BGVariant = 'primary';
export interface TabItemsType
  extends React.PropsWithChildren<{ id: string; bg?: BGVariant }> {}
