import { SimplePaletteColorOptions } from '@mui/material';

import type { Theme as DefaultThemeType } from './default';

declare module '@mui/material' {
  interface Palette {
    other: Record<string, string>;
    tertiary: SimplePaletteColorOptions;
    green: SimplePaletteColorOptions;
  }
  interface PaletteOptions {
    other: Record<string, string>;
    tertiary: SimplePaletteColorOptions;
    green: SimplePaletteColorOptions;
  }
}

declare module '@mui/private-theming/defaultTheme' {
  // eslint-disable-next-line @typescript-eslint/no-empty-interface
  interface DefaultTheme extends DefaultThemeType {}
}

// declare module '@mui/styles/defaultTheme' {
//   // eslint-disable-next-line @typescript-eslint/no-empty-interface
//   interface DefaultTheme extends Theme {}
// }

export { default as defaultTheme } from './default';
export type { Theme as DefaultThemeType } from './default';

export { default as partialNewTheme } from './partial-new';
export type { Theme as PartialNewType } from './partial-new';

export { default as v5Theme } from './v5-theme';
export type { Theme as V5ThemeType } from './v5-theme';

export { makeStyles } from '@mui/styles';
