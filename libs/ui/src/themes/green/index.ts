import { mergeDeep } from '@apollo/client/utilities';

import { createTheme } from '@mui/material/styles';

import { themeOptions } from '../v5-theme';
import { PALETTE } from './palette';

const theme = createTheme(mergeDeep({ ...themeOptions, palette: PALETTE }));

export default theme;
export type Theme = typeof theme;
