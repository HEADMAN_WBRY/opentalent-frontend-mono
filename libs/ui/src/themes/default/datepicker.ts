import { PALETTE } from './palette';
import { TYPOGRAPHY_THEME_OPTIONS } from './typography';

export const DATEPICKER_THEME_OPTIONS = {
  MuiPickersDay: {
    current: {
      ...TYPOGRAPHY_THEME_OPTIONS.button,
      border: `1px solid ${PALETTE.other['light']}`,
      background: PALETTE.other['gray'],
      color: PALETTE.text?.primary,
    },
    daySelected: {
      borderColor: 'transparent',
    },
  },
};
