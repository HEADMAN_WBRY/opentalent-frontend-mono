import { format, parseISO } from 'date-fns';

export const DEFAULT_DATE_SCHEME = 'dd/MM/yyyy';

export const formatDateDefault = (date: string | Date) => {
  const finalDate = typeof date === 'string' ? parseISO(date) : date;

  return format(finalDate, DEFAULT_DATE_SCHEME);
};
