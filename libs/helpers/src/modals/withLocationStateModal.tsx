/* eslint-disable func-names */
import React from 'react';
import { useHistory } from 'react-router-dom';

import { DefaultModalProps } from './types';

interface WithLocationStateModalArgs {
  id: string;
  renderAlways?: boolean;
}

// eslint-disable-next-line @typescript-eslint/no-unnecessary-type-constraint
export function withLocationStateModal<S extends unknown>({
  id,
  renderAlways = false,
}: WithLocationStateModalArgs) {
  return function <T extends DefaultModalProps<S>>(
    Component: React.ComponentType<T>,
  ) {
    return (props: Omit<T, 'isOpen' | 'close' | 'modalData'>) => {
      const history = useHistory<Record<string, S>>();
      const modalData = history.location.state?.[id];
      const isOpen = !!modalData;
      const close = () =>
        history.replace({
          ...history.location,
          state: { [id]: null } as Record<string, S>,
        });

      const ModalComponent = Component as React.ComponentType<
        DefaultModalProps<S>
      >;

      if (!renderAlways && !isOpen) {
        return null;
      }

      return (
        <ModalComponent
          {...props}
          isOpen={isOpen}
          modalData={modalData}
          close={close}
        />
      );
    };
  };
}
