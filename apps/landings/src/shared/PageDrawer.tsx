import React from 'react';

import CloseIcon from '@mui/icons-material/Close';
import PersonOutlineIcon from '@mui/icons-material/PersonOutline';
import {
  Box,
  Button,
  Drawer,
  Grid,
  IconButton,
  Typography,
} from '@mui/material';
import { makeStyles } from '@mui/styles';

import { EXTERNAL_LINKS } from '@libs/helpers/consts';

import { ReactComponent as LogoIcon } from '../assets/opentalent_dark.svg';

// import { LINKS_LIST_1, LINKS_LIST_2 } from './common-footer/consts';
// import LinkList from './corp-landing/shared/LinkList';

interface PageDrawerProps {
  isOpen: boolean;
  toggle: VoidFunction;
  isForTalent?: boolean;
  className?: string;
}

const useStyles = makeStyles((theme) => ({
  wrapper: {
    background: theme.palette.secondary.main,
    color: 'white',
    width: '90vw',
    maxWidth: '320px',
    padding: `${theme.spacing(4)} ${theme.spacing(5)}`,
  },
  logoWrapper: {
    color: theme.palette.primary.main,
  },
}));

const PageDrawer: React.FC<PageDrawerProps> = ({
  isOpen,
  toggle,
  isForTalent,
  className,
}) => {
  const classes = useStyles();

  return (
    <Drawer
      classes={{ paper: classes.wrapper, root: className }}
      anchor="left"
      open={isOpen}
      onClose={toggle}
    >
      <Grid container>
        <Grid item>
          <IconButton size="small" onClick={toggle} style={{ color: 'white' }}>
            <CloseIcon />
          </IconButton>
        </Grid>
        <Grid className={classes.logoWrapper} item component={Box} mb={-2}>
          <LogoIcon height={35} />
        </Grid>
      </Grid>

      <Box pb={8} pt={8}>
        <Typography variant="h5">Main menu</Typography>

        <Box pt={4}>
          <Button
            href={
              isForTalent
                ? 'https://opentalent.co'
                : 'https://opentalent.co/network'
            }
            color="inherit"
          >
            {isForTalent ? 'for companies' : 'JOIN OUR TALENT COMMUNITY'}
          </Button>
        </Box>
        {!isForTalent && (
          <>
            <Box>
              <Button href={EXTERNAL_LINKS.bookMeeting} color="inherit">
                Demo
              </Button>
            </Box>
            <Box>
              <Button href={EXTERNAL_LINKS.pricing} color="inherit">
                Pricing
              </Button>
            </Box>
          </>
        )}
        <Box>
          <Button
            href="https://app.opentalent.co"
            variant="text"
            color="inherit"
            startIcon={<PersonOutlineIcon />}
          >
            Sign in
          </Button>
        </Box>
      </Box>
      {/*
      <Box pb={4}>
        <Typography variant="h5">Contact Us</Typography>

        <LinkList list={LINKS_LIST_1} />
      </Box>
      <Box pb={4}>
        <Typography variant="h5">Company</Typography>

        <LinkList list={LINKS_LIST_2} />
      </Box> */}
    </Drawer>
  );
};

export default PageDrawer;
