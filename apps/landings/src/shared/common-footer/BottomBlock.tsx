/* eslint-disable jsx-a11y/accessible-emoji */
import { getYear } from 'date-fns';
import React from 'react';

import { Grid } from '@mui/material';
import { makeStyles } from '@mui/styles';

import { useMediaQueries } from '@libs/helpers/hooks/media-queries';
import Typography from '@libs/ui/components/typography';

interface BottomBlockProps {}

const useStyles = makeStyles((theme) => ({
  container: {},
  firstItem: {
    color: theme.palette.grey[500],

    [theme.breakpoints.down('md')]: {
      flexGrow: 1,
      justifyContent: 'center',
      display: 'flex',
    },
  },
  thirdItem: {
    [theme.breakpoints.down('md')]: {
      width: '100%',
      flexGrow: 1,
      marginTop: theme.spacing(4),
      justifyContent: 'center',
      display: 'flex',
    },
  },

  crosses: {
    fontSize: 10,
  },
  privacyLink: {
    color: `${theme.palette.common.white} !important`,
  },
}));

const BottomBlock = (props: BottomBlockProps) => {
  const { isXS } = useMediaQueries();
  const classes = useStyles();

  return (
    <Grid justifyContent="space-between" container>
      <Grid className={classes.firstItem} item>
        <Typography align="center" variant="body2">
          OpenTalent is proudly designed, built and continually enhanced with
          its{' '}
          <Typography
            // href="https://www.notion.so/opentalent/8169d291d1d24affa9be3a82bd39c164?v=04824516c655489dbd64fe469d847517"
            variant="body2"
            color="primary"
            component="span"
          >
            global remote team.
          </Typography>
        </Typography>
      </Grid>

      <Grid className={classes.thirdItem} item>
        <Typography align={isXS ? 'center' : 'inherit'} variant="body2">
          Copyright © {getYear(new Date())} Open Technologies B.V.
        </Typography>
      </Grid>
    </Grid>
  );
};

export default BottomBlock;
