import { Box } from '@mui/material';
import { makeStyles } from '@mui/styles';
import cn from 'classnames';
import React from 'react';

import Button from '@libs/ui/components/button';
import Typography from '@libs/ui/components/typography';

const useStyles = makeStyles((theme) => ({
  wrapper: {
    padding: '80px 16px',
    textAlign: 'center',
    backgroundColor: theme.palette.secondary.dark,
    color: theme.palette.secondary.contrastText,

    [theme.breakpoints.down('md')]: {
      padding: '56px 16px',
    },

    '& > button': {
      [theme.breakpoints.down('md')]: {
        width: '100%',
      },
    },
  },
  title: {
    lineHeight: '40px',
    fontWeight: 600,
    fontStyle: 'italic',
    marginBottom: theme.spacing(16),
    whiteSpace: 'pre-line',

    [theme.breakpoints.down('sm')]: {
      fontSize: '20px',
      lineHeight: '28px',
      marginBottom: 34,
    },
  },
}));

interface IProps {
  title: string;
  btnText: React.ReactNode;
  className?: string;
}

const Join: React.FC<IProps> = ({ title, btnText, className }) => {
  const classes = useStyles();
  const scrollToTop = () => {
    window.scrollTo({
      top: 0,
      behavior: 'smooth',
    });
  };

  return (
    <Box className={cn(classes.wrapper, className)}>
      <Typography className={classes.title} transform="uppercase" variant="h4">
        {title}
      </Typography>
      <Button
        variant="contained"
        color="primary"
        onClick={scrollToTop}
        size="large"
      >
        {btnText}
      </Button>
    </Box>
  );
};

export default Join;
