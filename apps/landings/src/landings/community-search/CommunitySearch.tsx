import React from 'react';
import { useTitle } from 'react-use';

import { makeStyles } from '@mui/styles';

import { TalentsCountItem } from '@libs/graphql-types';

import Footer from '../../shared/common-footer';
import { useAppInfo } from '../companies-landing/hooks';
import Action from './action';
import Countries from './countries';
import Header from './header';
import Help from './help';
import Intro from './intro';
import SeniorRecruiter from './senior-recruiter';
import Values from './values';

interface CommunitySearchProps {}

const useStyles = makeStyles((theme) => ({
  wrapper: {
    width: '100vw',
    overflow: 'hidden',

    '& .MuiButton-root': {
      borderRadius: 4,
      textTransform: 'none',
    },
  },
  '@global': {
    body: {
      width: '100%',
    },

    '.MuiButton-root': {
      color: theme.palette.green.light,
      transition: 'all 0.3s ease-in-out',
    },

    '.MuiButton-containedSecondary': {
      '&:hover': {
        background: theme.palette.green.main,
        color: theme.palette.secondary.main,
      },
    },
  },
  '@font-face': {
    fontFamily: 'MissRhinetta',
    src: 'url(assets/fonts/Miss-Rhinetta.otf)',
  },

  join: {
    background: 'white',
    color: theme.palette.text.primary,

    '& button': {
      boxShadow: '0px 8px 16px rgba(0, 0, 0, 0.14) !important',
    },
  },
}));

const CommunitySearch = (props: CommunitySearchProps) => {
  const classes = useStyles();
  const { data = {} } = useAppInfo();
  const { commonAppInfo, talentsCountByCategories } = data;
  const counts = (talentsCountByCategories || []) as TalentsCountItem[];

  useTitle('Community-powered Recruitment');

  return (
    <div className={classes.wrapper}>
      <Header />
      <Intro appInfo={commonAppInfo} />
      <Countries counts={counts} appInfo={commonAppInfo} />
      <Help
        countriesCount={commonAppInfo?.total_ot_freelancers_countries_count}
      />
      <Values />
      <SeniorRecruiter />
      <Action />
      <Footer />
    </div>
  );
};

export default CommunitySearch;
