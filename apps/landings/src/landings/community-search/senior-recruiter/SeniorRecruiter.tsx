import React from 'react';

import { Grid, Button, Hidden } from '@mui/material';
import { makeStyles } from '@mui/styles';

import Typography from '@libs/ui/components/typography';

import Section from '../../../shared/new-landings/section';
import SectionTitle from '../../../shared/new-landings/section-title';
import { paths } from '../../../utils/consts';
import image from '../assets/recruiter@2x.png';

interface SeniorRecruiterProps {}

const useStyles = makeStyles((theme) => ({
  title: {
    display: 'block',
    marginBottom: theme.spacing(8),
    textAlign: 'left',

    [theme.breakpoints.down('md')]: {
      fontSize: 20,
      lineHeight: '26px',
      marginBottom: theme.spacing(8),
      textAlign: 'center',
    },
  },
  wrapper: {
    overflow: 'hidden',
    maxWidth: 900,
    margin: '0 auto',

    [theme.breakpoints.down('md')]: {
      padding: 0,
    },

    '& > div': {
      maxWidth: 1000,
      margin: '0 auto',
      display: 'flex',
    },
  },
  content: {
    flexGrow: 1,
    maxWidth: '50%',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',

    [theme.breakpoints.down('md')]: {
      maxWidth: '100%',
      paddingBottom: `${theme.spacing(0)} !important`,
    },
  },
  text: {
    maxWidth: 396,
    marginBottom: theme.spacing(8),

    [theme.breakpoints.down('md')]: {
      maxWidth: '100%',
      textAlign: 'center',
      fontSize: 14,
      lineHeight: '20px',
      marginBottom: 16,
    },
  },
  pictureSection: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    textAlign: 'right',
    alignItems: 'center',

    [theme.breakpoints.down('md')]: {
      flexGrow: 1,
      paddingTop: 28,
      textAlign: 'center',
      paddingBottom: theme.spacing(6),
    },

    '& img': {
      maxWidth: 330,
      marginBottom: theme.spacing(5),

      [theme.breakpoints.down('sm')]: {
        maxWidth: 264,
        marginBottom: theme.spacing(3),
      },
    },
  },
  pictureLabel: {
    maxWidth: 250,
  },

  buttonsWrapper: {
    [theme.breakpoints.down('md')]: {
      flexDirection: 'column',
      width: '100%',
      margin: 0,

      '& > div': {
        width: '100%',
        textAlign: 'center',
        paddingLeft: '0 !important',
      },
    },
  },
}));

const SeniorRecruiter = (props: SeniorRecruiterProps) => {
  const classes = useStyles();

  return (
    <Section color="white">
      <Grid className={classes.wrapper} container>
        <Grid className={classes.content}>
          <SectionTitle
            className={classes.title}
            variant="h4"
            component="i"
            transform="uppercase"
            fontWeight={600}
            whiteSpace="break-spaces"
          >
            {`Want to join our
recruiter community?`}
          </SectionTitle>

          <Typography className={classes.text} variant="body1">
            Are you an experienced recruiter with domain-expertise, who’s
            looking for some extra work? Then we’d love to meet you! Sign up to
            the OpenTalent community to get started.
          </Typography>

          <Hidden mdDown>
            <Grid
              className={classes.buttonsWrapper}
              spacing={4}
              container
              alignItems="center"
            >
              <Grid item>
                <Button
                  size="large"
                  variant="contained"
                  color="secondary"
                  fullWidth
                  href={paths.talentLanding}
                  style={{ width: 240 }}
                >
                  Apply to Join
                </Button>
              </Grid>
            </Grid>
          </Hidden>
        </Grid>
        <Grid className={classes.pictureSection}>
          <img srcSet={`${image} 2x`} alt="Recruiter" />
          <Typography
            align="center"
            className={classes.pictureLabel}
            variant="body2"
          >
            Expert recruiters bring category-specific knowledge + network.
          </Typography>
        </Grid>

        <Hidden mdUp>
          <Grid
            className={classes.buttonsWrapper}
            spacing={4}
            container
            alignItems="center"
          >
            <Grid item>
              <Button
                size="large"
                variant="contained"
                color="secondary"
                fullWidth
                href={paths.talentLanding}
              >
                Apply to Join
              </Button>
            </Grid>
          </Grid>
        </Hidden>
      </Grid>
    </Section>
  );
};

export default SeniorRecruiter;
