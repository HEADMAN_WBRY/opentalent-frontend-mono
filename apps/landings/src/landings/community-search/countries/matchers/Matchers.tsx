import React from 'react';

import { Box, Grid } from '@mui/material';
import { makeStyles } from '@mui/styles';

import { CommonAppInfo } from '@libs/graphql-types';
import { formatNumberSafe } from '@libs/helpers/format';
import Typography from '@libs/ui/components/typography';

import { INFINITY_SIGN } from '../../../../utils/consts';
import MatcherCard from './MatcherCard';
import { CARDS_DATA } from './consts';

interface MatchersProps {
  appInfo?: Partial<CommonAppInfo>;
}

const useStyles = makeStyles((theme) => ({
  title: {
    textTransform: 'uppercase',
    marginBottom: theme.spacing(11),
    fontWeight: 600,
    fontStyle: 'italic',
    textAlign: 'center',

    [theme.breakpoints.down('md')]: {
      fontSize: 18,
      marginBottom: theme.spacing(6),
      lineHeight: '24px',
    },

    '& i': {
      width: '100%',
      textAlign: 'center',
      display: 'block',
      padding: `0 ${theme.spacing(2)}px`,

      [theme.breakpoints.down('md')]: {
        fontSize: 20,
        lineHeight: '26px',
      },
    },
  },
  cardsWrap: {
    [theme.breakpoints.down('md')]: {
      justifyContent: 'center',
    },
  },
  cardItem: {
    marginBottom: theme.spacing(6),

    '&:not(:last-child)': {
      paddingRight: theme.spacing(6),
    },

    [theme.breakpoints.down('sm')]: {
      width: '100%',
      paddingRight: '0 !important',
    },
  },
}));

const Matchers = ({ appInfo }: MatchersProps) => {
  const classes = useStyles();
  const recruiters = formatNumberSafe(appInfo?.total_ot_recruiters_count, {
    fallback: INFINITY_SIGN,
  });
  const countries = formatNumberSafe(
    appInfo?.total_ot_freelancers_countries_count,
    {
      fallback: INFINITY_SIGN,
    },
  );

  return (
    <Box>
      <Typography
        color="secondary.contrastText"
        className={classes.title}
        variant="h6"
        textTransform="uppercase"
      >
        MEET some of our {recruiters} expert recruiters
      </Typography>

      <Grid
        className={classes.cardsWrap}
        justifyContent="space-between"
        container
      >
        {CARDS_DATA.map((data) => (
          <Grid className={classes.cardItem} key={data.name} item>
            <MatcherCard {...data} />
          </Grid>
        ))}
      </Grid>
    </Box>
  );
};

export default Matchers;
