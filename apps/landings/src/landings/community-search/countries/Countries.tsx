import React from 'react';

import { Box, Grid } from '@mui/material';
import { makeStyles } from '@mui/styles';

import { CommonAppInfo, TalentsCountItem } from '@libs/graphql-types';
import { formatNumberSafe } from '@libs/helpers/format';
import Typography from '@libs/ui/components/typography';

import Section from '../../../shared/new-landings/section';
import SectionHeader from '../../../shared/new-landings/section-title';
import Categories from './Categories';
import { COUNTRIES } from './consts';
import Matchers from './matchers/Matchers';

interface CountriesProps {
  appInfo?: Partial<CommonAppInfo>;
  counts: TalentsCountItem[];
}

const useStyles = makeStyles((theme) => ({
  container: {
    padding: `${theme.spacing(20)} 0`,
    width: '100vw',
    overflow: 'hidden',

    [theme.breakpoints.down('md')]: {
      padding: `48px 0 32px`,
    },
  },
  title: {
    textAlign: 'center',
    fontStyle: 'italic',

    [theme.breakpoints.down('md')]: {
      margin: 0,
    },
  },
  subTitle: {
    marginBottom: theme.spacing(10),
    textTransform: 'uppercase',
    whiteSpace: 'break-spaces',

    [theme.breakpoints.down('md')]: {
      marginBottom: theme.spacing(6),
      whiteSpace: 'initial',
    },
  },

  '@keyframes slideRight': {
    from: { transform: 'translateX(0px)' },
    to: { transform: 'translateX(-9000px)' },
  },
  countries: {
    width: 1000000,
    animationName: '$slideRight',
    animationDuration: '240s',
    animationIterationCount: 'infinite',
    animationTimingFunction: 'linear',
    marginBottom: theme.spacing(6),
  },
  country: {
    fontSize: 72,
    lineHeight: '80px',

    [theme.breakpoints.down('md')]: {
      fontSize: 36,
      lineHeight: '48px',
    },
  },
}));

const Countries = ({ appInfo, counts }: CountriesProps) => {
  const classes = useStyles();
  const recruiters = formatNumberSafe(appInfo?.total_ot_recruiters_count);

  return (
    <Section color="grey" classes={{ root: classes.container }}>
      <Box pb={6}>
        <SectionHeader color="secondary.contrastText" className={classes.title}>
          {`Community-powered search`}
        </SectionHeader>

        <Typography
          color="secondary.contrastText"
          textAlign="center"
          className={classes.subTitle}
        >
          {`leverage the collective network of OpenTalent’s
community to find the talent your need in record time.`}
        </Typography>
      </Box>

      <Grid className={classes.countries} wrap="nowrap" spacing={8} container>
        {COUNTRIES.map((i, index) => (
          // eslint-disable-next-line react/no-array-index-key
          <Grid key={index} item>
            <Typography
              className={classes.country}
              paragraph
              transform="uppercase"
              component="i"
              variant="h2"
              fontWeight="bold"
              color="secondary.contrastText"
            >
              {i}
            </Typography>
          </Grid>
        ))}
      </Grid>

      <Box pt={11} mb={10}>
        <Categories counts={counts} />
      </Box>

      <Box>
        <Matchers appInfo={appInfo} />
      </Box>
    </Section>
  );
};

export default Countries;
