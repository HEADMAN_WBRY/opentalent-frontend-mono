import cn from 'classnames';
import React from 'react';

import { Box, Grid } from '@mui/material';
import { makeStyles } from '@mui/styles';

import { useMediaQueries } from '@libs/helpers/hooks/media-queries';
import Button from '@libs/ui/components/button';
import Typography from '@libs/ui/components/typography';

import Section from '../../../shared/new-landings/section';
import SectionTitle from '../../../shared/new-landings/section-title';
import { paths, LANDINGS_EXTERNAL_LINKS } from '../../../utils/consts';

interface ActionProps {}

const useStyles = makeStyles((theme) => ({
  withButtonText: {
    [theme.breakpoints.down('md')]: {
      textAlign: 'center',
    },
  },

  button: {
    transition: 'all 0.3s ease-in-out',
    fontWeight: 'bold',
    minWidth: 210,
  },

  outlinedGreenButton: {
    borderColor: theme.palette.green.dark,
    color: `${theme.palette.green.dark} !important`,
  },

  greenButton: {
    background: theme.palette.green.dark,
    color: `${theme.palette.secondary.main} !important`,

    '&:hover': {
      background: theme.palette.green.light,
    },
  },
}));

const Action = (props: ActionProps) => {
  const { isSM } = useMediaQueries();
  const classes = useStyles();

  return (
    <Section>
      <SectionTitle color="secondary.contrastText">
        {`Find the skills you need - faster!`}
      </SectionTitle>

      <Box pt={isSM ? 4 : 14}>
        <Grid
          justifyContent="center"
          direction={isSM ? 'column' : 'row'}
          spacing={isSM ? 6 : 8}
          container
        >
          <Grid item>
            <Button
              variant="contained"
              className={cn(classes.button, classes.greenButton)}
              fullWidth
              type="submit"
              size="large"
              href={paths.companyOnboarding}
            >
              Post a Job
            </Button>
          </Grid>
          <Grid item>
            <Button
              variant="outlined"
              className={cn(classes.button, classes.outlinedGreenButton)}
              fullWidth
              type="submit"
              size="large"
              href={LANDINGS_EXTERNAL_LINKS.sangibLink}
            >
              Book a Demo
            </Button>
          </Grid>
          <Grid className={classes.withButtonText} item>
            <Typography
              variant="subtitle2"
              color="secondary.contrastText"
              fontWeight={700}
            >
              No Cure No Pay -
            </Typography>
            <Typography variant="subtitle2" color="secondary.contrastText">
              only&nbsp;pay&nbsp;for&nbsp;success!
            </Typography>
          </Grid>
        </Grid>
      </Box>
    </Section>
  );
};

export default Action;
