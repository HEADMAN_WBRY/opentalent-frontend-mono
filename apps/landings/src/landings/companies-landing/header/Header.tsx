import React, { useState } from 'react';

import MenuIcon from '@mui/icons-material/Menu';
import {
  AppBar,
  Box,
  Button,
  Container,
  Grid,
  Hidden,
  IconButton,
  Toolbar,
} from '@mui/material';
import { makeStyles } from '@mui/styles';

import { EXTERNAL_LINKS } from '@libs/helpers/consts';
import { useMediaQueries } from '@libs/helpers/hooks/media-queries';

import { ReactComponent as LogoIcon } from '../../../assets/logo.svg';
import PageDrawer from '../../../shared/PageDrawer';
import { paths } from '../../../utils/consts';

interface HeaderProps {
  isForTalent?: boolean;
}

const useStyles = makeStyles((theme) => ({
  replaceBtn: {
    transition: `color ${theme.transitions.duration.short}s ${theme.transitions.easing.easeInOut}`,

    '&:hover': {
      color: theme.palette.primary.main,
    },
  },
  logoWrapper: {
    display: 'flex',
    alignItems: 'center',
    color: theme.palette.primary.main,
  },
}));

const Header: React.FC<HeaderProps> = ({ isForTalent }) => {
  const [isOpen, setIsOpen] = useState(false);
  const toggle = () => setIsOpen((s) => !s);
  const { isXS } = useMediaQueries();
  const classes = useStyles();

  return (
    <>
      <PageDrawer toggle={toggle} isOpen={isOpen} isForTalent={isForTalent} />
      <AppBar color="secondary" position="static">
        <Box>
          <Container>
            <Toolbar disableGutters>
              <Grid
                justifyContent="space-between"
                alignItems="center"
                container
              >
                <Grid item>
                  <Grid container>
                    {isXS && (
                      <Grid item>
                        <IconButton
                          size="small"
                          onClick={toggle}
                          style={{ color: 'white' }}
                        >
                          <MenuIcon />
                        </IconButton>
                      </Grid>
                    )}
                    <Grid
                      className={classes.logoWrapper}
                      item
                      component={Box}
                      mb={-2}
                    >
                      <a href="/">
                        <LogoIcon height={isXS ? 35 : 54} />
                      </a>
                    </Grid>
                  </Grid>
                </Grid>
                <Hidden smDown>
                  <Grid item>
                    <Grid spacing={4} container>
                      <Grid item>
                        <Button
                          color="inherit"
                          className={classes.replaceBtn}
                          href="/network"
                        >
                          JOIN OUR TALENT COMMUNITY
                        </Button>
                      </Grid>
                      <Grid item>
                        <Button
                          color="inherit"
                          className={classes.replaceBtn}
                          href={EXTERNAL_LINKS.pricing}
                        >
                          PRICING
                        </Button>
                      </Grid>
                      <Grid item>
                        <Button
                          color="inherit"
                          className={classes.replaceBtn}
                          href={EXTERNAL_LINKS.pieterLink}
                        >
                          DEMO
                        </Button>
                      </Grid>
                      <Grid item>
                        <Button
                          href={paths.mainAppRoute}
                          variant="outlined"
                          color="inherit"
                        >
                          Sign in
                        </Button>
                      </Grid>
                    </Grid>
                  </Grid>
                </Hidden>
              </Grid>
            </Toolbar>
          </Container>
        </Box>
      </AppBar>
    </>
  );
};

export default Header;
