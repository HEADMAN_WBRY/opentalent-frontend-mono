import { Box } from '@mui/material';
import { makeStyles } from '@mui/styles';
import React from 'react';

import Typography from '@libs/ui/components/typography';

interface WorkStepProps {
  index: number;
  title: string;
  text: string;
}

const useStyles = makeStyles((theme) => ({
  wrapper: {
    maxWidth: 200,
    textAlign: 'center',
  },
  index: {
    width: 56,
    height: 56,
    border: `1px solid ${theme.palette.primary.main}`,
    borderRadius: '100%',
    display: 'inline-block',
    fontSize: 32,
    lineHeight: '56px',
    paddingRight: 5,
    color: theme.palette.primary.main,
  },
  title: {
    display: 'inline-block',
    marginTop: theme.spacing(6),

    [theme.breakpoints.down('md')]: {
      marginTop: 10,
    },
  },

  text: {
    [theme.breakpoints.down('md')]: {
      lineHeight: '20px',
    },
  },
}));

const WorkStep = ({ index, title, text }: WorkStepProps) => {
  const classes = useStyles();

  return (
    <Box className={classes.wrapper}>
      <Box>
        <i className={classes.index}>{index}</i>
      </Box>
      <Box pb={2}>
        <Typography
          className={classes.title}
          fontWeight={600}
          variant="h6"
          component="i"
        >
          {title}
        </Typography>
      </Box>
      <Box>
        <Typography className={classes.text} variant="body2">
          {text}
        </Typography>
      </Box>
    </Box>
  );
};

export default WorkStep;
