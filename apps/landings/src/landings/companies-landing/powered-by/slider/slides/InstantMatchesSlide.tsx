import React from 'react';

import image from '../../../assets/slider/instant_match.svg';
import SliderItem from '../SliderItem';
import { DefaultSlideProps } from './types';

interface InstantMatchesSlideProps extends DefaultSlideProps {}

export const InstantMatchesSlide = (props: InstantMatchesSlideProps) => {
  return (
    <SliderItem
      {...props}
      img={image}
      title="Instant Matches"
      text="Quickly discover the best-fit candidates in your workforce using OpenTalent’s skills-based and AI-powered search framework."
    />
  );
};
