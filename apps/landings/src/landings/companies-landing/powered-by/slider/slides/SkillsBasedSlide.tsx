import React from 'react';

import image from '../../../assets/slider/skills_based.svg';
import SliderItem from '../SliderItem';
import { DefaultSlideProps } from './types';

interface SkillsBasedSlideProps extends DefaultSlideProps {}

export const SkillsBasedSlide = (props: SkillsBasedSlideProps) => {
  return (
    <SliderItem
      {...props}
      img={image}
      title="Skills-based OS"
      text="Powerful search to find the right candidates in your private workforce, or from the OpenTalent remote network."
    />
  );
};
