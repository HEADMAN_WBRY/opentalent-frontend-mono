import React from 'react';

import image from '../../../assets/slider/direct_communication.svg';
import SliderItem from '../SliderItem';
import { DefaultSlideProps } from './types';

interface DirectCommunicationSlideProps extends DefaultSlideProps {}

export const DirectCommunicationSlide = (
  props: DirectCommunicationSlideProps,
) => {
  return (
    <SliderItem
      {...props}
      img={image}
      title="Direct Communication "
      text="Reach candidates directly though the in-platform messenger system, or connect by e-mail or phone."
    />
  );
};
