import cn from 'classnames';
import React, { useState } from 'react';
import { useInterval } from 'react-use';

import { Slide } from '@mui/material';
import { makeStyles } from '@mui/styles';

import { getNextIndex } from '@libs/helpers/common/moveToIndex';
import Typography from '@libs/ui/components/typography';

interface DynamicTitleProps {}

const VALUES = [
  {
    count: 57,
    title: 'DevOps Engineers',
  },

  {
    count: 42,
    title: 'Salesforce Developers',
  },
  {
    count: 53,
    title: 'Pega Engineers',
  },
  {
    count: 104,
    title: 'Security Leads',
  },
  {
    count: 454,
    title: 'Digital Marketers',
  },
  {
    count: 42,
    title: 'iOS Developers',
  },
  {
    count: 105,
    title: 'Data Scientists',
  },
  {
    count: 135,
    title: 'Data Visualizers',
  },
  {
    count: 81,
    title: 'AI Engineers',
  },
  {
    count: 103,
    title: 'SEO Writers',
  },
  {
    count: 174,
    title: 'UX/UI Designers',
  },
];

const useStyles = makeStyles((theme) => ({
  big: {
    fontSize: 140,
    lineHeight: '110px',
    fontFamily: "'MissRhinetta', sans-serif",
    fontDisplay: 'swap',
    letterSpacing: -1.5,
    position: 'relative',
    perspective: '1000px',
    transformStyle: 'preserve-3d',

    [theme.breakpoints.down('md')]: {
      fontSize: 80,
      lineHeight: '80px',
      maxWidth: 380,
      marginBottom: -25,
      letterSpacing: -0.7,
    },
  },

  dataText: {
    fontFamily: 'Poppins',
    fontSize: 46,
    letterSpacing: 0,
    fontStyle: 'italic',

    [theme.breakpoints.down('md')]: {
      fontSize: 34,
      lineHeight: '42px',
    },
  },
  '@keyframes slideTop': {
    from: { transform: 'translateY(0px) rotateX(0deg)' },
    to: { transform: 'translateY(-32px) rotateX(90deg)' },
  },
  data: {
    whiteSpace: 'nowrap',
    display: 'block',
    position: 'relative',
    top: -28,
    transformStyle: 'inherit',
    transition: 'all .5s',
    backfaceVisibility: 'hidden',
    // animationName: '$slideTop',
    animationDuration: '.5s',
    // animationIterationCount: 'infinite',
    animationTimingFunction: 'linear',
    transform: 'translateY(-32px) rotateX(90deg)',

    [theme.breakpoints.down('md')]: {
      maxWidth: 340,
      top: 0,
    },
  },
  hideDataText: {
    height: 0,
    overflow: 'hidden',
  },
}));

const DynamicTitle = (props: DynamicTitleProps) => {
  const classes = useStyles();
  const containerRef = React.useRef(null);
  const [count, setCount] = useState(0);

  useInterval(() => {
    setCount(getNextIndex(count, VALUES.length));
  }, 3000);

  return (
    <Typography color="inherit" variant="h1">
      <span className={classes.big}>Available Now:</span>
      <br />

      <div style={{ height: 76, overflow: 'hidden' }} ref={containerRef}>
        {VALUES.map((i, index) => (
          <Slide
            key={index}
            timeout={{
              enter: 1000,
              exit: 3000,
            }}
            direction="up"
            in={index === count}
            container={containerRef.current}
          >
            <b
              className={cn(classes.data, classes.dataText, {
                [classes.hideDataText]: index !== count,
              })}
            >
              <Typography
                className={classes.dataText}
                color="primary"
                component="b"
              >
                {i.count}
              </Typography>{' '}
              {i.title}
            </b>
          </Slide>
        ))}
      </div>
    </Typography>
  );
};

export default DynamicTitle;
