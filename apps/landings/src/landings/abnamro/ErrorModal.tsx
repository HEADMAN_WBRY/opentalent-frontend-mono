import {
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Box,
} from '@mui/material';
import { makeStyles } from '@mui/styles';
import cn from 'classnames';
import qs from 'querystring';
import React from 'react';
import { useHistory } from 'react-router-dom';

import Button from '@libs/ui/components/button';

import { ReactComponent as WarningIcon } from './assets/warning.svg';

const useStyles = makeStyles((theme) => ({
  modal: {
    padding: theme.spacing(10),
    textAlign: 'center',
  },
  title: {
    padding: 0,
  },
  actions: {
    paddingTop: theme.spacing(6),
  },
  noHorizontalPadding: {
    paddingLeft: 0,
    paddingRight: 0,
    overflowY: 'visible',
  },
}));

const ErrorModal = () => {
  const classes = useStyles();
  const history = useHistory();
  const { error } = qs.parse(history.location.search.slice(1));
  const handleClose = () => history.push({ search: '' });

  return (
    <Dialog
      classes={{ paper: classes.modal }}
      open={!!error}
      onClose={handleClose}
    >
      <Box textAlign="center">
        <WarningIcon />
      </Box>

      <DialogTitle className={classes.title}>
        Oups! Something went wrong.
      </DialogTitle>
      <DialogContent className={classes.noHorizontalPadding}>
        <div>Sorry, the following error occurred</div>
        <div>{error}</div>
      </DialogContent>
      <DialogActions
        className={cn(classes.actions, classes.noHorizontalPadding)}
      >
        <Button
          fullWidth
          variant="contained"
          color="primary"
          autoFocus
          size="large"
          onClick={handleClose}
        >
          ok
        </Button>
      </DialogActions>
    </Dialog>
  );
};

export default ErrorModal;
