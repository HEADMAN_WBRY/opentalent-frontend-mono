import { Box, Grid, Container } from '@mui/material';
import { makeStyles } from '@mui/styles';
import cn from 'classnames';
import React from 'react';

import { useMediaQueries } from '@libs/helpers/hooks/media-queries';
import Typography from '@libs/ui/components/typography';

import { WHAT_TO_EXPECT_ITEMS, ItemType } from '../consts';

const useStyles = makeStyles((theme) => ({
  wrapper: {
    padding: '100px 0',
    backgroundColor: theme.palette.secondary.dark,
    color: theme.palette.secondary.contrastText,

    [theme.breakpoints.down('md')]: {
      padding: '40px 16px',
    },
  },
  container: {
    '&:not(:last-child)': {
      marginBottom: theme.spacing(30),
    },

    [theme.breakpoints.down('md')]: {
      '&:not(:last-child)': {
        marginBottom: theme.spacing(20),
      },
    },
  },
  miniTitle: {
    fontSize: '12px',
    lineHeight: '20px',
    color: theme.palette.primary.main,
  },
  title: {
    fontSize: '28px',
    lineHeight: '39px',
    marginBottom: theme.spacing(6),
    whiteSpace: 'pre-line',
    fontStyle: 'italic',

    [theme.breakpoints.down('sm')]: {
      fontSize: '20px',
      lineHeight: '28px',
    },
  },
  text: {
    lineHeight: '28px',
    marginBottom: theme.spacing(6),
    maxWidth: '437px',
  },
  ul: {
    listStyleType: 'square',
    paddingLeft: theme.spacing(5),

    '& > li::marker': {
      color: theme.palette.primary.main,
    },
  },
  point: {
    color: theme.palette.grey[500],
    marginBottom: theme.spacing(1),
  },
  img: {
    display: 'flex',
    alignItems: 'center',
    [theme.breakpoints.down('md')]: {
      justifyContent: 'center',
      marginBottom: theme.spacing(10),

      '& > svg': {
        height: 'auto !important',
      },
    },
  },
}));

const WhatToExpect: React.FC = () => {
  const classes = useStyles();
  const { isSM } = useMediaQueries();

  const renderTextSide = (item: ItemType) => (
    <Box>
      <Typography className={classes.miniTitle} variant="subtitle2">
        What To Expect:
      </Typography>
      <Typography className={classes.title} variant="h5" fontWeight="bold">
        {item.title}
      </Typography>
      <Typography className={classes.text}>{item.subtitle}</Typography>
      <ul className={classes.ul}>
        {item.points.map((point, idx) => {
          const key = point + idx;

          return (
            <li key={key}>
              <Typography className={classes.point} fontWeight={500}>
                {point}
              </Typography>
            </li>
          );
        })}
      </ul>
    </Box>
  );

  return (
    <Box className={classes.wrapper}>
      <Container>
        {WHAT_TO_EXPECT_ITEMS.map((item) => (
          <Grid
            container
            key={item.id}
            className={classes.container}
            direction={isSM && !item.revert ? 'column-reverse' : 'row'}
          >
            <Grid
              item
              xs={isSM ? 12 : 7}
              className={cn({ [classes.img]: item.revert })}
            >
              {item.revert ? item.imgComponent : renderTextSide(item)}
            </Grid>
            <Grid
              item
              xs={isSM ? 12 : 5}
              className={cn({ [classes.img]: !item.revert })}
            >
              {item.revert ? renderTextSide(item) : item.imgComponent}
            </Grid>
          </Grid>
        ))}
      </Container>
    </Box>
  );
};

export default WhatToExpect;
