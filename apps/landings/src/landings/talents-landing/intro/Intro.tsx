import { Box, Button, Container, Grid } from '@mui/material';
import { makeStyles } from '@mui/styles';
import { Form, Formik } from 'formik';
import React from 'react';
import * as yup from 'yup';

import { useMediaQueries } from '@libs/helpers/hooks/media-queries';
import { ConnectedTextField } from '@libs/ui/components/form/text-field';
import Typography from '@libs/ui/components/typography';

import image from '../assets/intro_img@2x.png';
import { FORM_ID } from '../consts';
import { useSubmitAction } from './hooks';

interface IntroProps {}

const useStyles = makeStyles((theme) => ({
  '@font-face': {
    fontFamily: 'MissRhinetta',
    src: 'url(assets/fonts/Miss-Rhinetta.otf)',
  },
  wrapper: {
    background: theme.palette.secondary.main,
    color: 'white',

    [theme.breakpoints.down('sm')]: {
      backgroundColor: theme.palette.secondary.dark,
    },
  },
  title: {
    fontSize: 130,
    lineHeight: '86px',
    fontFamily: "'MissRhinetta', sans-serif",
    fontDisplay: 'swap',
    marginBottom: '40px',

    [theme.breakpoints.down('md')]: {
      fontSize: 80,
      lineHeight: '48px',
      maxWidth: 340,
      marginBottom: '24px',
    },

    '& > b': {
      fontFamily: 'Poppins',
      fontStyle: 'italic',
      fontSize: 64,

      [theme.breakpoints.down('md')]: {
        fontSize: 34,
        lineHeight: '54px',
        maxWidth: 340,
      },
    },
  },
  titleLight: {
    color: theme.palette.primary.main,
  },
  subtitle: {
    fontStyle: 'italic',
  },
  container: {
    height: 750,
    display: 'flex',
    alignItems: 'center',
    background: `url(${image}) no-repeat right 12px  center`,
    backgroundSize: '33%',

    [theme.breakpoints.down('md')]: {
      backgroundSize: '40%',
      backgroundPosition: '50% top',
      height: 'auto',
      padding: `0 ${theme.spacing(4)}`,
    },

    [theme.breakpoints.down('sm')]: {
      background: 'unset',
      backgroundSize: 'unset',
      backgroundPosition: 'unset',
    },
  },
  content: {
    [theme.breakpoints.down('md')]: {
      paddingTop: '36vw',
      paddingBottom: theme.spacing(14),
    },

    [theme.breakpoints.down('sm')]: {
      paddingTop: theme.spacing(14),
    },
  },
  info: {
    maxWidth: 640,
    paddingBottom: theme.spacing(10),
    lineHeight: '28px',

    [theme.breakpoints.down('md')]: {
      paddingBottom: theme.spacing(4),
    },
  },
  input: {
    '& > .MuiInputBase-root': {
      height: 48,
      overflow: 'hidden',
    },
  },
  success: {
    border: `1px solid ${theme.palette.primary.main}`,
    maxWidth: 310,
    padding: theme.spacing(6),
    marginTop: theme.spacing(6),
  },
  button: {
    background: `${theme.palette.primary.main} !important`,
  },
}));

const validator = yup.object({
  email: yup.string().trim().required('Please add your email.').email(),
});

const Intro = (props: IntroProps) => {
  const { isSM } = useMediaQueries();
  const classes = useStyles();
  const { onSubmit, loading } = useSubmitAction();

  return (
    <Formik
      onSubmit={onSubmit}
      validationSchema={validator}
      initialValues={{ email: '' }}
    >
      <Form>
        <div className={classes.wrapper}>
          <Container className={classes.container}>
            <Grid className={classes.content} container>
              <Grid id={FORM_ID} sm={isSM ? 12 : 7} item>
                <Typography
                  className={classes.title}
                  paragraph
                  color="inherit"
                  variant="h1"
                >
                  Join OpenTalent <b>for&nbsp;Freelancers</b>.
                </Typography>
                <Typography className={classes.info}>
                  <Typography
                    fontWeight={600}
                    component="span"
                    color="primary"
                    className={classes.titleLight}
                  >
                    Welcome to Europe’s #1 talent-centric freelancer platform.{' '}
                  </Typography>
                  The invite-only network for top freelancers across Europe who
                  seek high-quality jobs at leading companies without the burden
                  of high fees, long payment periods and complex admin - Apply
                  to join!
                </Typography>

                <Box pt={4}>
                  <Grid
                    direction={isSM ? 'column' : 'row'}
                    spacing={4}
                    container
                  >
                    <Grid xs={isSM ? 12 : 6} item>
                      <ConnectedTextField
                        placeholder="Enter your email"
                        name="email"
                        fullWidth
                        size="medium"
                        variant="outlined"
                        className={classes.input}
                      />
                    </Grid>
                    <Grid xs={isSM ? 12 : 5} item>
                      <Button
                        variant="contained"
                        color="primary"
                        fullWidth
                        type="submit"
                        size="large"
                        disabled={loading}
                        data-test-id="email-submit"
                        className={classes.button}
                      >
                        Apply now
                      </Button>
                    </Grid>
                  </Grid>
                </Box>
              </Grid>
            </Grid>
          </Container>
        </div>
      </Form>
    </Formik>
  );
};

export default Intro;
