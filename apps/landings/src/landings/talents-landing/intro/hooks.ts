import { useSnackbar } from 'notistack';
import { useCallback, useState } from 'react';

import { useCheckTalentExistsByEmailLazyQuery } from '@libs/graphql-types';
import { LOCAL_STORAGE_KEYS } from '@libs/helpers/consts';

import { paths } from '../../../utils/consts';

export const useSubmitAction = (companyId?: string) => {
  const { enqueueSnackbar } = useSnackbar();
  const [lastEmail, setLastEmail] = useState<string | undefined>(undefined);
  const [checkEmail, { loading }] = useCheckTalentExistsByEmailLazyQuery({
    onCompleted: (data) => {
      if (data && !data.checkTalentExistsByEmail && lastEmail) {
        localStorage.setItem(
          LOCAL_STORAGE_KEYS.talentOnboardingEmail,
          lastEmail,
        );

        if (companyId) {
          localStorage.setItem(
            LOCAL_STORAGE_KEYS.talentOnboardingCompanyId,
            companyId,
          );
        }

        const search = new URLSearchParams({
          companyId: companyId || '',
          email: lastEmail,
        });

        window.location.href = `${paths.talentOnboarding}?${search}`;
      } else {
        enqueueSnackbar(`Email already exists`, {
          variant: 'error',
        });
      }
    },
    onError: () => {
      enqueueSnackbar(`Apply error`, { variant: 'error' });
    },
  });

  const onSubmit = useCallback(
    async (values) => {
      setLastEmail(values.email);
      await checkEmail({ variables: { talent_email: values.email } });
    },
    [checkEmail],
  );

  return { onSubmit, loading };
};
