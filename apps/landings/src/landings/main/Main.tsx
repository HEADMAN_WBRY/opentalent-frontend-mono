import React from 'react';

import { makeStyles } from '@mui/styles';

import Footer from './footer';
import Header from './header';
import Intro from './intro';

interface MainProps {}

const useStyles = makeStyles((theme) => ({
  wrapper: {
    width: '100vw',
    minHeight: '100vh',
    overflow: 'hidden',
    background: 'black',
    display: 'flex',
    flexDirection: 'column',

    '& .MuiButton-root': {
      borderRadius: 4,
      textTransform: 'none',
    },
  },
  '@global': {
    body: {
      width: '100%',
    },

    '.MuiButton-containedSecondary': {
      color: theme.palette.primary.main,
      transition: 'all .3s',

      '&:hover': {
        background: theme.palette.secondary.light,
      },
    },
  },
  '@font-face': {
    fontFamily: 'MissRhinetta',
    src: 'url(assets/fonts/Miss-Rhinetta.otf)',
  },

  join: {
    background: 'white',
    color: theme.palette.text.primary,

    '& button': {
      boxShadow: '0px 8px 16px rgba(0, 0, 0, 0.14) !important',
    },
  },
}));

const Main = (props: MainProps) => {
  const classes = useStyles();

  return (
    <div className={classes.wrapper}>
      <Header />
      <Intro />
      <Footer />
    </div>
  );
};

export default Main;
