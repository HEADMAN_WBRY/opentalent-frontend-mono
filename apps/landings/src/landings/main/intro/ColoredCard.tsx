import React from 'react';

import { Box, Card, CardContent } from '@mui/material';
import { green, pink } from '@mui/material/colors';
import { makeStyles } from '@mui/styles';

import Button from '@libs/ui/components/button';
import Typography from '@libs/ui/components/typography';

interface ColoredCardProps {
  title: React.ReactNode;
  text: React.ReactNode;
  color: 'green' | 'yellow';
  link: string;
  footerText: React.ReactNode;
  isNew?: boolean;
}

const useStyles = makeStyles((theme) => ({
  root: {
    position: 'relative',
    width: 430,
    textAlign: 'center',
    borderColor: theme.palette.secondary.main,
    transition: 'all 0.3s ease-in-out',
    borderRadius: 12,
    borderBottomRightRadius: 0,
    borderBottomLeftRadius: 0,
    borderBottom: 'none',
    overflow: 'visible !important',

    background: ({ color }: ColoredCardProps) => {
      switch (color) {
        case 'green':
          return green[300];
        case 'yellow':
          return '#F2FF88';
        default:
          return theme.palette.secondary.main;
      }
    },

    [theme.breakpoints.down('md')]: {
      maxWidth: '100%',
      height: 'auto',
      width: '100%',
    },
  },
  content: {
    flexDirection: 'column',
    padding: theme.spacing(8, 5),
    display: 'flex',
    height: '100%',
    width: '100%',

    [theme.breakpoints.down('md')]: {
      padding: theme.spacing(4, 4),
    },
  },
  newLabel: {
    position: 'absolute',
    padding: theme.spacing(2, 4),
    backgroundColor: pink[300],
    top: 10,
    right: -10,
    color: 'rgba(12, 1, 1, 0.79)',
    transform: 'rotate(-5deg)',
    fontWeight: 'bold',
    borderRadius: 12,
  },
  title: {
    fontStyle: 'italic',
    marginBottom: theme.spacing(6),
    whiteSpace: 'break-spaces',

    [theme.breakpoints.down('md')]: {
      marginBottom: theme.spacing(2),
      height: 'auto',
      fontSize: 24,
      lineHeight: '28px',
    },
  },
  button: {
    textTransform: 'uppercase !important' as any,
    color: 'white !important',
  },
  text: {
    whiteSpace: 'break-spaces',
    marginBottom: theme.spacing(6),

    [theme.breakpoints.down('xs')]: {
      whiteSpace: 'initial',
      marginBottom: theme.spacing(3),
    },
  },
  footer: {
    borderBottomRightRadius: 12,
    borderBottomLeftRadius: 12,
    textAlign: 'center',
    color: 'black',
    fontSize: 12,
    border: '1px solid black',
    lineHeight: '30px',
    borderTop: 'none',
    height: 30,

    background: ({ color }: ColoredCardProps) => {
      switch (color) {
        case 'green':
          return 'linear-gradient(270deg, #3F9243 1.51%, #8EBE90 50.76%, #388E3C 100%)';
        case 'yellow':
          return 'linear-gradient(271.52deg, #FFA726 6.97%, #E8C798 44.77%, #FFA726 91.35%)';
        default:
          return theme.palette.secondary.main;
      }
    },
  },
}));

const ColoredCard = (props: ColoredCardProps) => {
  const { title, text, link, footerText, isNew } = props;
  const classes = useStyles(props);

  return (
    <>
      <Card className={classes.root} variant="outlined">
        {isNew && (
          <Typography variant="body2" className={classes.newLabel}>
            NEW 🔥
          </Typography>
        )}
        <CardContent className={classes.content}>
          <Box>
            <Typography className={classes.title} variant="h4">
              {title}
            </Typography>
          </Box>

          <Typography className={classes.text}>{text}</Typography>

          <Box>
            <Button className={classes.button} variant="contained" href={link}>
              Learn more
            </Button>
          </Box>
        </CardContent>
        <span />
      </Card>
      <Box className={classes.footer}>{footerText}</Box>
    </>
  );
};

export default ColoredCard;
