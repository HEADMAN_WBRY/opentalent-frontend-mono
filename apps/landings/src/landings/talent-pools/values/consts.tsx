import { ReactComponent as CloudIcon } from '../../../assets/values/cloud.svg';
import { ReactComponent as GlobeIcon } from '../../../assets/values/globe.svg';
import { ReactComponent as DiversityIcon } from '../assets/values/diversity.svg';
import { ReactComponent as FastIcon } from '../assets/values/fast.svg';
import { ReactComponent as PayIcon } from '../assets/values/pay.svg';
import { ReactComponent as ProfileIcon } from '../assets/values/profile.svg';
import { ReactComponent as RecordIcon } from '../assets/values/record.svg';
import { ReactComponent as VerificationIcon } from '../assets/values/verification.svg';

export const getValues = () => [
  {
    title: 'Create pipeline',
    text: `Build record of vetted
people with critical skills.`,
    Icon: RecordIcon,
  },
  {
    title: 'Faster hiring',
    text: `Get the skills your need
instantly from your Hub.`,
    Icon: FastIcon,
  },
  {
    title: 'Bypass recruiters',
    text: `Stop paying for middlemen
fees by sourcing directly.`,
    Icon: PayIcon,
  },
  {
    title: 'Verified talent',
    text: `Automated in-platform checks
to verify all profiles.`,
    Icon: VerificationIcon,
  },

  {
    title: 'Up-to-date record',
    text: `Auto-updating of candidate
data results in rich profiles.`,
    Icon: ProfileIcon,
  },

  {
    title: 'Diversity & Inclusion',
    text: `Unbiast skills-based matching
for diverse & inclusive hiring.`,
    Icon: DiversityIcon,
  },

  {
    title: 'Global readiness',
    text: `Add anyone across the globe
to your Direct Sourcing Hub.`,
    Icon: GlobeIcon,
  },
  {
    title: 'Enterprise-ready',
    text: `Get the security and
scalability enterprises need.`,
    Icon: CloudIcon,
  },
];
