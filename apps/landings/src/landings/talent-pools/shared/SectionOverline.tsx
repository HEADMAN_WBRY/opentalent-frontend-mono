import React from 'react';

import { Box, Step, StepLabel } from '@mui/material';
import { makeStyles } from '@mui/styles';

interface SectionOverlineProps extends React.PropsWithChildren<unknown> {
  index: number;
  color: 'primary' | 'secondary';
}

const useStyles = makeStyles((theme) => ({
  root: {
    paddingBottom: theme.spacing(6),
  },
  step: {
    '& .MuiStepLabel-label': {
      color: ({ color }: SectionOverlineProps) => {
        switch (color) {
          case 'primary':
            return theme.palette.primary.main;
          default:
            return theme.palette.text.primary;
        }
      },
      textTransform: 'uppercase',
      fontSize: 18,
    },

    '& svg': {
      width: 32,
      height: 32,
      color: ({ color }: SectionOverlineProps) => {
        switch (color) {
          case 'primary':
            return theme.palette.primary.main;
          default:
            return theme.palette.secondary.main;
        }
      },
    },

    '& text': {
      fill: ({ color }: SectionOverlineProps) => {
        switch (color) {
          case 'primary':
            return theme.palette.secondary.main;
          default:
            return theme.palette.primary.main;
        }
      },
    },
  },
}));

const SectionOverline = (props: SectionOverlineProps) => {
  const { children, index } = props;
  const classes = useStyles(props);

  return (
    <Box className={classes.root} display="flex" justifyContent="center">
      <Step
        index={index}
        completed={false}
        className={classes.step}
        color="secondary.main"
        expanded
      >
        <StepLabel>{children}</StepLabel>
      </Step>
    </Box>
  );
};

export default SectionOverline;
