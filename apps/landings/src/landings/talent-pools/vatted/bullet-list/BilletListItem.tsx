import React from 'react';

import { Grid } from '@mui/material';
import { makeStyles } from '@mui/styles';

import Typography from '@libs/ui/components/typography';

import { ReactComponent as Icon } from '../assets/substract.svg';

interface BilletListItemProps {
  title: string;
  subtitle: string;
  isDisabled?: boolean;
}

const useStyles = makeStyles((theme) => ({
  root: {
    width: 280,
  },
  iconWrap: {
    color: ({ isDisabled }: BilletListItemProps) =>
      isDisabled ? '#D9D9D9' : theme.palette.green.dark,
  },
  title: {
    color: ({ isDisabled }: BilletListItemProps) =>
      isDisabled ? theme.palette.text.secondary : theme.palette.text.primary,
  },
}));

const BilletListItem = (props: BilletListItemProps) => {
  const { title, subtitle } = props;
  const classes = useStyles(props);

  return (
    <Grid className={classes.root} container spacing={4}>
      <Grid className={classes.iconWrap} item>
        <Icon />
      </Grid>
      <Grid item>
        <Typography className={classes.title} fontWeight={600}>
          {title}
        </Typography>
        <Typography variant="body2" color="text.secondary">
          {subtitle}
        </Typography>
      </Grid>
    </Grid>
  );
};

export default BilletListItem;
