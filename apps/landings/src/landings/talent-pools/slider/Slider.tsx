import React, { useState } from 'react';

import { Hidden } from '@mui/material';
import { makeStyles } from '@mui/styles';

import Typography from '@libs/ui/components/typography';

import Section from '../../../shared/new-landings/section';
import SectionTitle from '../../../shared/new-landings/section-title';
import SectionOverline from '../shared/SectionOverline';
import { ITEMS } from './consts';
import DesktopSlider from './desktop-slider';
import MobileSlider from './mobile-slider';

interface SliderProps {}

const useStyles = makeStyles((theme) => ({
  root: {
    maxWidth: 1050,
    margin: '0 auto',
  },
  subtitle: {
    fontSize: 20,

    [theme.breakpoints.down('md')]: {
      fontSize: 16,
    },

    [theme.breakpoints.down('sm')]: {
      whiteSpace: 'initial',
    },
  },
}));

const Slider = (props: SliderProps) => {
  const classes = useStyles();
  const [activeIndex = 0, setActiveIndex] = useState<number>();

  return (
    <Section
      overline={
        <SectionOverline color="secondary" index={0}>
          Self-service
        </SectionOverline>
      }
      classes={{ root: classes.root }}
      color="white"
    >
      <SectionTitle>
        {`IT’S LIKE YOUR own LINKEDIN
FOR future top Talent.`.toUpperCase()}
      </SectionTitle>

      <Typography
        className={classes.subtitle}
        textAlign="center"
        whiteSpace="break-spaces"
      >
        {`your Direct Sourcing Hub brings on-demand
access to in-demand skills, commission-free.`.toUpperCase()}
      </Typography>

      <Hidden mdDown>
        <DesktopSlider
          activeIndex={activeIndex}
          setActiveIndex={setActiveIndex}
          items={ITEMS}
        />
      </Hidden>
      <Hidden mdUp>
        <MobileSlider
          activeIndex={activeIndex}
          setActiveIndex={setActiveIndex}
          items={ITEMS}
        />
      </Hidden>
    </Section>
  );
};

export default Slider;
