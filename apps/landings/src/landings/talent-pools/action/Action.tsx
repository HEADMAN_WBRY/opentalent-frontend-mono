import React from 'react';

import { Box, Grid } from '@mui/material';
import { makeStyles } from '@mui/styles';

import { useMediaQueries } from '@libs/helpers/hooks/media-queries';
import Button from '@libs/ui/components/button';
import Typography from '@libs/ui/components/typography';

import Section from '../../../shared/new-landings/section';
import SectionTitle from '../../../shared/new-landings/section-title';
import { paths } from '../../../utils/consts';

interface ActionProps {}

const useStyles = makeStyles((theme) => ({
  withButtonText: {
    [theme.breakpoints.down('md')]: {
      textAlign: 'center',
    },
  },
}));

const Action = (props: ActionProps) => {
  const { isSM } = useMediaQueries();
  const classes = useStyles();

  return (
    <Section>
      <SectionTitle color="secondary.contrastText">
        {`Find the skills you need.`}
      </SectionTitle>

      <Box pt={isSM ? 4 : 14}>
        <Grid
          justifyContent="center"
          direction={isSM ? 'column' : 'row'}
          spacing={isSM ? 6 : 8}
          container
        >
          <Grid item>
            <Button
              variant="contained"
              color="primary"
              fullWidth
              type="submit"
              size="large"
              href={paths.companyOldOnboardingForm}
            >
              <b>Start My Hub</b> - it’s FREE
            </Button>
          </Grid>
          <Grid className={classes.withButtonText} item>
            <Typography
              variant="subtitle2"
              color="primary.main"
              fontWeight={700}
            >
              Claim your company hub -
            </Typography>
            <Typography variant="subtitle2" color="secondary.contrastText">
              <span style={{ textDecoration: 'underline' }}>15.000+ hubs</span>{' '}
              up for grabs.
            </Typography>
          </Grid>
        </Grid>
      </Box>
    </Section>
  );
};

export default Action;
