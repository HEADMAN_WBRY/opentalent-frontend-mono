import devops from '../assets/intro/devops@2x.png';
import mulesoft from '../assets/intro/mulesoft@2x.png';
import salesForce from '../assets/intro/salesforce@2x.png';

export const ITEMS = [
  { title: 'Salesforce Developer', image: salesForce, name: 'Erica' },
  { title: 'DevOps Engineer', image: devops, name: 'Jerome' },
  { title: 'Mulesoft Engineer', image: mulesoft, name: 'Anna' },
];
