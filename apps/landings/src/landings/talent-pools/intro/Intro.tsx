/* eslint-disable jsx-a11y/accessible-emoji */
import React, { useState } from 'react';
import { useInterval } from 'react-use';

import { Box, Button, Container, Grid, Hidden } from '@mui/material';
import { makeStyles } from '@mui/styles';

import { getNextIndex } from '@libs/helpers/common';
import { EXTERNAL_LINKS } from '@libs/helpers/consts';
import { useMediaQueries } from '@libs/helpers/hooks/media-queries';
import Typography from '@libs/ui/components/typography';

import { paths } from '../../../utils/consts';
import LogosLine from './LogosLine';
import { ITEMS } from './consts';

interface IntroProps {}

const useStyles = makeStyles((theme) => ({
  wrapper: {
    background: theme.palette.secondary.dark,

    [theme.breakpoints.down('md')]: {
      paddingTop: theme.spacing(10),
      paddingBottom: theme.spacing(20),
    },
  },
  container: {
    height: 750,
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    padding: `0 ${theme.spacing(5)}`,

    [theme.breakpoints.down('md')]: {
      height: 'auto',
    },
  },
  textSection: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
  },
  content: {
    background: ({ image }: any) => `url(${image}) no-repeat right center`,
    backgroundSize: '34% !important',
    minHeight: 400,

    [theme.breakpoints.down('md')]: {
      background: 'none !important',
      minHeight: 'auto',
    },
  },
  button: {
    height: 48,
    minWidth: 270,
  },
  logosContainer: {
    marginTop: theme.spacing(20),

    [theme.breakpoints.down('md')]: {
      marginTop: theme.spacing(4),
      marginBottom: theme.spacing(10),
    },
  },

  firstTitle: {
    whiteSpace: 'break-spaces',
    paddingBottom: theme.spacing(3),
    fontSize: 48,
    lineHeight: '56px',
    marginBottom: theme.spacing(7),

    [theme.breakpoints.down('md')]: {
      fontSize: 36,
      lineHeight: '42px',
      paddingBottom: theme.spacing(0),
    },
  },
  secondTitle: {
    whiteSpace: 'nowrap',
    maxWidth: '100%',

    [theme.breakpoints.down('md')]: {
      fontSize: 28,
      lineHeight: '40px',
    },
  },

  mobileImage: {
    paddingTop: theme.spacing(10),
    marginBottom: theme.spacing(5),
    textAlign: 'center',

    '& > img': {
      maxWidth: '100%',
    },
  },

  limitedText: {
    [theme.breakpoints.down('md')]: {
      textAlign: 'center',
      display: 'flex',
      flexDirection: 'column',
    },
  },

  smallTitle: {
    fontSize: 10,
    textTransform: 'uppercase',
    marginBottom: theme.spacing(4),
    color: 'white',
    fontWeight: 'bold',
  },
}));

const Intro = (props: IntroProps) => {
  const { isSM } = useMediaQueries();
  const [count, setCount] = useState(0);
  const image = ITEMS[count].image;
  const title = ITEMS[count].title;
  const talentName = ITEMS[count].name;
  const classes = useStyles({ image });

  useInterval(() => {
    setCount(getNextIndex(count, ITEMS.length));
  }, 5000);

  return (
    <div className={classes.wrapper}>
      {ITEMS.map((i) => (
        <link rel="preload" as="image" key={i.image} href={i.image} />
      ))}

      <Container className={classes.container}>
        <Grid className={classes.content} container>
          <Grid className={classes.textSection} sm={isSM ? 12 : 7} item>
            <Hidden mdDown>
              <Typography className={classes.smallTitle}>
                direct sourcing hub 👋
              </Typography>
            </Hidden>

            <Typography
              fontWeight={700}
              fontStyle="italic"
              color="secondary.contrastText"
              variant="h4"
              className={classes.firstTitle}
            >
              {`Hire top talent,
commission-free!`}
            </Typography>

            {/* <Grow
              timeout={{
                enter: 1000,
                exit: 3000,
              }}
              in
              key={title}
            >
              <div>
                <Typography
                  fontWeight={700}
                  fontStyle="italic"
                  color="primary.main"
                  variant="h3"
                  paragraph
                  className={classes.secondTitle}
                >
                  {title}
                </Typography>
              </div>
            </Grow> */}

            <Typography
              maxWidth={582}
              variant="body1"
              color="secondary.contrastText"
            >
              Start, run and grow a community of professionals with critical
              skills, available for future use without the need to work with
              recruiters, i.e. commission-free.
            </Typography>

            <Box pt={10}>
              <Grid
                direction={isSM ? 'column' : 'row'}
                spacing={isSM ? 4 : 8}
                container
              >
                <Grid item>
                  <Button
                    variant="contained"
                    color="primary"
                    fullWidth
                    type="submit"
                    size="large"
                    className={classes.button}
                    href={paths.companyOldOnboardingForm}
                  >
                    <b>Start My Hub</b> - it’s FREE
                  </Button>
                </Grid>
                <Grid item>
                  <Button
                    variant="outlined"
                    color="primary"
                    fullWidth
                    type="submit"
                    size="large"
                    className={classes.button}
                    href={EXTERNAL_LINKS.pieterLink}
                    style={{ fontWeight: 'bold' }}
                  >
                    Request a Demo
                  </Button>
                </Grid>
              </Grid>
            </Box>
            <Box mt={4} className={classes.limitedText}>
              <Typography
                variant="subtitle2"
                color="primary.main"
                component="span"
                fontWeight={700}
              >
                Claim your company’s hub -{' '}
              </Typography>
              <Typography
                component="span"
                variant="subtitle2"
                color="secondary.contrastText"
              >
                <span style={{ textDecoration: 'underline' }}>
                  15.000+ hubs
                </span>{' '}
                up for grabs.
              </Typography>
            </Box>
          </Grid>
        </Grid>

        <Hidden mdUp>
          <Box className={classes.mobileImage}>
            <img srcSet={`${image} 2x`} alt="Cards" />
          </Box>
        </Hidden>

        <Box className={classes.logosContainer}>
          <LogosLine />
        </Box>
      </Container>
    </div>
  );
};

export default Intro;
