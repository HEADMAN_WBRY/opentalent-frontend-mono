import React from 'react';

import { Grid } from '@mui/material';
import { makeStyles } from '@mui/styles';

import { TalentsCountItem } from '@libs/graphql-types';
import { formatNumber } from '@libs/helpers/format';

import TextBox3d from '../../../shared/text-box-3d';

interface CategoriesBoxesProps {
  counts: TalentsCountItem[];
}

const useStyles = makeStyles((theme) => ({
  categoriesWrap: {
    margin: '0 auto',
    maxWidth: 900,
    width: `calc(100% - ${theme.spacing(8)})`,
  },
  categories: {
    padding: `0 0 ${theme.spacing(16)}`,

    [theme.breakpoints.down('md')]: {
      paddingBottom: theme.spacing(8),
    },
  },
  categoriesItem: {
    marginRight: theme.spacing(2),
    marginLeft: theme.spacing(2),
    marginBottom: theme.spacing(4),
  },
  boxText: {
    color: theme.palette.text.primary,
    background: 'white',
  },
}));

const CategoriesBoxes = ({ counts }: CategoriesBoxesProps) => {
  const classes = useStyles();

  return (
    <Grid className={classes.categories} justifyContent="center" container>
      {counts.map((i) => (
        <Grid className={classes.categoriesItem} key={i.name} item>
          <TextBox3d
            classes={{ text: classes.boxText }}
            text={i.name}
            value={
              <>
                <b>{formatNumber(i.talents_count)}</b> profiles
              </>
            }
          />
        </Grid>
      ))}
    </Grid>
  );
};

export default CategoriesBoxes;
