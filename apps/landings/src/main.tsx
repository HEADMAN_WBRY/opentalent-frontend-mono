import ReactDOM from 'react-dom';

import App from './App';
import AppWrapper from './AppWrapper';

ReactDOM.render(
  <AppWrapper>
    <App />
  </AppWrapper>,
  document.getElementById('root'),
);
