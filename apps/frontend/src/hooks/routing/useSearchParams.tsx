import { useMemo } from 'react';
import { useLocation } from 'react-router-dom';

import { parse } from 'utils/querystring';

export const useSearchParams = () => {
  const location = useLocation();
  return useMemo(() => parse(location.search.slice(1)), [location]);
};
