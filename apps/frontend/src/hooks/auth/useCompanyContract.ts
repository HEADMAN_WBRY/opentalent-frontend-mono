import { OtContractStatusEnum } from '@libs/graphql-types';

import { useCurrentUser } from './useCurrentUser';

export const useCompanyContract = () => {
  const { data } = useCurrentUser();
  const status =
    data?.currentCompanyUser?.company?.contract?.status ||
    OtContractStatusEnum.Unknown;
  const isContractSigned = status === OtContractStatusEnum.Signed;
  return { status, isContractSigned };
};
