import { gql } from '@apollo/client';

export default gql`
  fragment FullUser on User {
    stream_chat_id
    stream_chat_token

    id
    email
    first_name
    last_name
    position
    company {
      id
      name
      logo
      website
      type_of_activity
      contract {
        id
        status
        read_contacts_permission
      }
      has_contract
    }
    created_at
    updated_at
    avatar {
      avatar
      hash
    }
  }
`;
