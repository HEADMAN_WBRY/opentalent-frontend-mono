import { gql } from '@apollo/client';

export default gql`
  mutation CreateSkill($skill_type: SkillTypeEnum!, $name: String!) {
    createSkill(skill_type: $skill_type, name: $name) {
      id
      skill_type
      name
      slug
      created_at
      updated_at
    }
  }
`;
