import { Box, Grid, PaletteOptions } from '@mui/material';
import React from 'react';

import Typography from '@libs/ui/components/typography/Typography';

export default {
  title: 'Palette',
};

const VARIANTS = ['main', 'light', 'dark'];

const renderPalette = (type: keyof PaletteOptions) => (
  <Box bgcolor="lightgray" padding="20px">
    <Grid container spacing={2}>
      {VARIANTS.map((variant) => {
        const color = `${type}.${variant}`;
        return (
          <Grid item key={color}>
            <Box padding="20px" color={`${type}.contrastText`} bgcolor={color}>
              <Typography>{color}</Typography>
            </Box>
          </Grid>
        );
      })}
    </Grid>
  </Box>
);

export const Palette = () => (
  <>
    {renderPalette('primary')}
    {renderPalette('secondary')}
    {renderPalette('info')}
    {renderPalette('error')}
    {renderPalette('warning')}
    {renderPalette('success')}
  </>
);
