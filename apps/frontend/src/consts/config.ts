export const PROJECT_NAME = 'OpenTalent';
export const API_URI = 'https://main.ottest.co/graphql2';

export const isDev = process.env.NODE_ENV === 'development';

export const CLIENT_ID = 'rHO0oaUfIXl7NH0Bm67oZox3rijAkY3i';
export const DOMAIN = 'opentalent.eu.auth0.com';
export const RESPONSE_TYPE = 'token id_token';
export const AUDIENCE = 'https://opentalent.co';
export const SCOPE = 'openid profile';
export const REDIRECT_URI = `${window.location.origin}/auth0`;

export const isProdInstance = 'app.opentalent.co' === window.location.host;
