export const LOCAL_STORAGE_KEYS = {
  talentOnboardingEmail: 'talentOnboardingEmail',
  talentOnboardingAppliedJobId: 'talentOnboardingAppliedJobId',

  talentOnboardingDeviceId: 'talentOnboardingDeviceId',
  talentOnboardingCompanyId: 'talentOnboardingCompanyId',

  companyOnboardingDeviceId: 'companyOnboardingDeviceId',
};
