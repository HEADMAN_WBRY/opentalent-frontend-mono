import {
  CompanyUserChatData,
  CustomChatUserData,
} from '../common/provider/types';
import { ChatTypes } from '../types';

export interface UserToTalentChatMeta {
  type: ChatTypes.UserToTalent;
  companyUser: CompanyUserChatData;
  talent: CustomChatUserData;
}
