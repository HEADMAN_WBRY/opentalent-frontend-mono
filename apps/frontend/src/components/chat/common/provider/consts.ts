import React from 'react';
import { AscDesc, StreamChat } from 'stream-chat';

import { ChatProviderValue } from './types';

export const StreamChatContext = React.createContext<ChatProviderValue | null>(
  null,
);

export const streamChatClient = StreamChat.getInstance(
  '88wkpjh7j4tw',
);

export const getOwnChatFilter = (userId: string) => ({
  members: { $in: [userId] },
});

export const DEFAULT_CHANNELS_SORT = { has_unread: -1 as AscDesc };
