import { isDev } from 'consts/config';

import { makeStyles } from '@mui/styles';

interface CookieConcentProps {}

const hideCookieConsent =
  isDev || window.origin === 'https://ottest.wwweberry.com';

const useStyles = makeStyles((theme) => ({
  '@global': {
    '.cky-consent-container': {
      display: hideCookieConsent ? 'none' : 'inherit',

      'body & .cky-consent-bar': {
        boxShadow: 'none',
      },

      '& .cky-notice': {
        maxWidth: 1024,
        margin: '0 auto',
      },

      'body & .cky-title': {
        fontSize: 20,
        fontWeight: 400,

        [theme.breakpoints.down('sm')]: {
          fontSize: 16,
        },
      },

      '& .cky-notice-des *': {
        letterSpacing: 0,

        [theme.breakpoints.down('sm')]: {
          fontSize: 12,
        },
      },

      '& button': {
        borderRadius: 8,
        borderWidth: 1,

        [theme.breakpoints.down('sm')]: {
          height: 36,
          lineHeight: '20px',
        },

        '&.cky-btn-customize::after': {
          [theme.breakpoints.down('sm')]: {
            top: 14,
          },
        },
      },
    },
  },
}));

export const CookieConcent = (props: CookieConcentProps) => {
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const classes = useStyles();

  return null;
};
