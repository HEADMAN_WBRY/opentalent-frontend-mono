import cn from 'classnames';
import React from 'react';

import { makeStyles } from '@mui/styles';

import { Maybe, TalentLogRecordInterface } from '@libs/graphql-types';

import { CommentRecordItem } from '../record-item';
import JobMatchRecordItem from '../record-item/job-match-record-item';
import { atsMatchers } from '../utils';

interface RecordListProps {
  records: Maybe<TalentLogRecordInterface>[];
  className?: string;
}

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
    maxHeight: '100%',
    overflowY: 'auto',
  },
}));

export const RecordList = React.forwardRef(
  ({ records, className }: RecordListProps, ref) => {
    const classes = useStyles();

    return (
      <div className={cn(className, classes.root)} ref={ref as any}>
        {records.map((record) => {
          if (!!record && atsMatchers.isATSComment(record)) {
            return <CommentRecordItem record={record} key={record?.id} />;
          }
          if (!!record && atsMatchers.isATSJobMatch(record)) {
            return <JobMatchRecordItem record={record} key={record?.id} />;
          }
          return <div key={record?.id}>Unknown record</div>;
        })}
      </div>
    );
  },
);
