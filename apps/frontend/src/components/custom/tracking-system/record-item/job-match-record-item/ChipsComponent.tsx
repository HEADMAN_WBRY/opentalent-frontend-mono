import ArrowForwardIcon from '@mui/icons-material/ArrowForward';
import { Grid } from '@mui/material';
import React from 'react';
import { JOB_MATCH_TYPES_LABELS } from 'utils/job';

import { JobMatchTypeEnum } from '@libs/graphql-types';
import Chip, { ChipProps } from '@libs/ui/components/chip';

import { JobMatchRecordItemProps } from './types';

interface ChipsComponentProps extends JobMatchRecordItemProps {}

const CHIP_COLORS: Record<JobMatchTypeEnum, ChipProps['color']> = {
  [JobMatchTypeEnum.Invited]: 'lightBlue',
  [JobMatchTypeEnum.TalentApplication]: 'primary',
  [JobMatchTypeEnum.InstantMatch]: 'tertiary',
  [JobMatchTypeEnum.OpentalentSuggestion]: 'successDark',
  [JobMatchTypeEnum.Intake]: 'green',
  [JobMatchTypeEnum.Hired]: 'success',
  [JobMatchTypeEnum.Rejected]: 'red',
  [JobMatchTypeEnum.Withdrawn]: 'success',
};

const ChipsComponent = ({ record }: ChipsComponentProps) => {
  const beforeType = record.job_match_action
    ?.job_match_type_before as JobMatchTypeEnum;
  const afterType = record.job_match_action
    ?.job_match_type_after as JobMatchTypeEnum;

  return (
    <>
      {beforeType && (
        <>
          <Grid item>
            <Chip
              color={CHIP_COLORS[beforeType] || 'white'}
              size="small"
              label={JOB_MATCH_TYPES_LABELS[beforeType] || '[unknown]'}
            />
          </Grid>
          <Grid item>
            <ArrowForwardIcon
              style={{ transform: 'translateY(4px)', color: '#777' }}
            />
          </Grid>
        </>
      )}
      <Grid item>
        <Chip
          size="small"
          color={CHIP_COLORS[afterType] || 'white'}
          label={JOB_MATCH_TYPES_LABELS[afterType] || '[unknown]'}
        />
      </Grid>
    </>
  );
};

export default ChipsComponent;
