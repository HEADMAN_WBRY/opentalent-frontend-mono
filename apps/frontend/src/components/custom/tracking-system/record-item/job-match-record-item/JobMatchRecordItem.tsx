import React from 'react';

import { JobMatchActionTypeEnum } from '@libs/graphql-types';

import {
  DefaultJobMatchRecordItem,
  WithdrawnRecord,
  ApplicationRecord,
  InitialTalentApplicationRecord,
  InstantMatchRecord,
  InvitationAcceptedRecord,
} from './match-type-records';
import { JobMatchRecordItemProps } from './types';

const COMPONENTS_MAP: Record<
  JobMatchActionTypeEnum,
  React.ComponentType<JobMatchRecordItemProps>
> = {
  [JobMatchActionTypeEnum.Application]: ApplicationRecord,
  [JobMatchActionTypeEnum.DirectIntake]: DefaultJobMatchRecordItem,
  [JobMatchActionTypeEnum.Hired]: DefaultJobMatchRecordItem,
  [JobMatchActionTypeEnum.InitialInvitationMadeByCompanyUser]:
    DefaultJobMatchRecordItem,
  [JobMatchActionTypeEnum.InitialInvitationMadeByTalent]:
    DefaultJobMatchRecordItem,
  [JobMatchActionTypeEnum.InitialTalentApplication]:
    InitialTalentApplicationRecord,
  [JobMatchActionTypeEnum.InstantMatchCreated]: InstantMatchRecord,
  [JobMatchActionTypeEnum.Invitation]: DefaultJobMatchRecordItem,
  [JobMatchActionTypeEnum.InvitationAccepted]: InvitationAcceptedRecord,
  [JobMatchActionTypeEnum.Rejected]: DefaultJobMatchRecordItem,
  [JobMatchActionTypeEnum.Withdrawn]: WithdrawnRecord,
};

const JobMatchRecordItem = ({ record }: JobMatchRecordItemProps) => {
  const Component =
    COMPONENTS_MAP[
      record.job_match_action?.job_match_action_type as JobMatchActionTypeEnum
    ];

  return <Component record={record} />;
};

export default JobMatchRecordItem;
