import AddCircle from '@mui/icons-material/AddCircleOutline';
import CheckCircleOutlineIcon from '@mui/icons-material/CheckCircleOutline';
import NotInterestedIcon from '@mui/icons-material/NotInterested';
import { ReactComponent as CheckIcon } from 'assets/icons/check.svg';
import { useDeclineAccount } from 'hooks/account/useDeclineAccount';
import { useRemindAccount } from 'hooks/account/useRemindAccount';
import { useRevokeInviteAccount } from 'hooks/account/useRevokeInviteAccount';
import { useVerifyAccount } from 'hooks/account/useVerifyAccount';
import { formatName } from 'utils/talent';

import { Talent } from '@libs/graphql-types';

import { ActionItem } from '../types';
import { useOnInviteAction } from './useOnInviteAction';

interface Args {
  onSuccess?: VoidFunction;
  talent: Talent;
}
const useVerificationActions = ({ onSuccess, talent }: Args) => {
  const { verifyAccount, loading } = useVerifyAccount(talent, onSuccess);
  const { loading: isRevoking, declineAccount } = useDeclineAccount(
    talent,
    onSuccess,
  );

  const actions: ActionItem[] = [
    {
      text: 'Verify',
      Icon: CheckIcon as any,
      onClick: verifyAccount,
      disabled: loading || !talent.is_verification_required,
    },
    {
      text: 'Decline',
      Icon: NotInterestedIcon,
      onClick: declineAccount,
      disabled: isRevoking,
    },
  ];

  return actions;
};

const useNotAcceptedActions = ({ onSuccess, talent }: Args) => {
  const { remindTalent, loading } = useRemindAccount(talent, onSuccess);
  const { loading: isRevoking, revokeInvite } = useRevokeInviteAccount(
    talent,
    onSuccess,
  );

  const actions: ActionItem[] = [
    {
      text: 'Send reminder',
      Icon: CheckCircleOutlineIcon,
      onClick: remindTalent,
      disabled: loading,
    },
    {
      text: 'Revoke invite',
      Icon: NotInterestedIcon,
      onClick: revokeInvite,
      disabled: isRevoking,
    },
  ];

  return actions;
};

const useCommonTalentActions = ({ onSuccess, talent }: Args) => {
  const onInvite = useOnInviteAction({
    talentId: talent.id,
    talentName: formatName({
      firstName: talent.first_name,
      lastName: talent.last_name,
    }),
  });

  const actions: ActionItem[] = [
    {
      text: 'Invite to apply',
      Icon: AddCircle,
      onClick: onInvite,
    },
  ];

  return actions;
};

export const useTalentActions = ({ talent, onSuccess }: Args) => {
  const isNotVerified = talent.is_verification_required;
  const isNotAccepted = !talent.is_invitation_accepted;
  let finalActions = null;
  const verificationActions = useVerificationActions({ talent, onSuccess });
  const notAcceptedActions = useNotAcceptedActions({ talent, onSuccess });
  const commonActions = useCommonTalentActions({ talent, onSuccess });

  if (isNotVerified) {
    finalActions = verificationActions;
  } else if (isNotAccepted) {
    finalActions = notAcceptedActions;
  } else {
    finalActions = commonActions;
  }

  return finalActions;
};
