import { ListItemIcon, ListItemText, Menu, MenuItem } from '@mui/material';
import { makeStyles } from '@mui/styles';
import React from 'react';

import { ActionsMenuProps } from './types';

const useStyles = makeStyles((theme) => ({
  menu: {
    minWidth: `150px !important`,
  },
}));

const ActionsMenu = ({
  open,
  anchorEl,
  actions,
  handleClose,
  hideDisabled,
}: ActionsMenuProps) => {
  const classes = useStyles();

  return (
    <Menu
      id="long-menu"
      anchorEl={anchorEl}
      keepMounted
      open={open}
      onClose={handleClose}
      anchorOrigin={{ vertical: 'bottom', horizontal: 'right' }}
      transformOrigin={{ vertical: 'top', horizontal: 'right' }}
      classes={{ list: classes.menu }}
    >
      {actions.map(({ text, onClick, Icon, disabled = false }) => {
        if (disabled && hideDisabled) {
          return null;
        }

        return (
          <MenuItem
            key={text}
            onClick={() => {
              onClick();
              handleClose();
            }}
            style={{ paddingTop: '2px', paddingBottom: '2px' }}
            disabled={disabled}
          >
            {Icon && (
              <ListItemIcon>
                <Icon fontSize="inherit" />
              </ListItemIcon>
            )}
            <ListItemText primary={text} />
          </MenuItem>
        );
      })}
    </Menu>
  );
};

export default ActionsMenu;
