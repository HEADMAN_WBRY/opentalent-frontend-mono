import MoreVertIcon from '@mui/icons-material/MoreVert';
import { TalentActionsMenu } from 'components/custom/talent/actions-menu';
import { useMenuAction } from 'hooks/common/useMenuAction';
import React from 'react';
import { noop } from 'utils/common';

import { Talent } from '@libs/graphql-types';
import Button from '@libs/ui/components/button';

interface TalentActionsProps {
  talent: Talent;
  refetch?: VoidFunction;
  loading?: boolean;
}

export const TalentActions = ({
  talent,
  refetch = noop,
}: TalentActionsProps) => {
  const { open, handleClose, handleClick, anchorEl } = useMenuAction();

  return (
    <>
      <Button
        onClick={handleClick as VoidFunction}
        variant="outlined"
        size="medium"
        data-test-id="talentActions"
        endIcon={<MoreVertIcon />}
      >
        Actions
      </Button>
      <TalentActionsMenu
        open={open}
        anchorEl={anchorEl}
        handleClose={handleClose}
        talent={talent}
        onSuccess={refetch}
      />
    </>
  );
};
