import React from 'react';

import { Theme } from '@mui/material';
import { makeStyles } from '@mui/styles';

import Typography from '@libs/ui/components/typography';

interface SideCardMarkProps {
  color: 'warning' | 'primary' | 'green' | 'info';
}

export const getCustomColor = (
  color: SideCardMarkProps['color'],
  theme: Theme,
) => {
  switch (color) {
    case 'warning':
      return theme.palette.tertiary;
    case 'green':
      return theme.palette.success;
    case 'info':
      return theme.palette.info;
    case 'primary':
    default:
      return theme.palette.primary;
  }
};

const useStyles = makeStyles((theme) => ({
  root: {
    height: 36,
    position: 'absolute',
    right: -12,
    top: theme.spacing(6),
    display: 'flex',
    alignItems: 'center',
    padding: `0 ${theme.spacing(4)}`,
    opacity: 0.8,

    '& > p': {
      position: 'relative',
      zIndex: 2,
    },

    '&::before': {
      left: 0,
      top: 0,
      content: '""',
      display: 'block',
      position: 'absolute',
      zIndex: 0,
      width: '100%',
      height: '100%',
      opacity: 0.8,
      borderRadius: '8px 8px 0px 8px',
      background: ({ color }: SideCardMarkProps) =>
        getCustomColor(color, theme).main,
    },

    '&::after': {
      content: '""',
      display: 'block',
      position: 'absolute',
      bottom: -4,
      right: 0,
      width: 0,
      height: 0,
      borderStyle: 'solid',
      opacity: 0.8,
      borderWidth: '4px 12px 0 0',
      borderColor: ({ color }: SideCardMarkProps) =>
        `${
          getCustomColor(color, theme).dark
        } transparent transparent transparent`,
    },
  },
  shadow: {
    position: 'absolute',
    right: 12,
    bottom: -2,
    width: 'calc(100% - 12px)',
    height: 2,
    background: 'rgba(247, 179, 79, 0.26)',
    clipPath: 'polygon(100% 0, 0 0, 100% 100%)',
  },
}));

const SideCardMark = (props: React.PropsWithChildren<SideCardMarkProps>) => {
  const { children } = props;
  const classes = useStyles(props);

  return (
    <div className={classes.root}>
      <Typography variant="body2">{children}</Typography>
      <i className={classes.shadow} />
    </div>
  );
};

export default SideCardMark;
