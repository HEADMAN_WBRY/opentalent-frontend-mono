import { makeStyles } from '@mui/styles';

import { Theme } from '@libs/ui/themes/default';

const useStyles = makeStyles((theme: Theme) => ({
  modal: {
    padding: `${theme.spacing(2)} ${theme.spacing(5)} ${theme.spacing(6)}`,
  },
  cropContainer: {
    width: 368,
    height: 240,
    position: 'relative',
  },
  slider: {
    paddingTop: theme.spacing(8),
  },
}));

export default useStyles;
