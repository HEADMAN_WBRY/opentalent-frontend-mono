import { ReactComponent as PhotoIcon } from 'assets/icons/photo.svg';
import cn from 'classnames';
import { useSnackbar } from 'notistack';
import React, { useCallback, useState } from 'react';
import { DropzoneOptions, FileRejection, useDropzone } from 'react-dropzone';
import { getDropzoneRejectionMessage } from 'utils/dropzone';

import Grid from '@mui/material/Grid';

import Typography from '@libs/ui/components/typography';

import CropImageModal from './crop-image-modal';
import useStyles from './styles';

export interface ImageUploadProps {
  label?: string;
  Icon?: React.ElementType;
  onChange?: (e: File[]) => void;
  error?: boolean;
  helperText?: string;
  dropzoneOptions?: DropzoneOptions;
  name?: string;
  initialImage?: string;
}

const ImageUpload = (props: ImageUploadProps) => {
  const {
    Icon = PhotoIcon,
    label,
    onChange,
    dropzoneOptions = {},
    name,
    initialImage = '',
  } = props;
  const [imgSrc, setImageSrc] = useState<string>(initialImage);
  const { enqueueSnackbar } = useSnackbar();
  const [showCropModal, setShowCropModal] = useState<boolean>(false);
  const classes = useStyles({ ...props, uploaded: !!imgSrc });
  const handleReject = useCallback(
    (e: FileRejection[]) => {
      const errMsg = getDropzoneRejectionMessage(e);
      if (enqueueSnackbar) {
        enqueueSnackbar(errMsg, { variant: 'error' });
      }
      // eslint-disable-next-line no-console
      console.error(errMsg);
    },
    [enqueueSnackbar],
  );
  const onDropAccepted = ([file]: File[]) => {
    setImageSrc(URL.createObjectURL(file));
    onChange?.([file]);
    setShowCropModal(true);
  };
  const { getRootProps, getInputProps } = useDropzone({
    accept: 'image/jpeg,image/png,image/gif',
    ...dropzoneOptions,
    onDropRejected: dropzoneOptions.onDropRejected || handleReject,
    onDropAccepted,
  });
  const inputProps = getInputProps();

  return (
    <>
      <div {...getRootProps()} className={cn(classes.container)}>
        {imgSrc && (
          <img className={classes.preview} src={imgSrc} alt="Preview" />
        )}
        <Grid
          className={classes.label}
          justifyContent="center"
          alignItems="center"
          direction="column"
          container
        >
          <Grid item>
            <Icon className={classes.icon} />
          </Grid>
          <Grid item>
            <Typography variant="overline" className={classes.label}>
              {label}
            </Typography>
          </Grid>
        </Grid>
        <input name={name} {...inputProps} />
      </div>
      <CropImageModal
        changeImage={(e: any) => {
          (inputProps.onChange as any)(e);
          onDropAccepted(e?.target?.value);
        }}
        handleClose={() => setShowCropModal(false)}
        open={showCropModal}
        image={imgSrc}
      />
    </>
  );
};

export default ImageUpload;
