import { makeStyles } from '@mui/styles';

import { Theme } from '@libs/ui/themes/default';

import { ImageUploadProps } from './ImageUpload';

interface StylesProps extends ImageUploadProps {
  uploaded: boolean;
}

const useStyles = makeStyles((theme: Theme) => ({
  container: {
    width: 124,
    height: 124,
    display: 'flex',
    borderRadius: '100%',
    border: ({ error }) =>
      `1px dashed ${
        error ? theme.palette.error.main : theme.palette.other.light
      }`,
    alignItems: 'center',
    background: 'white',
    cursor: 'pointer',
    position: 'relative',
    overflow: 'hidden',
    boxShadow: 'none',
    outline: 'none',
    color: ({ uploaded }: StylesProps) =>
      uploaded
        ? theme.palette.secondary.contrastText
        : theme.palette.other.light,
    transition: `all ${theme.transitions.duration.short}s ${theme.transitions.easing.easeInOut}`,

    '&:active, &:active $icon': {
      color: theme.palette.tertiary.main,
      borderColor: theme.palette.tertiary.main,
    },

    '&:hover $label': {
      opacity: 1,
    },
  },
  icon: {
    color: 'inherit',
    transition: 'inherit',
  },
  label: {
    position: 'relative',
    zIndex: 10,
    transition: `all ${theme.transitions.duration.standard}s ${theme.transitions.easing.easeInOut}`,
    opacity: ({ uploaded }: StylesProps) => (uploaded ? 0 : 1),
    display: ({ uploaded }: StylesProps) => (uploaded ? 'none' : 'inherit'),
    color: ({ uploaded }: StylesProps) =>
      uploaded
        ? theme.palette.secondary.contrastText
        : theme.palette.other.light,
  },
  input: {
    position: 'absolute',
    top: -5000000,
    left: -5000000,
    visibility: 'hidden',
  },
  preview: {
    objectFit: 'contain',
    width: '100%',
    height: '100%',
  },
}));

export default useStyles;
