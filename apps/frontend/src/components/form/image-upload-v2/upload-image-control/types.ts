export interface ImageFormikState {
  isLoading: boolean;
  hash?: string;
  file?: File;
}
