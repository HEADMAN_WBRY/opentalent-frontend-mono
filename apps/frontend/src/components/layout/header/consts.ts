import { DEFAULT_AVATAR } from 'consts/common';

export const FAKE_ACCOUNT_PROPS = {
  name: 'Linda Smith',
  position: 'Hiring Manager',
  src: DEFAULT_AVATAR,
};
