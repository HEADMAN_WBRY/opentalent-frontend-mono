import { JOB_TYPE_MAP } from 'consts/common';
import { format, isFuture, isPast, parseISO } from 'date-fns';
import { isNumber } from 'utils/common';

import { Job, Skill, SkillTypeEnum } from '@libs/graphql-types';
import { isNil } from '@libs/helpers/common';
import { formatCurrency } from '@libs/helpers/format';

export const formatDate = (date: string) =>
  date ? format(parseISO(date), 'dd MMM yyyy') : '';

export const sortSkills = (skills: Skill[] = []) =>
  skills.reduce<Record<SkillTypeEnum, Skill[]>>(
    (acc, skill) => {
      acc[skill.skill_type].push(skill);
      return acc;
    },
    {
      [SkillTypeEnum.HardSkills]: [],
      [SkillTypeEnum.SoftSkills]: [],
      [SkillTypeEnum.Solutions]: [],
    },
  );

const thousandsSeparatorRegExp = RegExp(/\B(?=(\d{3})+(?!\d))/g);

export const formatFloatRate = (s: string | number = ''): string =>
  s?.toString()?.replace('.', ',').replace(thousandsSeparatorRegExp, '.');

export const formatRate = ({
  min,
  max,
  isNegotiable = false,
  period = 'hour',
}: {
  min?: number;
  max?: number;
  currencySign?: string;
  isNegotiable?: boolean;
  period?: 'hour' | 'month';
}): string | undefined => {
  if ([min, max].some((i) => !isNil(i))) {
    const interval = [min, max]
      .filter((i) => !!i)
      .map((i) => (isNumber(i) ? formatCurrency(i) : i))
      .join('-');

    return `${interval}/${period} ${
      isNegotiable ? ' (rate is negotiable)' : ''
    }`;
  }
  return undefined;
};

export enum CampaignStatus {
  NotStarted = 'not-started',
  Started = 'started',
  Finished = 'finished',
  Archived = 'archived',
  Draft = 'draft',
}

export const getCampaignStatus = (job?: Job) => {
  if (job?.is_draft) {
    return CampaignStatus.Draft;
  }

  const startDate = parseISO(job?.campaign_start_date);
  const endDate = parseISO(job?.campaign_end_date);

  if (!job) {
    // eslint-disable-next-line no-console
    console.info('🚀 ~ getCampaignStatus - not job provided', job);
    return CampaignStatus.NotStarted;
  }
  if (job?.is_archived) {
    return CampaignStatus.Archived;
  }
  if (isPast(endDate)) {
    return CampaignStatus.Finished;
  }
  if (isPast(startDate) && isFuture(endDate)) {
    return CampaignStatus.Started;
  }
  return CampaignStatus.NotStarted;
};

export const isUnprocessableJob = (job: Job) =>
  [CampaignStatus.Finished, CampaignStatus.Archived].includes(
    getCampaignStatus(job),
  );

export const getJobCapacity = (job: Job) => {
  const value = job.is_old_format ? job.capacity : job.hours_per_week;
  return value ? `${value} hours/week` : '';
};

export const getJobInfoItems = (job: Job) => [
  ...([job?.rate_min, job?.rate_max].some((i) => i !== undefined)
    ? [
        {
          label: 'Rate:',
          value: formatRate({
            min: job?.rate_min,
            max: job?.rate_max,
            isNegotiable: job.is_rate_negotiable,
          }),
        },
      ]
    : []),
  { label: 'Starting date:', value: formatDate(job.start_date) },
  { label: 'End date:', value: formatDate(job.end_date) },
  {
    label: 'Capacity:',
    value: getJobCapacity(job),
  },
  { label: 'Type:', value: (JOB_TYPE_MAP as any)[job.location_type || ''] },
  { label: 'Client:', value: job.client || '-' },
];
