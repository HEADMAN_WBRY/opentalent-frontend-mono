import { useCallback } from 'react';
import { useHistory } from 'react-router-dom';

export function useOpenModal<M>(id: string) {
  const history = useHistory();
  return useCallback(
    (modalData?: M) => {
      history.replace({
        ...history.location,
        state: { [id]: modalData || true },
      });
    },
    [history, id],
  );
}
