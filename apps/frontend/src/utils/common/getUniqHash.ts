function getRandomNumber(min: number, max: number) {
  return Math.random() * (max - min) + min;
}

export const getUniqHash = () => `${Date.now()}_${getRandomNumber(1, 5000)}`;
