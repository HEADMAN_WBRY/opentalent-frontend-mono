import React from 'react';

const Notifications = React.lazy(() => import('./Notifications'));

export default Notifications;
