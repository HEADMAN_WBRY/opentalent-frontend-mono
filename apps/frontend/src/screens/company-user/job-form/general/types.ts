import { CampaignStatus } from 'utils/job';

import { JobLocationTypeEnum, SourceTypeEnum } from '@libs/graphql-types';

export interface GeneralProps {
  status: CampaignStatus;
  remoteIsOption?: boolean;
  locationType?: JobLocationTypeEnum;
  onlyOpenTalentPool?: boolean;
  talentPool?: SourceTypeEnum[];
}
