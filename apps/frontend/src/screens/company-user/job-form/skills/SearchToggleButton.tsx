import { useField } from 'formik';
import React from 'react';

import Button from '@libs/ui/components/button';

interface SearchToggleButtonProps {
  name: string;
  disabled: boolean;
}

const SearchToggleButton = ({ name, disabled }: SearchToggleButtonProps) => {
  const [{ value }, , { setValue, setTouched }] = useField(name);
  const onClick = () => {
    setValue(!value);
    setTouched(true);
  };

  return (
    <>
      <Button disabled={disabled} onClick={onClick} variant="text" color="info">
        {value ? 'switch to standard search' : 'SWITCH TO BOOLEAN SEARCH'}
      </Button>
    </>
  );
};

export default SearchToggleButton;
