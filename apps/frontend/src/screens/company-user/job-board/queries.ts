import { gql } from '@apollo/client';
import JOB_FRAGMENT from 'graphql/fragments/companyUser/jobBoardJobFragment';

export const GET_GOBS = gql`
  ${JOB_FRAGMENT}
  query GetJobs(
    $company_id: ID
    $is_archived: Boolean
    $first: Int = 20
    $page: Int
    $is_draft: Boolean = false
  ) {
    currentCompanyUserJobsCountByStatus {
      type
      count
    }
    jobs(
      is_draft: $is_draft
      first: $first
      page: $page
      company_id: $company_id
      is_archived: $is_archived
    ) {
      paginatorInfo {
        count
        currentPage
        firstItem
        hasMorePages
        lastItem
        lastPage
        perPage
        total
      }
      data {
        ...JobBoardJobFragment
      }
    }
  }
`;

export const GET_JOB_BOARD_INFO = gql`
  query GetJobBoardInfo {
    currentCompanyUserJobBoardInfo {
      talent_responses_count
      talent_matches_count
      talent_responses_count_archived
      talent_matches_count_archived
    }
    commonAppInfo {
      total_ot_freelancers_count
      total_ot_approved_freelancers_count
      total_ot_freelancers_countries_count
      total_unique_skills_count
      total_unique_companies_count
      total_ot_recruiters_countries_count
      total_ot_recruiters_count
    }
  }
`;
