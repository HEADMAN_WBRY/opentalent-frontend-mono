import { ConnectedPageLayout } from 'components/layout/page-layout';
import { useCurrentUser } from 'hooks/auth/useCurrentUser';
import { useCurrentTime } from 'hooks/common/useCurrentTime';
import useMediaQueries from 'hooks/common/useMediaQueries';
import { useSearchParams } from 'hooks/routing';
import React from 'react';
import { Link, RouteComponentProps } from 'react-router-dom';
import { pathManager } from 'routes/consts';

import AddIcon from '@mui/icons-material/Add';
import { Box, Grid, Hidden, Fab } from '@mui/material';

import { JobsCountByStatus } from '@libs/graphql-types';
import Button from '@libs/ui/components/button';
import Typography from '@libs/ui/components/typography';

import DraftsAlert from './DraftsAlert';
import JobsPagination from './JobsPagination';
import NoItems from './NoItems';
import { JOB_BOARD_TEST_DATA_ATTRS } from './consts';
import { useJobBoardInfo } from './hooks';
import JobCard from './job-card';
import JobTabs from './job-tabs';
import Overall from './overall';
import useStyles from './styles';
import { JobTypeEnums } from './types';

interface JobBoardProps extends RouteComponentProps {}

const JobBoard = ({ location }: JobBoardProps) => {
  const currentTime = useCurrentTime();
  const classes = useStyles();
  const { jobsType = JobTypeEnums.Active, page } = useSearchParams();
  const { isSM } = useMediaQueries();
  const {
    jobs,
    appInfo,
    loading,
    currentCompanyUserJobBoardInfo,
    jobsPagination,
    jobsCount,
  } = useJobBoardInfo({
    jobType: jobsType as JobTypeEnums,
    page: Number(page as string),
  });
  const { data } = useCurrentUser();

  const isDraft = jobsType === JobTypeEnums.Draft;
  const showDraftsAlter =
    isDraft &&
    !!jobs.length &&
    !data?.currentCompanyUser?.company?.contract?.id;

  return (
    <ConnectedPageLayout
      documentTitle="Job board"
      headerProps={{ accountProps: {} }}
      drawerProps={{}}
      isLoading={loading}
      contentSpacing={isSM ? 4 : 6}
      classes={{ contentWrapper: classes.contentWrapper }}
    >
      <Box display="flex" justifyContent="space-between" pt={6} pb={4}>
        <Typography variant="h5" paragraph>
          My Jobs
        </Typography>
        <Hidden only={['xs']}>
          <Link to={pathManager.company.newJob.service.generatePath()}>
            <Button
              data-test-id={JOB_BOARD_TEST_DATA_ATTRS.addJobButton}
              variant="contained"
              color="primary"
            >
              POST A JOB
            </Button>
          </Link>
        </Hidden>
        <Hidden only={['sm', 'lg', 'md', 'xl']}>
          <Link to={pathManager.company.newJob.service.generatePath()}>
            <Fab
              className={classes.floatingAction}
              color="primary"
              variant="extended"
            >
              <AddIcon
                data-test-id={JOB_BOARD_TEST_DATA_ATTRS.addJobButton}
                fontSize="small"
              />
              &nbsp;Add new job
            </Fab>
          </Link>
        </Hidden>
      </Box>
      <JobTabs
        jobsCount={jobsCount as JobsCountByStatus[]}
        value={jobsType as JobTypeEnums}
      />
      {!!showDraftsAlter && <DraftsAlert />}
      <Grid
        className={classes.contentGrid}
        spacing={isSM ? 6 : 10}
        pt={isSM ? 6 : 10}
        component={Box}
        wrap="nowrap"
        container
      >
        <Grid
          className={classes.cardsContainer}
          component={Box}
          flexGrow={1}
          item
        >
          {jobs.map((item: any) => (
            <Box key={item?.id} pb={4}>
              <JobCard currentTime={currentTime} job={item} />
            </Box>
          ))}
          {!jobs.length && (
            <div>
              <NoItems appInfo={appInfo} jobType={jobsType as JobTypeEnums} />
            </div>
          )}
        </Grid>
        <Grid className={classes.overallContainer} item>
          {!isDraft && (
            <Overall
              jobsType={jobsType as JobTypeEnums}
              data={currentCompanyUserJobBoardInfo}
            />
          )}
        </Grid>
      </Grid>
      {!!jobs.length && (
        <Box pb={4}>
          <JobsPagination
            currentPage={jobsPagination?.currentPage}
            lastPage={jobsPagination?.lastPage}
          />
        </Box>
      )}
    </ConnectedPageLayout>
  );
};

export default JobBoard;
