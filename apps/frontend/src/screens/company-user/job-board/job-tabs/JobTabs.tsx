import ArchiveIcon from '@mui/icons-material/Archive';
import DraftsIcon from '@mui/icons-material/Drafts';
import WorkIcon from '@mui/icons-material/Work';
import { Tabs, Tab, Grid, Box } from '@mui/material';
import { usePushWithQuery } from 'hooks/routing';
import React, { useCallback } from 'react';

import { JobsCountByStatus, JobStatusEnum } from '@libs/graphql-types';

import { JobTypeEnums } from '../types';
import useStyles from './styles';

interface JobTabsProps {
  value: JobTypeEnums;
  jobsCount: JobsCountByStatus[];
}

const JobTabs = ({ value, jobsCount }: JobTabsProps) => {
  const classes = useStyles();
  const push = usePushWithQuery();
  const onChange = useCallback(
    (_, jobs: string) => {
      push({ query: { jobsType: jobs } });
    },
    [push],
  );
  const jobsCountMap = jobsCount.reduce<Record<JobStatusEnum, number>>(
    (acc, i) => {
      acc[i.type] = i.count;
      return acc;
    },
    {} as Record<JobStatusEnum, number>,
  );

  return (
    <Tabs
      value={value}
      onChange={onChange}
      indicatorColor="secondary"
      textColor="secondary"
      classes={{ root: classes.root }}
    >
      <Tab
        value={JobTypeEnums.Active}
        classes={{ root: classes.tab }}
        label={
          <Grid alignItems="center" spacing={2} container wrap="nowrap">
            <Grid component={Box} display="inline-flex" item>
              <WorkIcon />
            </Grid>
            <Grid item>{`Active (${jobsCountMap.ACTIVE || 0})`}</Grid>
          </Grid>
        }
      />
      <Tab
        value={JobTypeEnums.Archived}
        classes={{ root: classes.tab }}
        label={
          <Grid
            style={{ whiteSpace: 'nowrap' }}
            alignItems="center"
            spacing={2}
            container
            wrap="nowrap"
          >
            <Grid component={Box} display="inline-flex" item>
              <ArchiveIcon />
            </Grid>

            <Grid item>{`Archived (${jobsCountMap.ARCHIVED || 0})`}</Grid>
          </Grid>
        }
      />
      <Tab
        value={JobTypeEnums.Draft}
        classes={{ root: classes.tab }}
        label={
          <Grid
            style={{ whiteSpace: 'nowrap' }}
            alignItems="center"
            justifyContent="center"
            spacing={2}
            container
            wrap="nowrap"
          >
            <Grid component={Box} display="inline-flex" item>
              <DraftsIcon />
            </Grid>
            <Grid item>Drafts</Grid>
            <Grid item>({jobsCountMap.DRAFT || 0})</Grid>
          </Grid>
        }
      />
    </Tabs>
  );
};

export default JobTabs;
