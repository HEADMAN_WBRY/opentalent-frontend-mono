import { useQuery } from '@apollo/client';
import { useCurrentUser } from 'hooks/auth';
import { usePushWithQuery } from 'hooks/routing';
import { useCallback } from 'react';

import { QueryJobsArgs, Query } from '@libs/graphql-types';

import { GET_GOBS, GET_JOB_BOARD_INFO } from './queries';
import { JobTypeEnums } from './types';

export const useJobBoardInfo = ({
  jobType,
  page,
}: {
  jobType: JobTypeEnums;
  page: number;
}) => {
  const { data: userData } = useCurrentUser();
  const { data, loading } = useQuery<Query, QueryJobsArgs>(GET_GOBS, {
    fetchPolicy: 'network-only',
    variables: {
      is_archived: jobType === JobTypeEnums.Archived,
      is_draft: jobType === JobTypeEnums.Draft,
      company_id: userData?.currentCompanyUser.company?.id,
      page,
    },
  });

  const { data: boardData, loading: isBoardInfoLoading } = useQuery<
    Query,
    QueryJobsArgs
  >(GET_JOB_BOARD_INFO);
  const jobsCount = data?.currentCompanyUserJobsCountByStatus || {};
  const appInfo = boardData?.commonAppInfo;
  const jobs = data?.jobs?.data || [];
  const jobsPagination = data?.jobs?.paginatorInfo;
  const currentCompanyUserJobBoardInfo =
    boardData?.currentCompanyUserJobBoardInfo;

  return {
    appInfo,
    jobsCount,
    jobs,
    jobsPagination,
    currentCompanyUserJobBoardInfo,
    loading: isBoardInfoLoading || loading,
  };
};

export const usePaginationChange = () => {
  const push = usePushWithQuery();

  const onPaginationChange = useCallback(
    (e: React.ChangeEvent<unknown>, page) => {
      push({ query: { page } });
    },
    [push],
  );

  return onPaginationChange;
};
