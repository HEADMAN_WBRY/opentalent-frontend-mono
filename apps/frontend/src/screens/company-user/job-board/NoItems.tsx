import React from 'react';
import { Link } from 'react-router-dom';
import { pathManager } from 'routes';

import { CommonAppInfo } from '@libs/graphql-types';
import { formatNumberSafe } from '@libs/helpers/format';
import Typography from '@libs/ui/components/typography';

import { JobTypeEnums } from './types';

interface NoItemsProps {
  jobType: JobTypeEnums;
  appInfo?: CommonAppInfo;
}

const MAIL_MESSAGE = `Hey team OpenTalent,\n
I have a questions. Please get back to me asap.\n
Thanks!`;

const NoItems = ({ jobType, appInfo }: NoItemsProps) => {
  if (jobType === JobTypeEnums.Active) {
    // const countries = appInfo?.total_ot_recruiters_countries_count || 0;
    const totalFreelancers = formatNumberSafe(
      appInfo?.total_ot_approved_freelancers_count,
    );
    // const totalCompanies = formatNumberSafe(
    //   appInfo?.total_unique_companies_count,
    // );
    // const totalSkills = formatNumberSafe(appInfo?.total_unique_skills_count);
    const recruitersCount = formatNumberSafe(
      appInfo?.total_ot_recruiters_count,
    );

    return (
      <>
        <Typography variant="body1" color="textSecondary" paragraph>
          Hey there 👋
        </Typography>
        <Typography variant="body1" color="textSecondary" paragraph>
          Welcome to OpenTalent: the direct sourcing platform for hiring teams
          and independent recruiters.
        </Typography>
        <Typography variant="body1" color="textSecondary" paragraph>
          With us you can do three things:
        </Typography>
        <ol>
          <Typography
            component="li"
            variant="body1"
            color="textSecondary"
            paragraph
          >
            <Link
              style={{ textDecoration: 'underline' }}
              to={pathManager.company.createProfile.generatePath()}
            >
              Build your own talent pool
            </Link>{' '}
            to source people, commission-free.
          </Typography>
          <Typography
            component="li"
            variant="body1"
            color="textSecondary"
            paragraph
          >
            Search for talent from{' '}
            <Link
              style={{ textDecoration: 'underline' }}
              to={`${pathManager.company.workforce.generatePath()}?source_type=OPENTALENT;OpenTalent`}
            >
              OpenTalent’s network
            </Link>{' '}
            of {totalFreelancers} vetted ‘high-skilled’ freelancers across
            Europe.
          </Typography>
          <Typography
            component="li"
            variant="body1"
            color="textSecondary"
            paragraph
          >
            Work with{' '}
            <Link
              style={{ textDecoration: 'underline' }}
              to={pathManager.company.newJob.choose.generatePath()}
            >
              OpenTalent’s community of {recruitersCount} recruiters
            </Link>{' '}
            across 112 categories to close 'hard-to-fill' roles at record speed.
          </Typography>
        </ol>
        <Typography variant="body1" color="textSecondary" paragraph>
          If you have any questions, please send us a{' '}
          <a
            style={{ textDecoration: 'underline' }}
            href={`mailto:info@opentalent.co?subject=Question to OpenTalent&body=${encodeURIComponent(
              MAIL_MESSAGE,
            )}`}
          >
            email
          </a>
          .
        </Typography>
        <Typography variant="body1" color="textSecondary" paragraph>
          All the best,
        </Typography>
        <Typography variant="body1" color="textSecondary" paragraph>
          - Team OpenTalent.
        </Typography>
      </>
    );
  }
  if (jobType === JobTypeEnums.Draft) {
    return (
      <Typography variant="body1" color="textSecondary">
        No draft job positions
      </Typography>
    );
  }

  return (
    <Typography variant="body1" color="textSecondary">
      You don’t have archived positions yet.
    </Typography>
  );
};

export default NoItems;
