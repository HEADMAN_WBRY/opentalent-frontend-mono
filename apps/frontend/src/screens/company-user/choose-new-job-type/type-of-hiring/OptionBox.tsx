import cn from 'classnames';
import React from 'react';

import { Box, Paper } from '@mui/material';
import { makeStyles } from '@mui/styles';

import { JobTypeEnum } from '@libs/graphql-types';
import Checkbox from '@libs/ui/components/form/checkbox';
import Typography from '@libs/ui/components/typography';

interface OptionBoxProps {
  title: string;
  subtitle: string;
  info: string;
  isActive: boolean;
  type: JobTypeEnum;
  isBeta?: boolean;
  onClick: VoidFunction;
  disabled?: boolean;
}

const useStyles = makeStyles((theme) => ({
  root: {
    cursor: 'pointer',
    position: 'relative',
    height: '100%',
    textAlign: 'center',
    transition: 'border .3s',
    border: ({ isActive }: OptionBoxProps) =>
      isActive
        ? `2px solid ${theme.palette.success.main}`
        : `2px solid ${theme.palette.grey[300]}`,
  },
  container: {
    padding: theme.spacing(8, 6),
  },
  checkboxChecked: {
    color: `${theme.palette.success.main} !important`,
  },
  checkboxLabel: {
    margin: 0,
  },
  betaBadge: {
    padding: '6px 12px',
    borderRadius: '24px',
    position: 'absolute',
    right: '20px',
    top: '20px',
    color: `${theme.palette.text.secondary}`,
    backgroundColor: `${theme.palette.other.green}`,
    fontWeight: 'bold',
    textTransform: 'uppercase',
  },
  rootDisabled: {
    pointerEvents: 'none',

    '& $container': {
      opacity: 0.4,
    },
  },
}));

const OptionBox = (props: OptionBoxProps) => {
  const classes = useStyles(props);
  const { title, subtitle, info, isActive, type, onClick, disabled } = props;

  return (
    <Paper
      onClick={onClick}
      elevation={0}
      classes={{ root: cn(classes.root, { [classes.rootDisabled]: disabled }) }}
    >
      {disabled && <Box className={classes.betaBadge}>Coming soon!</Box>}
      {/* {isBeta && <Box className={classes.betaBadge}>BETA!</Box>} */}
      <Box className={classes.container} position="relative">
        <Checkbox
          checked={isActive}
          formControlLabelProps={{ classes: { root: classes.checkboxLabel } }}
          classes={{ checked: classes.checkboxChecked }}
          name={type}
        />
        <Typography paragraph variant="h5">
          {title}
        </Typography>
        <Typography paragraph variant="body1">
          {subtitle}
        </Typography>
        <Typography variant="body2" color="textSecondary">
          {info}
        </Typography>
      </Box>
    </Paper>
  );
};

export default OptionBox;
