import SideCardMark from 'components/side-card-mark';
import React from 'react';

import { Box } from '@mui/material';
import { makeStyles } from '@mui/styles';

import { EXTERNAL_LINKS } from '@libs/helpers/consts';
import { OuterLink } from '@libs/ui/components/typography';

import OptionBox, { OptionBoxProps } from './OptionBox';

interface DedicatedSearchBoxProps extends OptionBoxProps {}

const useStyles = makeStyles((theme) => ({
  content: {
    paddingTop: theme.spacing(4),
  },
  checkbox: {
    padding: '0 8px 0 0',
    alignSelf: 'flex-start',
  },
  section: {
    padding: '8px 0',
    flexGrow: 1,
    textAlign: 'left',

    '&:nth-child(2)': {
      borderLeft: `1px solid ${theme.palette.grey[300]}`,
      paddingLeft: theme.spacing(4),
    },
  },
  label: {
    marginLeft: 0,
  },
  link: {
    textDecoration: 'underline',
  },
}));

const DedicatedSearchBox = (props: DedicatedSearchBoxProps) => {
  const classes = useStyles();

  return (
    <>
      <OptionBox {...props}>
        <SideCardMark color="green">Coming soon</SideCardMark>
        <Box className={classes.content}>
          <OuterLink
            href={EXTERNAL_LINKS.pricingInfo}
            color="info.main"
            variant="body1"
            className={classes.link}
            target="_blank"
          >
            See our pricing models
          </OuterLink>
        </Box>
      </OptionBox>
    </>
  );
};

export default DedicatedSearchBox;
