import React from 'react';

const Workforce = React.lazy(() => import('./Workforce'));

export default Workforce;
