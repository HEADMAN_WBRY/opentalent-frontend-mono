import CampaignOutlinedIcon from '@mui/icons-material/CampaignOutlined';
import React from 'react';

import { RouterButton } from '@libs/ui/components/button';

import { ProfileModals } from '../modals';

interface SendAnnouncementProps {}

const SendAnnouncement = (props: SendAnnouncementProps) => {
  return (
    <RouterButton
      to={{
        state: {
          [ProfileModals.MessageToWorkforce]: true,
        },
      }}
      startIcon={<CampaignOutlinedIcon />}
    >
      Send announcement
    </RouterButton>
  );
};

export default SendAnnouncement;
