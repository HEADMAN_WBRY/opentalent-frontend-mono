import {
  BooleanModalState,
  isBooleanSearchEmpty,
  mapBooleanToServer,
} from 'components/custom/skills-boolean-search';
import * as qs from 'utils/querystring';

import { QueryTalentsSearchArgs, SourceTypeEnum } from '@libs/graphql-types';
import { isNil } from '@libs/helpers/common';
import { OptionType } from '@libs/ui/components/form/select';

import { EUROPE_COUNTRY, FilterValues } from './consts';

const getTalentStatus = ({
  is_active: active,
  is_inactive: inactive,
}: Partial<FilterValues>) => {
  if (active && inactive) {
    return null;
  }
  // eslint-disable-next-line no-nested-ternary
  return inactive ? false : active ? true : null;
};

export const stringifyBoolean = (booleanSearch?: BooleanModalState) => {
  return isBooleanSearchEmpty(booleanSearch)
    ? ''
    : JSON.stringify(booleanSearch);
};
const parseBoolean = (boolString: string) => {
  if (!boolString) {
    return undefined;
  }
  const parsed = JSON.parse(boolString);

  if (isBooleanSearchEmpty(parsed)) {
    return undefined;
  }

  return parsed;
};

export const mapFormValuesToQueryVariables = (
  values: Partial<FilterValues>,
): QueryTalentsSearchArgs => {
  const {
    search = '',
    category_ids = [],
    source_type,
    skills_ids = [],
    available_now,
    max_rate,
    min_rate,
    page,
    is_verification_required,
    booleanSearch,
    country,
    isBooleanSkills,
    is_recruiter,
    tags_ids,
  } = values;
  const categories = category_ids.map(({ value }) => String(value));
  const skills = skills_ids.map(({ value }) => String(value));
  const tags = tags_ids?.map(({ value }) => String(value)) || [];
  const isActive = getTalentStatus(values);
  const needRecruiter = source_type?.value === SourceTypeEnum.Opentalent;

  const skillsVariables =
    isBooleanSkills && booleanSearch
      ? {
          skills_boolean_filter: mapBooleanToServer(booleanSearch),
        }
      : (skills.length && { skills_ids: skills }) || {};

  return {
    country: country === EUROPE_COUNTRY ? undefined : country,
    ...(search.length >= 3 && { search }),
    ...(!!is_recruiter && needRecruiter && { is_recruiter: true }),
    ...(isActive !== null && { is_active: isActive }),
    ...(!!categories.length && { category_ids: categories }),
    ...(!!page && { page }),
    ...(!!tags.length && { tags_ids: tags }),
    ...(available_now && { available_now: true }),
    ...(isNil(is_verification_required)
      ? {}
      : { is_verification_required: !!is_verification_required }),
    ...skillsVariables,
    source_type: (source_type?.value || SourceTypeEnum.Own) as SourceTypeEnum,
    max_rate,
    min_rate,
  };
};

const mapOptionsToQuery = (opts: OptionType[]) =>
  opts.map(({ value, text, skill_type }) => {
    const arr = [value, text];
    return skill_type ? arr.concat(skill_type) : arr;
  });
export const mapValuesToQuery = (values: FilterValues) => {
  const {
    category_ids,
    skills_ids,
    source_type,
    isBooleanSkills,
    booleanSearch,
    tags_ids,
  } = values;

  const params = {
    ...mapFormValuesToQueryVariables(values),
    ...(!!category_ids?.length && {
      category_ids: mapOptionsToQuery(category_ids),
    }),
    ...(!!tags_ids?.length && {
      tags_ids: mapOptionsToQuery(tags_ids),
    }),
    ...(!!skills_ids?.length && { skills_ids: mapOptionsToQuery(skills_ids) }),

    source_type: mapOptionsToQuery([source_type])[0],
    isBooleanSkills: isBooleanSkills ? '1' : '',
    booleanSearch: stringifyBoolean(booleanSearch),
  };
  return params;
};

const mapOptionsFromQuery = (
  str: string,
  { typeName }: { typeName?: string } = {},
) =>
  str?.split(';').map((s) => {
    const [value, text, type] = s?.split(',');
    return { value, text, ...(type && typeName && { [typeName]: type }) };
  });

export const getValuesFromQuerystring = (s: string): Partial<FilterValues> => {
  const {
    skills_ids,
    category_ids,
    source_type,
    is_active,
    search,
    min_rate,
    max_rate,
    available_now,
    page,
    is_verification_required,
    booleanSearch,
    isBooleanSkills,
    country,
    is_recruiter,
    tags_ids,
  } = qs.parse(s);

  return {
    search: search as string,
    min_rate: min_rate as number,
    max_rate: max_rate as number,
    country: (country as string) || EUROPE_COUNTRY,
    is_verification_required: !!is_verification_required,
    booleanSearch: parseBoolean(booleanSearch as string),
    isBooleanSkills: !!isBooleanSkills,

    available_now: !!available_now,
    page: page as number,

    is_recruiter: !!is_recruiter,

    ...(is_active === undefined
      ? { is_active: true, is_inactive: false }
      : { is_active: !!is_active, is_inactive: !is_active }),
    ...(!!source_type && {
      source_type: mapOptionsFromQuery(source_type as string)[0],
    }),
    ...(!!category_ids && {
      category_ids: mapOptionsFromQuery(category_ids as string),
    }),
    ...(!!tags_ids && {
      tags_ids: mapOptionsFromQuery(tags_ids as string),
    }),
    ...(!!skills_ids && {
      skills_ids: mapOptionsFromQuery(skills_ids as string, {
        typeName: 'skill_type',
      }),
    }),
  };
};
