import { ReactComponent as SearchIcon } from 'assets/icons/search.svg';
import FormikAutoSaving from 'components/form/formik/FormikAutoSave';
import { Form } from 'formik';
import { TALENT_CATEGORIES } from 'graphql/talents';
import useMediaQueries from 'hooks/common/useMediaQueries';
import React from 'react';

import CloseIcon from '@mui/icons-material/Close';
import {
  Box,
  Collapse,
  FormControl,
  FormGroup,
  Grid,
  IconButton,
  InputAdornment,
  Paper,
} from '@mui/material';

import { useGetCurrentUserCompanyTagsQuery } from '@libs/graphql-types';
import { modelPath } from '@libs/helpers/form';
import Accordion from '@libs/ui/components/accordion/Accordion';
import { ConnectedCheckbox } from '@libs/ui/components/form/checkbox';
import ConnectedMultipleSelect, {
  GraphConnectedMultipleSelect,
} from '@libs/ui/components/form/multiple-select/ConnectedMultipleSelect';
import { ConnectedSelect, OptionType } from '@libs/ui/components/form/select';
import { ConnectedTextField } from '@libs/ui/components/form/text-field';
import Typography from '@libs/ui/components/typography';

import CircleControl from './circle-control';
import { COUNTRY_OPTIONS_CUSTOM, FilterValues } from './consts';
import RateSelector from './rate-selector';
import { SkillsSelector } from './skills-selector';
import useStyles from './styles';

export interface SearchFilterProps {
  closeDrawer?: VoidFunction;
  isOpentalentSource?: boolean;
}

const SearchFilter = (props: SearchFilterProps) => {
  const { isSM } = useMediaQueries();
  const classes = useStyles(props);
  const { closeDrawer, isOpentalentSource } = props;
  const { data } = useGetCurrentUserCompanyTagsQuery();
  const tagOptions = (data?.currentUserCompanyTags || []).map((i) => ({
    text: i?.name,
    value: i?.id,
  }));

  return (
    <Paper elevation={0} className={classes.paper}>
      <Form>
        <FormikAutoSaving debounceMs={1000} />
        {isSM && (
          <Box
            mb={4}
            display="flex"
            justifyContent="space-between"
            alignItems="center"
          >
            <Typography variant="subtitle1">Search</Typography>
            <IconButton onClick={closeDrawer} size="large">
              <CloseIcon />
            </IconButton>
          </Box>
        )}
        <Box>
          <Accordion
            defaultExpanded
            summary={
              <Typography variant="overline">SELECT COMMUNITY</Typography>
            }
            details={
              <Box mb={6} className={classes.box}>
                <CircleControl />
              </Box>
            }
          />
        </Box>
        <Box>
          <Accordion
            defaultExpanded
            summary={<Typography variant="overline">SEARCH</Typography>}
            details={
              <Box mb={6} className={classes.box}>
                <ConnectedTextField
                  label="Search by Name, Job title, Work history, ..."
                  name="search"
                  variant="filled"
                  fullWidth
                  autoComplete="off"
                  className={classes.searchByInput}
                  InputProps={{
                    startAdornment: (
                      <InputAdornment position="start">
                        <SearchIcon width="14" />
                      </InputAdornment>
                    ),
                  }}
                />
              </Box>
            }
          />
        </Box>
        <Box>
          <Accordion
            defaultExpanded
            summary={
              <Typography variant="overline">FILTER BY SKILLS</Typography>
            }
            details={
              <Box mb={6} className={classes.box}>
                <SkillsSelector />
              </Box>
            }
          />
        </Box>
        <Box>
          <Accordion
            defaultExpanded
            summary={
              <Typography variant="overline">FILTER BY CATEGORY</Typography>
            }
            details={
              <Box mb={6} className={classes.box}>
                <GraphConnectedMultipleSelect
                  name="category_ids"
                  query={TALENT_CATEGORIES}
                  dataPath="talentCategories"
                  dataMap={{ text: 'name', value: 'id' }}
                  chipProps={{
                    color: 'tertiary',
                    size: 'small',
                  }}
                  inputProps={{
                    variant: 'filled',
                    label: 'Pick from list',
                    placeholder: '',
                    margin: 'dense',
                  }}
                />
              </Box>
            }
          />
        </Box>
        <Box>
          <Accordion
            defaultExpanded
            summary={
              <Typography variant="overline">FILTER BY COUNTRY</Typography>
            }
            details={
              <Box mb={6} className={classes.box}>
                <ConnectedSelect
                  name="country"
                  fullWidth
                  variant="filled"
                  label="Pick from list"
                  options={COUNTRY_OPTIONS_CUSTOM}
                  hideNoneValue
                  displayEmpty
                />
              </Box>
            }
          />
        </Box>
        <Box>
          <Accordion
            defaultExpanded
            summary={<Typography variant="overline">STATUS</Typography>}
            details={
              <Box ml={3} mb={6} className={classes.box}>
                <FormControl fullWidth component="fieldset">
                  <FormGroup>
                    <Grid
                      container
                      justifyContent="space-between"
                      alignItems="center"
                    >
                      <Grid item>
                        <ConnectedCheckbox
                          name={modelPath<FilterValues>((m) => m.is_active)}
                          label="Active"
                          color="primary"
                        />
                      </Grid>
                      {/* <Grid item>
                            <div className={classes.badge}>12</div>
                          </Grid> */}
                    </Grid>
                    <Grid
                      container
                      justifyContent="space-between"
                      alignItems="center"
                    >
                      <Grid item>
                        <ConnectedCheckbox
                          label="Inactive"
                          color="primary"
                          name={modelPath<FilterValues>((m) => m.is_inactive)}
                        />
                      </Grid>
                    </Grid>
                    {/* <Grid
                      container
                      justifyContent="space-between"
                      alignItems="center"
                    >
                      <Grid item>
                        <ConnectedCheckbox
                          label="Pending verification"
                          color="primary"
                          name={modelPath<FilterValues>(
                            (m) => m.is_verification_required,
                          )}
                        />
                      </Grid>
                    </Grid> */}
                  </FormGroup>
                </FormControl>
              </Box>
            }
          />
        </Box>
        <Box>
          <Accordion
            defaultExpanded
            summary={
              <Typography variant="overline" transform="uppercase">
                Availability
              </Typography>
            }
            details={
              <Box ml={3} mb={6} className={classes.box}>
                <ConnectedCheckbox
                  name="available_now"
                  label="Available now"
                  color="primary"
                />
              </Box>
            }
          />
        </Box>
        <Box>
          <Accordion
            defaultExpanded
            summary={<Typography variant="overline">HOURLY RATE</Typography>}
            details={
              <Box ml={3} mb={6} className={classes.box}>
                <RateSelector
                  minValue={{ name: 'min_rate' }}
                  maxValue={{ name: 'max_rate' }}
                />
              </Box>
            }
          />
        </Box>
        <Collapse in={isOpentalentSource}>
          <div>
            <Accordion
              defaultExpanded
              summary={
                <Typography transform="uppercase" variant="overline">
                  Type
                </Typography>
              }
              details={
                <Box ml={3} mb={6} className={classes.box}>
                  <FormControl fullWidth component="fieldset">
                    <FormGroup>
                      <Grid
                        container
                        justifyContent="space-between"
                        alignItems="center"
                      >
                        <Grid item>
                          <ConnectedCheckbox
                            name={modelPath<FilterValues>(
                              (m) => m.is_recruiter,
                            )}
                            label="Recruiter"
                            color="primary"
                          />
                        </Grid>
                      </Grid>
                    </FormGroup>
                  </FormControl>
                </Box>
              }
            />
          </div>
        </Collapse>
        <Collapse in={!!tagOptions.length}>
          <div>
            <Accordion
              defaultExpanded
              summary={
                <Typography transform="uppercase" variant="overline">
                  Tags
                </Typography>
              }
              details={
                <ConnectedMultipleSelect
                  name="tags_ids"
                  size="small"
                  options={tagOptions}
                  autoCompleteProps={{
                    filterSelectedOptions: true,
                    isOptionEqualToValue: (
                      opt: OptionType,
                      value: OptionType,
                    ) => opt.value === value.value,
                    limitTags: 3,
                  }}
                  chipProps={{
                    size: 'small',
                  }}
                  inputProps={{
                    label: 'Filter by tag',
                  }}
                />
              }
            />
          </div>
        </Collapse>
      </Form>
    </Paper>
  );
};

export default React.memo(SearchFilter);
