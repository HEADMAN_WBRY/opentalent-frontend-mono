import React from 'react';

import { Grid } from '@mui/material';

import { Talent } from '@libs/graphql-types';
import Chip from '@libs/ui/components/chip';

interface CategoriesListProps {
  talent: Talent;
}

const CategoriesList = ({ talent }: CategoriesListProps) => {
  const category = talent?.category?.name;
  const subcategories = talent?.category?.subcategories || [];

  if (!category) {
    return <></>;
  }

  return (
    <Grid spacing={2} container>
      {subcategories.map((i) => (
        <Grid key={i.id} item>
          <Chip color="grey" size="small" label={i.name} />
        </Grid>
      ))}
    </Grid>
  );
};

export default CategoriesList;
