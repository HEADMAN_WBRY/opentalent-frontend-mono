import { makeStyles } from '@mui/styles';

const useStyles = makeStyles((theme) => ({
  talentCard: {
    border: `1px solid ${theme.palette.grey[200]}`,
    borderRadius: theme.shape.borderRadius,
  },
  talentAvatarWrap: {
    minWidth: 70,
  },
  nameRow: {
    paddingTop: theme.spacing(1),
    height: 36,
  },
  talentAvatar: {
    width: 56,
    height: 56,
  },
}));

export default useStyles;
