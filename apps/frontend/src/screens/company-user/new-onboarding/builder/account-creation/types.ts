export interface CreationInfo {
  firstName: string;
  lastName: string;
  email: string;
  companyName: string;
  agreement: boolean;
  id?: string;
}
