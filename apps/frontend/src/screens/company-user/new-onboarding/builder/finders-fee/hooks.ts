import { useSnackbar } from 'notistack';
import { useCallback } from 'react';
import { useHistory } from 'react-router-dom';
import { pathManager } from 'routes';

import { useCreateCompanyUserMutation } from '@libs/graphql-types';
import { FormikSubmit } from '@libs/helpers/form';

import { FindersFeeInfo } from './types';

export const useSubmitHandler = () => {
  const { enqueueSnackbar } = useSnackbar();
  const history = useHistory();
  const [createUser, { loading }] = useCreateCompanyUserMutation({
    onCompleted() {
      enqueueSnackbar('User account successfully created', {
        variant: 'success',
      });

      history.push(
        pathManager.company.newOnboarding.builder.step.generatePath({
          step: 7,
        }),
      );
    },
  });
  const onSubmit: FormikSubmit<FindersFeeInfo> = useCallback(
    async (values) => {
      await createUser({});
    },
    [createUser],
  );

  return { onSubmit, loading };
};
