import { Box, Grid, Typography } from '@mui/material';
import { makeStyles } from '@mui/styles';
import React from 'react';
import { DefaultModalProps, withLocationStateModal } from 'utils/modals';

import Button from '@libs/ui/components/button';
import { DefaultModal } from '@libs/ui/components/modals';

import { MODAL_TYPES } from '../consts';
import { useSubmitHandler } from './hooks';

interface ArchiveModalProps extends DefaultModalProps<true> {
  jobId: string;
}

const useStyles = makeStyles((theme) => ({
  archiveButton: {
    background: `${theme.palette.error.light} !important`,
  },
}));

const ArchiveModalComponent = ({ isOpen, close, jobId }: ArchiveModalProps) => {
  const classes = useStyles();
  const { onSubmit, loading } = useSubmitHandler({ jobId, handleClose: close });

  return (
    <DefaultModal
      handleClose={close}
      open={isOpen}
      title="Be aware"
      actions={
        <Grid spacing={4} container>
          <Grid xs={6} item>
            <Button
              className={classes.archiveButton}
              fullWidth
              color="inherit"
              variant="contained"
              onClick={onSubmit}
              disabled={loading}
            >
              Proceed
            </Button>
          </Grid>
          <Grid xs={6} item>
            <Button
              fullWidth
              variant="outlined"
              color="secondary"
              onClick={close}
              disabled={loading}
            >
              Cancel
            </Button>
          </Grid>
        </Grid>
      }
    >
      <Box>
        <Typography>
          This campaign is currently live.
          <br /> If you Archive it now it will be stopped.
        </Typography>
      </Box>
    </DefaultModal>
  );
};

export const ArchiveModal = withLocationStateModal({ id: MODAL_TYPES.ARCHIVE })(
  ArchiveModalComponent,
);
