import { ReactComponent as CheckIcon } from 'assets/icons/check.svg';
import { ReactComponent as ExclamationStarYellowIcon } from 'assets/icons/exclamation-star-yellow.svg';
import { ReactComponent as MapPinIcon } from 'assets/icons/map-pin.svg';
import { DEFAULT_AVATAR } from 'consts/common';
import { useTalentName } from 'hooks/talents';
import React from 'react';
import { Link } from 'react-router-dom';
import { pathManager } from 'routes/consts';

import { Avatar, Badge, Box, Grid } from '@mui/material';
import { makeStyles } from '@mui/styles';

import { Talent } from '@libs/graphql-types';
import Chip from '@libs/ui/components/chip/Chip';
import Typography from '@libs/ui/components/typography';

interface UserBlockProps {
  talent?: Talent;
  isNewMatch: boolean;
}

const useStyles = makeStyles((theme) => ({
  root: {
    flexWrap: 'nowrap',
  },
  avatar: {
    width: 96,
    height: 96,

    [theme.breakpoints.down('md')]: {
      'body &': {
        width: 56,
        height: 56,
      },
    },
  },
  checkIcon: {
    color: theme.palette.info.main,
  },
  pinIcon: {
    height: 18,
    marginBottom: -theme.spacing(1),
    paddingRight: theme.spacing(1),
  },
  badge: {
    background: ({ talent }: UserBlockProps) =>
      talent?.available_now
        ? theme.palette.primary.light
        : theme.palette.grey[500],
    width: 20,
    height: 20,
    border: `2px solid ${theme.palette.grey[200]}`,
  },
}));

const UserBlock = (props: UserBlockProps) => {
  const { talent, isNewMatch } = props;
  const classes = useStyles(props);
  const name = useTalentName(talent);

  return (
    <Grid className={classes.root} container spacing={4}>
      <Grid item>
        <Link
          to={pathManager.company.talentProfile.generatePath({
            id: talent?.id || '',
          })}
        >
          <Badge
            overlap="circular"
            classes={{ badge: classes.badge }}
            anchorOrigin={{
              vertical: 'bottom',
              horizontal: 'right',
            }}
            variant="dot"
          >
            <Avatar
              classes={{ root: classes.avatar }}
              alt="avatar"
              src={talent?.avatar?.avatar || DEFAULT_AVATAR}
            />
          </Badge>
        </Link>
      </Grid>
      <Grid item>
        <Grid wrap="nowrap" alignItems="center" container>
          <Grid item>
            <Link
              to={pathManager.company.talentProfile.generatePath({
                id: talent?.id || '',
              })}
            >
              <Typography component="span" variant="h6">
                {name}
              </Typography>
            </Link>
          </Grid>
          {talent?.is_invitation_accepted && (
            <Grid display="flex" component={Box} item pl={2}>
              <CheckIcon className={classes.checkIcon} />
            </Grid>
          )}
          {isNewMatch && (
            <Grid display="flex" component={Box} item pl={2}>
              <Chip
                icon={<ExclamationStarYellowIcon />}
                style={{ padding: '2px 8px', color: 'white' }}
                label="New match"
                size="small"
                color="successDark"
              />
            </Grid>
          )}
        </Grid>
        <Box pt={1} pb={1}>
          <Typography variant="body2">
            {talent?.recent_position_title}
          </Typography>
        </Box>
        <Grid alignItems="center" container>
          <Grid component={Box} pr={10} display="flex" flexWrap="nowrap" item>
            <Typography
              color="textSecondary"
              component="span"
              variant="subtitle2"
            >
              Hourly Rate:
            </Typography>
            &nbsp;
            <Typography color="textSecondary" component="span" variant="body2">
              {`${[talent?.rate_min, talent?.rate_max]
                .filter(Boolean)
                .join('-')}€/h`}
            </Typography>
          </Grid>
          <Grid display="flex" component={Box} item>
            <Typography color="textSecondary" component="span" variant="body2">
              <MapPinIcon className={classes.pinIcon} />
              {talent?.location}
            </Typography>
          </Grid>
        </Grid>
      </Grid>
    </Grid>
  );
};

export default UserBlock;
