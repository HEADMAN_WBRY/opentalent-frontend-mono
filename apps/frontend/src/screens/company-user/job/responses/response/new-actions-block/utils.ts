import { JobMatchTypeEnum } from '@libs/graphql-types';

export const getMenuTitle = (jobMachType: JobMatchTypeEnum) => {
  switch (jobMachType) {
    case JobMatchTypeEnum.Rejected:
      return 'Rejected';
    case JobMatchTypeEnum.Withdrawn:
      return 'Withdrawn';
    case JobMatchTypeEnum.Hired:
      return 'Hired';
    default:
      return 'actions';
  }
};

export const isDisabled = (jobMachType: JobMatchTypeEnum) => {
  switch (jobMachType) {
    case JobMatchTypeEnum.Rejected:
    case JobMatchTypeEnum.Withdrawn:
    case JobMatchTypeEnum.Hired:
      return true;
    default:
      return false;
  }
};
