import { useMutation } from '@apollo/client';
import { useSnackbar } from 'notistack';
import { useCallback } from 'react';

import { Mutation, MutationDeclineJobMatchArgs } from '@libs/graphql-types';

import { DECLINE_JOB_MATCH, GET_JOB_INFO } from '../../queries';

export const useInviteHandler = ({
  jobMatchId,
  onClose,
  jobId,
}: {
  jobMatchId: string;
  jobId: string;
  onClose: VoidFunction;
}) => {
  const { enqueueSnackbar } = useSnackbar();
  const [invite, { loading }] = useMutation<
    Mutation,
    MutationDeclineJobMatchArgs
  >(DECLINE_JOB_MATCH, {
    onCompleted: () => {
      enqueueSnackbar('Instant match was successfully removed', {
        variant: 'success',
      });
      onClose();
    },
    refetchQueries: [{ query: GET_JOB_INFO, variables: { id: jobId } }],
  });

  const onSubmit = useCallback(() => {
    invite({ variables: { job_match_id: jobMatchId } });
  }, [invite, jobMatchId]);

  return { onSubmit, loading };
};
