export interface FormState {
  message?: string;
}

export interface InviteModalData {
  talentId: string;
  firstName: string;
}
