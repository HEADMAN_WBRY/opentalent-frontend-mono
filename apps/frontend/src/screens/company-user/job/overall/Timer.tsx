import { Box, Grid, Typography } from '@mui/material';
import { makeStyles } from '@mui/styles';
import cn from 'classnames';
import React from 'react';
import { useIntervalTimer } from 'screens/talent/job-apply/job-info/hooks';
import { CampaignStatus } from 'utils/job';

interface TimerProps {
  startDate: string;
  endDate: string;
  campaignStatus: CampaignStatus;
  currentTime: Date;
}

const useStyles = makeStyles(() => ({
  timeText: {
    width: 42,
    display: 'inline-flex',
  },
  divider: {
    lineHeight: '30px',
  },
  dividerLeft: {
    paddingRight: 4,
  },
  dividerRight: {
    paddingLeft: 4,
  },
  labels: {
    opacity: 0.6,
  },
  minutes: {
    width: 36,
    position: 'relative',
    left: -4,
    display: 'inline-flex',
  },
}));

const formatTimeValue = (val = 0, status: CampaignStatus) =>
  status === CampaignStatus.Finished ? '00' : val.toString().padStart(2, '0');

const Timer = ({
  endDate,
  startDate,
  campaignStatus,
  currentTime,
}: TimerProps) => {
  const classes = useStyles();
  const duration = useIntervalTimer({
    start: startDate,
    end: endDate,
    currentTime,
  });

  return (
    <Grid spacing={1} container>
      <Grid item>
        <Grid alignItems="center" container direction="column">
          <Grid className={classes.timeText} item>
            <Typography variant="h4" color="textSecondary">
              {formatTimeValue(duration?.days, campaignStatus)}
            </Typography>
          </Grid>
          <Grid item>
            <Typography
              className={classes.labels}
              variant="caption"
              color="textSecondary"
            >
              Day
            </Typography>
          </Grid>
        </Grid>
      </Grid>
      <Grid item>
        <Grid alignItems="center" container direction="column">
          <Grid display="flex" component={Box} item>
            <Typography
              className={cn(classes.divider, classes.dividerLeft)}
              component="span"
              variant="h4"
              color="textSecondary"
            >
              :
            </Typography>
            <Typography
              className={classes.timeText}
              component="span"
              variant="h4"
              color="textSecondary"
            >
              {formatTimeValue(duration?.hours, campaignStatus)}
            </Typography>
            <Typography
              className={cn(classes.divider, classes.dividerRight)}
              component="span"
              variant="h4"
              color="textSecondary"
            >
              :
            </Typography>
          </Grid>
          <Grid item>
            <Typography
              className={classes.labels}
              variant="caption"
              color="textSecondary"
            >
              Hours
            </Typography>
          </Grid>
        </Grid>
      </Grid>
      <Grid item>
        <Grid alignItems="center" container direction="column">
          <Grid className={classes.timeText} item>
            <Typography variant="h4" color="textSecondary">
              {formatTimeValue(duration?.minutes, campaignStatus)}
            </Typography>
          </Grid>
          <Grid item>
            <Typography
              className={cn(classes.minutes, classes.labels)}
              variant="caption"
              color="textSecondary"
            >
              Minutes
            </Typography>
          </Grid>
        </Grid>
      </Grid>
    </Grid>
  );
};

export default React.memo(Timer);
