import { useSnackbar } from 'notistack';
import { useCallback } from 'react';

import { useCreateJobSuggestionMutation } from '@libs/graphql-types';
import { FormikSubmit } from '@libs/helpers/form';

import { IFormState } from './propose-modal/types';

export const useProposeHandler = ({
  onSuccess,
  jobId,
}: {
  onSuccess: VoidFunction;
  jobId: string;
}) => {
  const { enqueueSnackbar } = useSnackbar();
  const [request, { loading }] = useCreateJobSuggestionMutation();

  const onSubmit: FormikSubmit<IFormState> = useCallback(
    async (values) => {
      enqueueSnackbar('Success!', { variant: 'success' });

      const res = await request({
        variables: {
          job_id: jobId,
          email: values.email,
          name: values.firstName,
        },
      });

      if (res.data?.createJobSuggestion) {
        onSuccess();
      }
    },
    [enqueueSnackbar, jobId, onSuccess, request],
  );

  return { onSubmit, isLoading: loading };
};
