import { useApolloClient, useQuery } from '@apollo/client';
import { useSnackbar } from 'notistack';
import qs from 'query-string';
import { useCallback, useState } from 'react';
import { useLocation } from 'react-router-dom';

import { Query } from '@libs/graphql-types';

import { GET_JOB_INFO, SAVE_JOB_MATCH, DELETE_JOB_MATCH } from './queries';

const POLL_INTERVAL = 5000;

export const useJobScreenData = ({ id }: { id: string }) => {
  const { data, loading } = useQuery<Query>(GET_JOB_INFO, {
    variables: { id },
    pollInterval: POLL_INTERVAL,
  });

  return { data, loading };
};

export const useEuropeFilterSearch = () => {
  const location = useLocation();
  const europeSearch = qs.stringify({
    ...qs.parse(location.search.slice(1)),
    location: '',
  });
  return europeSearch;
};

export const useSaveAction = ({ id }: { id: string }) => {
  const { enqueueSnackbar } = useSnackbar();
  const [globalLoading, setGlobalLoading] = useState(false);
  const client = useApolloClient();
  const action = useCallback(
    async ({ isSaved, matchId }: { isSaved: boolean; matchId: string }) => {
      setGlobalLoading(true);
      await client.mutate({
        mutation: isSaved ? DELETE_JOB_MATCH : SAVE_JOB_MATCH,
        variables: { id: matchId },
      });
      await client.query({
        query: GET_JOB_INFO,
        variables: { id },
        fetchPolicy: 'network-only',
      });
      setGlobalLoading(false);
      enqueueSnackbar(
        isSaved
          ? 'Candidate successfully removed from shortlist'
          : 'Candidate successfully added to shortlist',
        {
          variant: 'success',
        },
      );
    },
    [client, enqueueSnackbar, id],
  );

  return { action, isLoading: globalLoading };
};
