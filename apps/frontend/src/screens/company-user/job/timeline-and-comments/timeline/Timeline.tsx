import { RecordList } from 'components/custom/tracking-system';
import React from 'react';

import { Box, CircularProgress } from '@mui/material';
import { makeStyles } from '@mui/styles';

import { Job } from '@libs/graphql-types';

import { useJobATSRecords } from './hooks';

interface TimelineProps {
  job: Job;
}

const useStyles = makeStyles((theme) => ({
  root: {
    overflow: 'hidden',
    height: '100%',
    paddingBottom: theme.spacing(4),
  },
  recordList: {
    padding: theme.spacing(2, 4, 0),
  },
}));

export const Timeline = ({ job }: TimelineProps) => {
  const { records, loading } = useJobATSRecords(job.id);
  const classes = useStyles();

  return (
    <Box className={classes.root} pt={4}>
      <RecordList className={classes.recordList} records={records} />

      {!!loading && (
        <Box
          p={4}
          style={{ width: '100%' }}
          display="flex"
          justifyContent="center"
        >
          <CircularProgress color="secondary" />
        </Box>
      )}
    </Box>
  );
};
