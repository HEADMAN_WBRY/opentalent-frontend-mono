import { JobMatchTypeEnum } from '@libs/graphql-types';

export const MATCHES_TABS = {
  instant: JobMatchTypeEnum.InstantMatch,
  talent: JobMatchTypeEnum.TalentApplication,
  invited: JobMatchTypeEnum.Invited,
  saved: 'SHORTLIST',
  rejected: JobMatchTypeEnum.Rejected,
  withdrawn: JobMatchTypeEnum.Withdrawn,
  hired: JobMatchTypeEnum.Hired,
  intake: JobMatchTypeEnum.Intake,
} as const;

export const MODAL_TYPES = {
  INVITE: 'invite',
  DECLINE: 'decline',
  REMOVE: 'remove',
  ARCHIVE: 'archive',
};
