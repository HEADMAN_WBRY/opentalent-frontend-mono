import CreateIcon from '@mui/icons-material/Create';
import DeleteForeverIcon from '@mui/icons-material/DeleteForever';
import MoreVertIcon from '@mui/icons-material/MoreVert';
import {
  Paper,
  TableContainer,
  Table,
  TableHead,
  TableRow,
  TableCell,
  TableBody,
  IconButton,
  Menu,
  MenuItem,
  Avatar,
  Grid,
} from '@mui/material';
import { DEFAULT_AVATAR } from 'consts/common';
import { usePushWithQuery } from 'hooks/routing';
import PopupState, { bindTrigger, bindMenu } from 'material-ui-popup-state';
import React from 'react';
import { pathManager } from 'routes';
import { useOpenModal } from 'utils/modals';
import { formatName } from 'utils/talent';

import { User } from '@libs/graphql-types';

import { DeleteUserModalData } from './delete-user-modal';
import { Modals } from './types';

interface UsersTableProps {
  colleagues: User[];
}

const UsersTable = ({ colleagues }: UsersTableProps) => {
  const push = usePushWithQuery();
  const openDeleteUserModal = useOpenModal<DeleteUserModalData>(Modals.Delete);
  const onDelete = (close: VoidFunction, id: string) => () => {
    openDeleteUserModal({ userId: id });
    close();
  };

  return (
    <TableContainer component={Paper}>
      <Table aria-label="customized table">
        <TableHead>
          <TableRow>
            <TableCell>Name</TableCell>
            <TableCell>Email</TableCell>

            <TableCell align="right">Actions</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {colleagues.map((item) => (
            <TableRow key={item.first_name}>
              <TableCell component="th" scope="row">
                <Grid alignItems="center" spacing={2} container>
                  <Grid item>
                    <Avatar src={item?.avatar?.avatar || DEFAULT_AVATAR} />
                  </Grid>
                  <Grid item>
                    {formatName({
                      firstName: item.first_name,
                      lastName: item.last_name,
                    })}
                  </Grid>
                </Grid>
              </TableCell>

              <TableCell>{item.email}</TableCell>

              <TableCell align="right">
                <PopupState variant="popover" popupId="demo-popup-menu">
                  {(popupState) => (
                    <>
                      <IconButton size="small" {...bindTrigger(popupState)}>
                        <MoreVertIcon />
                      </IconButton>
                      <Menu {...bindMenu(popupState)}>
                        <MenuItem
                          onClick={() => {
                            popupState.close();
                            push({
                              pathname:
                                pathManager.company.users.edit.generatePath({
                                  id: item.id || '',
                                }),
                              query: {},
                            });
                          }}
                        >
                          <CreateIcon style={{ paddingRight: 8 }} />
                          Edit user
                        </MenuItem>
                        <MenuItem
                          onClick={onDelete(
                            popupState.close,
                            item.id as string,
                          )}
                        >
                          <DeleteForeverIcon style={{ paddingRight: 8 }} />
                          Delete user
                        </MenuItem>
                      </Menu>
                    </>
                  )}
                </PopupState>
              </TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
};

export default UsersTable;
