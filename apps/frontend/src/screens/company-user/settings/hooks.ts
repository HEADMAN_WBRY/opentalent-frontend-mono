import { useMutation } from '@apollo/client';
import REQUEST_COMPANY_USER from 'graphql/user/currentCompanyUser';
import { useCurrentUser } from 'hooks/auth';
import { useSearchParams, usePushWithQuery } from 'hooks/routing';
import { useSnackbar } from 'notistack';

import { Mutation } from '@libs/graphql-types';

import { REQUEST_CONTRACT } from './queries';
import { SettingsTabs } from './types';

export const useSettingsTabs = () => {
  const { tab = SettingsTabs.General } = useSearchParams();
  const push = usePushWithQuery();
  const onTabChange = (e: React.ChangeEvent<unknown>, tab: string) =>
    push({ query: { tab } });

  return { tab, onTabChange };
};

export const useClientContract = () => {
  const { data } = useCurrentUser();
  const contract = data?.currentCompanyUser?.company?.contract;

  return contract;
};

export const useRequestContract = () => {
  const { enqueueSnackbar } = useSnackbar();
  const [request, { loading }] = useMutation<Mutation>(REQUEST_CONTRACT, {
    onCompleted: () => {
      enqueueSnackbar('Contract requested', { variant: 'success' });
    },
    // eslint-disable-next-line no-console
    onError: console.error,
    refetchQueries: [{ query: REQUEST_COMPANY_USER }],
  });

  return { request, isLoading: loading };
};
