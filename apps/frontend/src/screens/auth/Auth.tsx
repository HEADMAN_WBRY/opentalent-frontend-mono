import { useAuth0 } from '@auth0/auth0-react';
import Box from '@mui/material/Box';
import Container from '@mui/material/Container';
import Grid from '@mui/material/Grid';
import Paper from '@mui/material/Paper';
import { ReactComponent as AuthScreenIllustration } from 'assets/images/auth-page-illustration.svg';
import PageLayout from 'components/layout/page-layout';
import React, { useCallback } from 'react';

import Button from '@libs/ui/components/button';
import Typography from '@libs/ui/components/typography';

import { useAuthRedirect } from './hooks';
import useStyles from './styles';

interface AuthProps {}

const Auth = (props: AuthProps) => {
  const classes = useStyles(props);
  const { loginWithRedirect } = useAuth0();

  const handleLogin = useCallback(() => {
    loginWithRedirect({});
  }, [loginWithRedirect]);

  const { isLoading } = useAuthRedirect();

  return (
    <PageLayout documentTitle="Sign in" isLoading={isLoading} centered>
      <Container className={classes.container}>
        <Grid
          className={classes.gridContainer}
          alignItems="center"
          justifyContent="space-between"
          container
          spacing={4}
        >
          <Grid className={classes.infoColumn} sm={7} xs={12} item>
            <Box>
              <Typography className={classes.title} variant="h3">
                The World’s most trusted remote talent pool.
              </Typography>
            </Box>
            <Box>
              <Typography className={classes.subtitle} variant="body1">
                Join today, as freelancer or agency. Currently only available by
                invite.
              </Typography>
            </Box>
            <Box className={classes.illustrationBlock}>
              <AuthScreenIllustration className={classes.illustration} />
            </Box>
          </Grid>
          <Grid sm={5} xs={12} className={classes.formColumn} item>
            <Paper className={classes.form} elevation={3}>
              <Typography
                className={classes.formTitle}
                align="center"
                variant="h6"
              >
                Sign in to your account
              </Typography>
              <Button
                className={classes.formButton}
                color="primary"
                size="large"
                fullWidth
                variant="contained"
                onClick={handleLogin}
              >
                Sign in
              </Button>
            </Paper>
          </Grid>
        </Grid>
      </Container>
    </PageLayout>
  );
};

export default Auth;
