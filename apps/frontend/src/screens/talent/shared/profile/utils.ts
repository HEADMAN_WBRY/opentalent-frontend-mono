import { MIN_JOB_RATE } from 'consts/jobs';
import { SOLUTIONS_WITH_HARD } from 'consts/skills';
import { AvailabilityType } from 'screens/talent/edit-profile/types';

import { SkillTypeEnum } from '@libs/graphql-types';

import {
  AvailabilityFormState,
  CompaniesFormState,
  CreatingFormState,
  CustomWorkHistory,
  CVFormState,
  DescribeFormState,
  PictureFormState,
  ProfileFormState,
  RateFormState,
  SkillsFormState,
} from './types';

export const getDefaultProfile = (
  profileOverrides?: ProfileFormState,
): ProfileFormState => ({
  firstName: '',
  lastName: '',
  location: '',
  category: '',
  subcategories: [],
  position: '',
  linkedLink: '',
  phone: '',
  ...profileOverrides,
});

export const getDefaultPictureState = (
  overrides?: PictureFormState,
): PictureFormState => ({
  avatar: '',
  hash: '',
  ...overrides,
});

export const getDefaultAvailability = (
  overrides?: AvailabilityFormState,
): AvailabilityFormState => ({
  availableDate: '',
  availableNow: AvailabilityType.Now,
  hoursPerWeek: '',
  ...overrides,
});

export const getDefaultRate = (overrides?: RateFormState): RateFormState => ({
  min: MIN_JOB_RATE,
  ...overrides,
});

export const getDefaultSkills = (
  overrides?: SkillsFormState,
): SkillsFormState => ({
  [SkillTypeEnum.HardSkills]: [],
  [SkillTypeEnum.SoftSkills]: [],
  [SkillTypeEnum.Solutions]: [],
  [SOLUTIONS_WITH_HARD]: [],
  ...overrides,
});

export const getDefaultCompanies = (
  overrides?: CompaniesFormState,
): CompaniesFormState => ({
  companies: [],
  ...overrides,
});

export const getDefaultCV = (overrides?: CVFormState): CVFormState => ({
  documents: [],
  ...overrides,
});

export const getDefaultDescribe = (
  overrides?: DescribeFormState,
): DescribeFormState => ({
  about: '',
  ...overrides,
});

export const getDefaultWorkHistory = (): CustomWorkHistory[] => [
  {
    positionTitle: '',
    companyName: '',
    worked: [null, null],
    workNow: false,
    id: Date.now(),
  },
];

export const getTalentInitialState = (): CreatingFormState => ({
  profile: getDefaultProfile(),
  picture: getDefaultPictureState(),
  availability: getDefaultAvailability(),
  rate: getDefaultRate(),
  skills: getDefaultSkills(),
  companies: getDefaultCompanies(),
  cv: getDefaultCV(),
  describe: getDefaultDescribe(),
  workHistory: getDefaultWorkHistory(),
});
