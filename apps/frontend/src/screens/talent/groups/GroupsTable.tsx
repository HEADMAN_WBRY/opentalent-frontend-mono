import {
  Paper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Grid,
} from '@mui/material';
import { makeStyles } from '@mui/styles';
import React from 'react';

import { ReactComponent as Logo } from './logo.svg';

interface GroupsTableProps {}

const createData = (name: string, date: string, fat: number) => ({
  name,
  date,
  fat,
});

const rows = [
  createData('ABN AMRO', '2021-05-20T15:59:08', 6.0),
  createData('Philips', '2021-05-20T15:59:08', 9.0),
];

const useStyles = makeStyles(() => ({
  tableBody: {
    '& $tableCell': {
      borderBottom: 'none',
    },
  },
  tableCell: {},
}));

const GroupsTable = (props: GroupsTableProps) => {
  const classes = useStyles();
  return (
    <TableContainer component={Paper} elevation={0}>
      <Table aria-label="simple table">
        <TableHead>
          <TableRow>
            <TableCell>Client</TableCell>
            <TableCell align="left">Joined on date</TableCell>
            <TableCell align="left">Hours worked</TableCell>
          </TableRow>
        </TableHead>
        <TableBody className={classes.tableBody}>
          {rows.map((row) => (
            <TableRow key={row.name}>
              <TableCell
                className={classes.tableCell}
                component="th"
                scope="row"
              >
                <Grid spacing={2} container>
                  <Grid item>
                    <Logo />
                  </Grid>
                  <Grid item>{row.name}</Grid>
                </Grid>
              </TableCell>
              <TableCell className={classes.tableCell} align="left">
                {row.date}
              </TableCell>
              <TableCell className={classes.tableCell} align="left">
                {row.fat}
              </TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
};

export default GroupsTable;
