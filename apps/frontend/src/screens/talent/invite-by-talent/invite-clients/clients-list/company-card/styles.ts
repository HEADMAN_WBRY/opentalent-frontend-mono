import { makeStyles } from '@mui/styles';

import { Theme } from '@libs/ui/themes/default';

const useStyles = makeStyles((theme: Theme) => ({
  container: {
    background: 'white',
    width: 450,
    padding: theme.spacing(0, 4, 4, 0),
    margin: '0 auto',

    [theme.breakpoints.down('sm')]: {
      width: 'auto',
      margin: `0 ${theme.spacing(4)}`,
    },
  },
  avatar: {
    width: 80,
    height: 80,
  },
}));

export default useStyles;
