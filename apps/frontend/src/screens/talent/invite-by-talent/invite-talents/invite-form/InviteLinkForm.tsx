import errors from 'consts/validationErrors';
import { Form, Formik } from 'formik';
import React from 'react';
import * as yup from 'yup';

import PermIdentityIcon from '@mui/icons-material/PermIdentity';
import { Grid } from '@mui/material';

import { maxStringValidator } from '@libs/helpers/yup';
import Button from '@libs/ui/components/button';
import { ConnectedTextField } from '@libs/ui/components/form/text-field';
import Typography, { RouterLink } from '@libs/ui/components/typography';

import { FORM_TYPES, INITIAL_VALUES } from './consts';
import { useSubmitLinkForm } from './hooks';

interface InviteLinkFormProps {
  disabled: boolean;
  id: string;
  getInvitationRequest: VoidFunction;
}

const validator = yup.object().shape({
  name: maxStringValidator.required(errors.required),
});

// eslint-disable-next-line @typescript-eslint/no-unused-vars
interface FormValues {
  name: string;
}

const InviteLinkForm = ({
  getInvitationRequest,
  disabled,
  id,
}: InviteLinkFormProps) => {
  const { onSubmit, loading } = useSubmitLinkForm({ id, getInvitationRequest });

  return (
    <Formik<FormValues>
      validationSchema={validator}
      initialValues={INITIAL_VALUES}
      onSubmit={onSubmit}
    >
      {() => (
        <Form>
          <Grid spacing={8} container direction="column">
            <Grid item>
              <Typography variant="h6" align="center">
                Who do you want to invite?
              </Typography>
            </Grid>
            <Grid item>
              <Typography variant="body2" align="center">
                share your personal invite link
              </Typography>
            </Grid>
            <Grid item>
              <ConnectedTextField
                label="First Name"
                fullWidth
                name="name"
                variant="outlined"
                InputProps={{
                  startAdornment: <PermIdentityIcon color="disabled" />,
                }}
              />
            </Grid>
            <Grid item>
              <Button
                type="submit"
                variant="contained"
                color="primary"
                fullWidth
                disabled={disabled || !!loading}
              >
                Copy invite link
              </Button>
            </Grid>

            <Grid item>
              <Typography
                style={{
                  display: 'flex',
                  justifyContent: 'center',
                }}
                variant="body2"
              >
                or&nbsp;
                <RouterLink to={{ search: `?form=${FORM_TYPES.email}` }}>
                  send invite by email
                </RouterLink>
              </Typography>
            </Grid>
          </Grid>
        </Form>
      )}
    </Formik>
  );
};

export default InviteLinkForm;
