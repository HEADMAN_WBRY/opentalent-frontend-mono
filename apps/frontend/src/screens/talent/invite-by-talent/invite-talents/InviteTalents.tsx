import React from 'react';

import { Talent } from '@libs/graphql-types';

import { useModalState } from './hooks';
import HowItWorks from './how-it-works';
import InviteForm from './invite-form';
import ProgrammeModal, { SuccessProgrammeModal } from './programme-modal';

interface ProfileProps {
  talentInvitationsInfo?: any;
  getInvitationRequest?: any;
  id: string;
}

const InviteByTalent = ({
  talentInvitationsInfo,
  getInvitationRequest,
  id,
}: ProfileProps) => {
  const { isOpen, handleClose, handleOpen } = useModalState('joinProgramme');
  const {
    isOpen: isSuccessOpen,
    handleClose: handleCloseSuccess,
    handleOpen: handleOpenSuccess,
  } = useModalState('joinSuccess');
  const left = talentInvitationsInfo?.talent_invitations_left ?? 0;
  const invites = (talentInvitationsInfo?.invited_talents ?? []) as Talent[];
  const max = left + invites.length;

  return (
    <>
      <InviteForm
        openModal={handleOpen}
        getInvitationRequest={getInvitationRequest}
        id={id}
        max={max}
        left={left}
      />
      <HowItWorks
        openModal={handleOpen}
        max={max}
        left={left}
        invites={invites}
      />
      <ProgrammeModal
        id={id}
        open={isOpen}
        onSuccess={handleOpenSuccess}
        handleClose={handleClose}
      />
      <SuccessProgrammeModal
        open={isSuccessOpen}
        handleClose={handleCloseSuccess}
      />
    </>
  );
};

export default InviteByTalent;
