import { Box, Grow } from '@mui/material';
import { makeStyles } from '@mui/styles';
import { ReactComponent as MailIcon } from 'assets/icons/mail.svg';
import React from 'react';

import Typography from '@libs/ui/components/typography';

interface InviteAuthProps {}

const useStyles = makeStyles(() => ({
  wrapper: {
    maxWidth: 424,
    position: 'relative',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
  },
}));

const InviteSuccess = (props: InviteAuthProps) => {
  const classes = useStyles();

  return (
    <Box className={classes.wrapper}>
      <Grow in timeout={500}>
        <div>
          <Typography align="center" variant="h4" paragraph>
            Congrats! 🎉
          </Typography>

          <Box pb={4} display="flex" justifyContent="center">
            <MailIcon />
          </Box>

          <Typography align="center" variant="body1" paragraph>
            Please check your email,
            <br /> we’ve sent you an invite link.
          </Typography>
        </div>
      </Grow>
    </Box>
  );
};

export default InviteSuccess;
