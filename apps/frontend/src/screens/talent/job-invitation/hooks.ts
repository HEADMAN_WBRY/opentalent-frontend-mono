import { useMutation, useQuery } from '@apollo/client';
import { useSnackbar } from 'notistack';
import { useHistory } from 'react-router-dom';
import { pathManager } from 'routes';

import {
  Mutation,
  MutationDeclineJobInvitationArgs,
  Query,
  QueryJobInvitationArgs,
} from '@libs/graphql-types';

import { DECLINE_INVITATION, GET_JOB_INVITATION } from './queries';

export const useScreenData = ({ id }: { id: string }) => {
  const { data, loading } = useQuery<Query, QueryJobInvitationArgs>(
    GET_JOB_INVITATION,
    {
      variables: { id },
    },
  );

  return { jobInvitation: data?.jobInvitation, isLoading: loading };
};

export const useHandleDecline = ({ id }: { id: string }) => {
  const history = useHistory();
  const { enqueueSnackbar } = useSnackbar();
  const [declineInvitation, { loading }] = useMutation<
    Mutation,
    MutationDeclineJobInvitationArgs
  >(DECLINE_INVITATION, {
    variables: { job_invitation_id: id },
    onCompleted: () => {
      history.push(pathManager.talent.jobBoard.generatePath());
      enqueueSnackbar('Invitation successfully declined!', {
        variant: 'success',
      });
    },
    onError: () => {
      enqueueSnackbar('Error when declining an invitation!', {
        variant: 'error',
      });
    },
  });

  return { declineInvitation, declineInProgress: loading };
};
