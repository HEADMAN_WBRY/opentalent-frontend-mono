import { gql } from '@apollo/client';

import FULL_JOB_FRAGMENT from 'graphql/fragments/talent/talentJobFragment';

export const GET_JOB_INVITATION = gql`
  ${FULL_JOB_FRAGMENT}
  query GetJobInvitation($id: ID!) {
    jobInvitation(id: $id) {
      job {
        ...FullJob
      }
      message
    }
  }
`;

export const DECLINE_INVITATION = gql`
  mutation DeclineJobInvitation($job_invitation_id: ID!) {
    declineJobInvitation(job_invitation_id: $job_invitation_id)
  }
`;
