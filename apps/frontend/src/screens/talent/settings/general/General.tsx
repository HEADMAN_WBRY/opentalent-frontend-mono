import DeleteForeverIcon from '@mui/icons-material/DeleteForever';
import { Box } from '@mui/material';
import React from 'react';

import { EXTERNAL_LINKS } from '@libs/helpers/consts';
import Button from '@libs/ui/components/button';
import Typography from '@libs/ui/components/typography';

import { useUserData } from './hooks';

interface GeneralProps {}

const General = (props: GeneralProps) => {
  const data = useUserData();

  return (
    <>
      <Box pb={4}>
        <Typography fontWeight={500} variant="h6">
          Profile
        </Typography>
      </Box>

      {data.map((i) => (
        <Box mb={4} key={i.text}>
          <Typography variant="subtitle1">{i.text}</Typography>
          <Typography color="textSecondary" variant="body2">
            {i.value}
          </Typography>
        </Box>
      ))}
      <Box>
        <Button
          component="a"
          style={{ background: 'transparent' }}
          variant="text"
          color="error"
          endIcon={<DeleteForeverIcon />}
          href={EXTERNAL_LINKS.deleteAccount}
        >
          delete profile
        </Button>
      </Box>
    </>
  );
};

export default General;
