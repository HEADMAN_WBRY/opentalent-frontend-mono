import {
  CreateSkillModal,
  SuggestSkillAction,
} from 'components/custom/skill-creation';
import { INPUT_SUGGESTION_LENGTH, SOLUTIONS_WITH_HARD } from 'consts/skills';
import React from 'react';

import Grid from '@mui/material/Grid';

import { SkillTypeEnum } from '@libs/graphql-types';
import { modelPath } from '@libs/helpers/form';
import { ConnectedMultipleSelect } from '@libs/ui/components/form/multiple-select';
import StepSection from '@libs/ui/components/step-section';

import { ProfileFormState } from '../types';
import { useAddSkill } from './hooks';
import { renderTags } from './utils';

const SkillsSection = () => {
  const {
    onInputBlur,
    onInputFocus,
    onInputChange,
    inputValue = '',
    onSelectSkill,
    skillsSuggest,
    skillsLoading,
    openCreateSkillDialog,
    closeCreateSkillDialog,
    initialSkillDialogValues,
    dialogOpened,
    onSubmitSkillDialog,
  } = useAddSkill();

  const getNoOption = (defaultText: string) =>
    !skillsLoading &&
    inputValue?.length > INPUT_SUGGESTION_LENGTH &&
    skillsSuggest.length === 0 ? (
      <SuggestSkillAction onCreate={openCreateSkillDialog} />
    ) : (
      defaultText
    );

  return (
    <StepSection index={4} title="My Skills Passport">
      <Grid spacing={4} direction="column" container>
        <Grid item>
          <ConnectedMultipleSelect
            options={skillsSuggest}
            name={modelPath<ProfileFormState>(
              (m) => m.skills.SOLUTIONS_WITH_HARD,
            )}
            noOptionsText={getNoOption(
              'e.g. React, .NET, Photoshop, IT Security, UX Design',
            )}
            chipProps={{
              color: 'tertiary',
              size: 'small',
            }}
            autoCompleteProps={{
              onChange: onSelectSkill,
              filterSelectedOptions: true,
              popupIcon: null,
            }}
            inputProps={{
              variant: 'filled',
              label: 'Technologies and Hard Skills',
              onFocus: onInputFocus(SOLUTIONS_WITH_HARD),
              onBlur: onInputBlur,
              onChange: onInputChange,
            }}
            renderTags={renderTags}
          />
        </Grid>
        <Grid item>
          <ConnectedMultipleSelect
            options={skillsSuggest}
            name={modelPath<ProfileFormState>((m) => m.skills.SOFT_SKILLS)}
            noOptionsText={getNoOption(
              'e.g. Communication, Leadership, Problem-solving',
            )}
            chipProps={{
              color: 'lightBlue',
              size: 'small',
            }}
            autoCompleteProps={{
              onChange: onSelectSkill,
              filterSelectedOptions: true,
              popupIcon: null,
            }}
            inputProps={{
              variant: 'filled',
              label: 'Soft Skills',
              onFocus: onInputFocus(SkillTypeEnum.SoftSkills),
              onBlur: onInputBlur,
              onChange: onInputChange,
            }}
          />
        </Grid>
      </Grid>
      <CreateSkillModal
        close={closeCreateSkillDialog}
        initialValues={initialSkillDialogValues}
        isOpen={dialogOpened}
        onSubmit={onSubmitSkillDialog}
      />
    </StepSection>
  );
};

export default React.memo(SkillsSection);
