import { makeStyles } from '@mui/styles';

import { Theme } from '@libs/ui/themes/default';

const useStyles = makeStyles((theme: Theme) => ({
  container: {
    [theme.breakpoints.down('sm')]: {
      paddingTop: theme.spacing(4),
    },
  },
  form: {
    paddingTop: 40,
    maxWidth: 700,

    [theme.breakpoints.down('sm')]: {
      paddingTop: theme.spacing(6),
    },
  },
}));

export default useStyles;
