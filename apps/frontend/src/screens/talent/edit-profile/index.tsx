import React from 'react';

const EditProfile = React.lazy(() => import('./EditProfile'));

export default EditProfile;
