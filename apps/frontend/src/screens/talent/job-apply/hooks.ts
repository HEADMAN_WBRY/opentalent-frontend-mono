import { useQuery } from '@apollo/client';

import { Query, QueryJobArgs } from '@libs/graphql-types';

import { GET_JOB_INFO } from './queries';

export const useScreenData = ({ id }: { id: string }) => {
  const { data, loading } = useQuery<Query, QueryJobArgs>(GET_JOB_INFO, {
    variables: { id },
  });

  return {
    job: data?.job,
    application: data?.currentTalentJobApplicationByJobId,
    isLoading: loading,
  };
};
