import { Box } from '@mui/material';
import { ConnectedPageLayout } from 'components/layout/page-layout';
import { useCurrentUser } from 'hooks/auth/useCurrentUser';
import useMediaQueries from 'hooks/common/useMediaQueries';
import useMixPanel from 'hooks/common/useMixPanel';
import React, { useEffect } from 'react';

import { Job } from '@libs/graphql-types';
import Typography from '@libs/ui/components/typography';

import ApplyForm from './apply-form';
import { useScreenData } from './hooks';
import JobInfo from './job-info';
import { ScreenProps } from './types';

const useIsTalentInvitedToJob = (job?: Job) => {
  const { data } = useCurrentUser();
  const currentTalentId = data?.currentTalent?.id;

  return job?.invitations?.some((i) => i?.talent_id === currentTalentId);
};

const JobApply = ({ match }: ScreenProps) => {
  const { isXS } = useMediaQueries();
  const jobId = match.params.id;
  const { job, isLoading, application } = useScreenData({ id: jobId });
  const mixPanel = useMixPanel();
  const isInvited = useIsTalentInvitedToJob(job);

  useEffect(() => {
    mixPanel.track('Application page has been visited');
  }, [mixPanel]);

  return (
    <ConnectedPageLayout
      documentTitle="Job board"
      drawerProps={{}}
      headerProps={{ accountProps: {} }}
      contentSpacing={isXS ? 0 : undefined}
      isLoading={isLoading}
    >
      {job ? (
        <Box maxWidth="768px" pt={4}>
          <Typography variant="h5">
            {isInvited
              ? `Respond to invitation to a job: ${job?.name}`
              : `Apply for ${job?.name}`}
          </Typography>

          <Box pb={8}>
            <JobInfo job={job as Required<Job>} />
          </Box>
          <ApplyForm application={application} job={job as Job} />
        </Box>
      ) : (
        <Box maxWidth="768px" pt={4}>
          <Typography variant="h5">{`Job ${jobId} not found`}</Typography>
        </Box>
      )}
    </ConnectedPageLayout>
  );
};

export default JobApply;
