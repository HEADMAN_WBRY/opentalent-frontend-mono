import { gql } from '@apollo/client';

export const CREATE_JOB_SUGGESTION = gql`
  mutation CreateJobSuggestion($job_id: ID!, $name: String!, $email: String!) {
    createJobSuggestion(job_id: $job_id, name: $name, email: $email)
  }
`;
