import { DEFAULT_AVATAR, JOB_TYPES_LABELS_MAP } from 'consts/common';
import useMediaQueries from 'hooks/common/useMediaQueries';
import React from 'react';
import JobTimer from 'screens/talent/job-apply/job-info/JobTimer';
import { isUnprocessableJob } from 'utils/job';

import { Avatar, Box, Grid } from '@mui/material';
import { makeStyles } from '@mui/styles';

import { Job } from '@libs/graphql-types';
import Typography from '@libs/ui/components/typography';

import SaveToFavoritesButton from './SaveToFavoritesButton';

interface JobCardTitleProps {
  job: Job;
  currentTime: Date;
  isSaved: boolean;
  onJobSave?: VoidFunction;
}

const useStyles = makeStyles(() => ({
  avatar: {
    '& > img': {
      objectFit: 'contain',
    },
  },
}));

const JobCardTitle = ({
  job,
  currentTime,
  isSaved,
  onJobSave,
}: JobCardTitleProps) => {
  const { isXS } = useMediaQueries();
  const classes = useStyles();
  const companyLogo = job?.campaign_owner?.company?.logo || DEFAULT_AVATAR;
  const isUnprocessable = isUnprocessableJob(job);

  return (
    <Grid
      justifyContent="space-between"
      container
      wrap="nowrap"
      direction={isXS ? 'column' : 'row'}
    >
      <Grid item>
        <Grid
          wrap="nowrap"
          spacing={4}
          style={{ alignItems: 'center' }}
          container
        >
          <Grid item>
            <Avatar
              alt="Company logo"
              src={companyLogo}
              className={classes.avatar}
            />
          </Grid>
          <Grid item>
            <Typography variant="h6">{job?.name}</Typography>
            <Typography color="textSecondary" variant="caption">
              {job?.type ? JOB_TYPES_LABELS_MAP[job.type] : ''}
            </Typography>
          </Grid>
        </Grid>
      </Grid>
      <Grid item>
        <Grid
          wrap="nowrap"
          component={Box}
          pt={isXS ? 3 : 0}
          spacing={6}
          container
        >
          <Grid item>
            <Box>
              {!isUnprocessable && (
                <Typography
                  style={{ whiteSpace: 'nowrap', marginTop: -4 }}
                  color="textSecondary"
                  variant="caption"
                  component="div"
                >
                  Campaign time left
                </Typography>
              )}
              <Box display="flex" alignItems="center">
                <JobTimer job={job} currentTime={currentTime} />
              </Box>
            </Box>
          </Grid>
          <Grid item>
            <SaveToFavoritesButton
              onJobSave={onJobSave}
              isSaved={isSaved}
              jobId={job.id}
            />
          </Grid>
        </Grid>
      </Grid>
    </Grid>
  );
};

export default React.memo(JobCardTitle);
