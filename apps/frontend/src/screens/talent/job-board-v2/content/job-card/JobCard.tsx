import useMixPanel from 'hooks/common/useMixPanel';
import React, { Dispatch, SetStateAction, useState } from 'react';

import KeyboardArrowUpIcon from '@mui/icons-material/KeyboardArrowUp';
import { Box, Paper } from '@mui/material';
import { makeStyles } from '@mui/styles';

import { Job } from '@libs/graphql-types';
import Button from '@libs/ui/components/button';
import Typography from '@libs/ui/components/typography';

import JobCardFooter from './JobCardFooter';
import JobCardTitle from './JobCardTitle';
import JobDescription from './JobDescription';
import JobDetails from './JobDetails';
import JobKeySkills from './JobKeySkills';

interface JobCardProps {
  onInvite: Dispatch<SetStateAction<Required<Job> | undefined>>;
  onJobApply: Dispatch<SetStateAction<Required<Job> | undefined>>;
  onJobSave?: VoidFunction;
  loadJobs?: VoidFunction;
  job: Job;
  currentTime: Date;
}

const useStyles = makeStyles((theme) => ({
  root: {
    marginBottom: theme.spacing(4),
  },
  toggler: {
    color: theme.palette.tertiary.main,
  },
  togglerIcon: {
    transform: ({ isOpen }: { isOpen: boolean }) =>
      `rotate(${isOpen ? 0 : 180}deg)`,
  },
}));

const JobCard = ({
  job,
  currentTime,
  onJobApply,
  onInvite,
  onJobSave,
  loadJobs,
}: JobCardProps) => {
  const mixPanel = useMixPanel();
  const [isOpen, setIsOpen] = useState(false);
  const classes = useStyles({ isOpen });
  const handleShowMore = () => {
    setIsOpen((s) => !s);
    mixPanel.track("User clicked 'Show more' on a job card");
  };

  return (
    <Paper data-test-id="jobCard" className={classes.root} elevation={0}>
      <Box p={6}>
        <JobCardTitle
          isSaved={!!job?.is_saved}
          job={job}
          currentTime={currentTime}
          onJobSave={onJobSave}
        />
        <JobKeySkills job={job} isOpen={isOpen} />
        <JobDetails job={job} isOpen={isOpen} />
        <Box display="flex" justifyContent="flex-end">
          <Button
            onClick={handleShowMore}
            className={classes.toggler}
            variant="text"
            endIcon={
              <KeyboardArrowUpIcon
                className={classes.togglerIcon}
                color="inherit"
              />
            }
          >
            <Typography
              style={{ textTransform: 'none' }}
              variant="body2"
              color="tertiary"
            >
              Show more
            </Typography>
          </Button>
        </Box>
        <JobDescription job={job} isOpen={isOpen} />
        <JobCardFooter
          job={job}
          onJobApply={onJobApply}
          onInvite={onInvite}
          loadJobs={loadJobs}
          isOpen={isOpen}
        />
      </Box>
    </Paper>
  );
};

export default JobCard;
