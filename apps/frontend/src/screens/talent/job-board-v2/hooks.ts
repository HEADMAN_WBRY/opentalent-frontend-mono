import { useLazyQuery } from '@apollo/client';
import { useFormikContext } from 'formik';
import { usePushWithQuery, useSearchParams } from 'hooks/routing';
import { useCallback, useEffect, useMemo, useState } from 'react';

import {
  Job,
  Query,
  QueryCurrentTalentJobBoardSearchArgs,
} from '@libs/graphql-types';

import { GET_JOBS } from './queries';
import { FilterForm } from './types';
import {
  mapFormToQuery,
  mapQueryToInitialValues,
  mapQueryToVariables,
} from './utils';

export const useQueryParamsToLoadVariables =
  (): QueryCurrentTalentJobBoardSearchArgs => {
    const params = useSearchParams();
    const variables = useMemo(() => mapQueryToVariables(params), [params]);

    return variables;
  };

export const useJobList = () => {
  const variables = useQueryParamsToLoadVariables();
  const [loadJobs, { data, loading }] = useLazyQuery<
    Query,
    QueryCurrentTalentJobBoardSearchArgs
  >(GET_JOBS, { variables, fetchPolicy: 'network-only' });

  const loadWithOptions = useCallback(() => {
    loadJobs({ variables });
  }, [loadJobs, variables]);

  useEffect(() => {
    loadWithOptions();
  }, [loadWithOptions, variables]);

  const { currentPage = 1, lastPage = 1 } =
    data?.currentTalentJobBoardSearch?.paginatorInfo || {};

  return {
    jobs: data?.currentTalentJobBoardSearch?.data || [],
    loadingJobList: loading,
    currentPage,
    lastPage,
    loadJobs: loadWithOptions,
  };
};

export const useJobModals = () => {
  const [inviteJob, setInviteJob] = useState<Required<Job>>();
  const [applyJob, setApplyJob] = useState<Required<Job>>();

  const handleClose = useCallback(() => {
    setInviteJob(undefined);
    setApplyJob(undefined);
  }, []);

  return {
    inviteJob,
    applyJob,
    handleClose,
    onJobApply: setApplyJob,
    onInvite: setInviteJob,
  };
};

export const useAutoSaveHandler = () => {
  const push = usePushWithQuery();
  return useCallback(
    (values: FilterForm) => push({ query: mapFormToQuery(values) }),
    [push],
  );
};

export const useInitialValues = () => {
  const params = useSearchParams();
  return mapQueryToInitialValues(params);
};

export const usePaginationChange = () => {
  const { setFieldValue } = useFormikContext();

  const onPaginationChange = useCallback(
    (e: React.ChangeEvent<unknown>, page) => {
      setFieldValue('page', page);
    },
    [setFieldValue],
  );

  return onPaginationChange;
};
