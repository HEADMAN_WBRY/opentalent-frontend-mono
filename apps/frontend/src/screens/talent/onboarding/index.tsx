import React from 'react';

const Onboarding = React.lazy(() => import('./Onboarding'));

export default Onboarding;
