/* eslint-disable react-hooks/exhaustive-deps */
import { ReactComponent as LogoIcon } from 'assets/icons/opentalent_dark.svg';
import PageLayout from 'components/layout/page-layout';
import { useCurrentUser } from 'hooks/auth/useCurrentUser';
import React from 'react';
import {
  Redirect,
  Route,
  RouteComponentProps,
  Switch,
  useLocation,
} from 'react-router-dom';
import { EXTERNAL_RESOURCES, pathManager } from 'routes';

import { makeStyles } from '@mui/styles';

import TalentNotExistYetContext from './TalentContext';
import Creating from './creating';
import Finish from './finish';
import Form from './form';
import Greeting from './greeting';
import { useDeviceId, useTmpVarsFromQuery } from './hooks';
import Intro from './intro';

interface OnboardingProps extends RouteComponentProps {}

const useStyles = makeStyles((theme) => ({
  contentWrapper: {
    overflow: 'hidden',
    margin: '0',
    width: '100%',
    maxWidth: '100%',
    color: theme.palette.secondary.contrastText,
    backgroundColor: theme.palette.secondary.main,
    paddingBottom: theme.spacing(25),

    '& .MuiFilledInput-root': {
      background: 'white',
    },
  },
  '@global': {
    header: {
      backgroundColor: `${theme.palette.other.black} !important`,

      '&::before': {
        background: `${theme.palette.other.black} !important`,
      },
    },
  },
}));

const Onboarding = (props: OnboardingProps) => {
  const classes = useStyles();
  const location = useLocation();
  const { isAuthorized, isCompany } = useCurrentUser();

  const { email } = useTmpVarsFromQuery();
  useDeviceId();

  if (isAuthorized && isCompany) {
    return <Redirect to={pathManager.company.jobBoard.generatePath()} />;
  }

  if (!isAuthorized && !email) {
    window.location.href = EXTERNAL_RESOURCES.talentLending;
    return null;
  }

  return (
    <TalentNotExistYetContext.Provider value={!isAuthorized}>
      <PageLayout
        classes={{ contentWrapper: classes.contentWrapper }}
        documentTitle="Onboarding"
        headerProps={{
          accountProps: null,
          Logo: LogoIcon,
        }}
      >
        <Switch>
          <Route
            path={pathManager.talent.onboarding.greeting.getRoute()}
            render={(params) => (
              <Greeting
                {...params}
                link={pathManager.talent.onboarding.intro.getRoute()}
              />
            )}
          />
          <Route
            path={pathManager.talent.onboarding.intro.getRoute()}
            component={Intro}
          />
          <Route
            path={pathManager.talent.onboarding.creating.getRoute()}
            component={Creating}
          />
          <Route
            path={pathManager.talent.onboarding.form.getRoute()}
            component={Form}
          />
          <Route
            path={pathManager.talent.onboarding.finish.getRoute()}
            component={Finish}
          />
          <Redirect
            to={{
              pathname: pathManager.talent.onboarding.greeting.generatePath(),
              state: location.state,
            }}
          />
        </Switch>
      </PageLayout>
    </TalentNotExistYetContext.Provider>
  );
};

export default Onboarding;
