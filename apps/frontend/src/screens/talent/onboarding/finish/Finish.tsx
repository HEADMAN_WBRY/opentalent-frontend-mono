import { LOCAL_STORAGE_KEYS } from 'consts/ls-keys';
import React, { useContext, useEffect } from 'react';
import { Link, RouteComponentProps } from 'react-router-dom';
import { pathManager } from 'routes/consts';

import { Box, Grow } from '@mui/material';
import { makeStyles } from '@mui/styles';

import Button from '@libs/ui/components/button';
import Typography from '@libs/ui/components/typography';

import TalentNotExistYetContext from '../TalentContext';

const useStyles = makeStyles((theme) => ({
  wrapper: {
    maxWidth: 870,
    position: 'relative',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    textAlign: 'center',
  },
  title: {
    marginBottom: theme.spacing(4),

    [theme.breakpoints.down('sm')]: {
      lineHeight: '36px',
    },
  },
  titleText: {
    fontStyle: 'italic',
    fontWeight: 500,

    [theme.breakpoints.down('sm')]: {
      fontSize: 32,
    },
  },
  subTitle: {
    lineHeight: '24px',
    marginBottom: theme.spacing(12),
  },
  bottomSubTitle: {
    fontSize: '14px',
    lineHeight: '20px',
    color: theme.palette.grey[500],
    marginTop: theme.spacing(4),
    marginBottom: theme.spacing(15),
    whiteSpace: 'pre-line',
  },
  button: {
    maxWidth: 252,
    width: '100%',
    fontSize: '15px',
  },
}));

interface IProps extends RouteComponentProps {}

const Greeting: React.FC<IProps> = () => {
  const classes = useStyles();
  const talentCreated = !useContext(TalentNotExistYetContext);

  useEffect(() => {
    localStorage.removeItem(LOCAL_STORAGE_KEYS.talentOnboardingEmail);
    localStorage.removeItem(LOCAL_STORAGE_KEYS.talentOnboardingCompanyId);
    localStorage.removeItem(LOCAL_STORAGE_KEYS.talentOnboardingAppliedJobId);
  }, []);

  return (
    <Box className={classes.wrapper}>
      <Grow timeout={500} in>
        <div>
          <Typography className={classes.title} variant="h3">
            {talentCreated && <span role="img">🥳</span>}
            <span className={classes.titleText}>
              {talentCreated ? 'Congratulations!' : 'Thank you!'}
            </span>
          </Typography>
          <Typography variant="subtitle1" className={classes.subTitle}>
            {talentCreated
              ? 'Your profile on OpenTalent is now live.'
              : 'We received your application.'}
          </Typography>

          {/* <Form {...props} /> */}

          <Typography variant="h5">So what’s next?</Typography>

          <Typography variant="subtitle2" className={classes.bottomSubTitle}>
            {talentCreated
              ? `You can now visit OpenTalent to browse open jobs,
              further enhance your profile, or start inviting people.`
              : 'Someone will review your profile and get back to you asap.'}
          </Typography>

          {talentCreated && (
            <Box>
              <Link to={pathManager.talent.jobBoard.generatePath()}>
                <Button
                  color="primary"
                  variant="contained"
                  className={classes.button}
                >
                  Go to opentalent
                </Button>
              </Link>
            </Box>
          )}
        </div>
      </Grow>
    </Box>
  );
};

export default Greeting;
