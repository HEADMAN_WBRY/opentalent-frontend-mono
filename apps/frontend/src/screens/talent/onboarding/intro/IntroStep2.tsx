import { Box } from '@mui/material';
import { makeStyles } from '@mui/styles';
import Lottie from 'lottie-react';
import React from 'react';

import Typography from '@libs/ui/components/typography';

import * as animationData from './assets/notifications.json';

const useStyles = makeStyles((theme) => ({
  title: {
    fontSize: '28px',
    lineHeight: '44px',
    fontStyle: 'italic',
  },
  image: {
    width: 405,
    margin: '0 auto',

    [theme.breakpoints.down('md')]: {
      width: '100%',
    },
  },
}));

const IntroStep3 = () => {
  const classes = useStyles();

  return (
    <Box mt={4}>
      <Box pb={6}>
        <Typography variant="h4" className={classes.title} fontWeight={600}>
          Top jobs in your inbox.
        </Typography>
      </Box>
      <Box pt={2} className={classes.image}>
        <Lottie animationData={(animationData as any).default} loop />
      </Box>
    </Box>
  );
};

export default IntroStep3;
