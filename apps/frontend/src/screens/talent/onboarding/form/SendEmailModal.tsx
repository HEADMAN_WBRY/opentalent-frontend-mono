import { useMutation } from '@apollo/client';
import MailOutlineIcon from '@mui/icons-material/MailOutline';
import { Box } from '@mui/material';
import { Formik } from 'formik';
import { useSnackbar } from 'notistack';
import React, { useCallback, useMemo } from 'react';
import * as yup from 'yup';

import {
  Mutation,
  MutationSendInvitationLinkByEmailArgs,
} from '@libs/graphql-types';
import Button from '@libs/ui/components/button';
import { ConnectedTextField } from '@libs/ui/components/form/text-field';
import DefaultModal, {
  DefaultModalProps,
} from '@libs/ui/components/modals/DefaultModal';

import { SEND_INVITATION_LINK } from './queries';

interface SendEmailModalProps extends DefaultModalProps {
  link: string;
  name: string;
}

const validator = yup.object().shape({
  email: yup.string().email().required(),
});

const SendEmailModal = ({ link, name, ...props }: SendEmailModalProps) => {
  const { enqueueSnackbar } = useSnackbar();
  const defaultVariables = useMemo(
    () => ({ email: '', invitation_link_id: link }),
    [link],
  );
  const [send, { loading }] = useMutation<
    Mutation,
    MutationSendInvitationLinkByEmailArgs
  >(SEND_INVITATION_LINK, {
    variables: defaultVariables,
    onCompleted: () => {
      enqueueSnackbar(`Your invite has been sent to ${name}`, {
        variant: 'success',
      });
      props.handleClose();
    },
    // eslint-disable-next-line no-console
    onError: console.error,
  });

  const onSendAction = useCallback(
    (values: { email: string }) => {
      send({ variables: { ...defaultVariables, ...values } });
    },
    [defaultVariables, send],
  );

  return (
    <Formik
      validationSchema={validator}
      initialValues={{ email: '' }}
      onSubmit={onSendAction}
    >
      {({ handleSubmit, resetForm }) => (
        <DefaultModal
          {...props}
          handleClose={() => {
            props?.handleClose();
            resetForm();
          }}
          actions={
            <Button
              size="large"
              color="primary"
              variant="contained"
              fullWidth
              disabled={loading}
              onClick={handleSubmit}
              endIcon={<MailOutlineIcon />}
            >
              send the invite
            </Button>
          }
        >
          <Box width="300px">
            <ConnectedTextField
              size="medium"
              variant="filled"
              fullWidth
              name="email"
              disabled={loading}
              label="Enter email address"
            />
          </Box>
        </DefaultModal>
      )}
    </Formik>
  );
};

export default SendEmailModal;
