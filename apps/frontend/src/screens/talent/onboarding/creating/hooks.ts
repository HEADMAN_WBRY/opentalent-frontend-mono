import { useMutation, useQuery } from '@apollo/client';
import { mergeDeep } from '@apollo/client/utilities';
import { FormikConfig } from 'formik';
import { GetTalentLayoutData } from 'graphql/common/queries';
import { GET_CURRENT_TALENT, UPDATE_TALENT_PROFILE } from 'graphql/talents';
import { GET_TMP_DATA } from 'graphql/tmp-data';
import { CREATE_UNTRUSTED_TALENT } from 'graphql/user';
import { useCurrentUser, useAuth0 } from 'hooks/auth';
import { useSnackbar } from 'notistack';
import { useCallback, useMemo } from 'react';
import { useHistory } from 'react-router-dom';
import { pathManager } from 'routes';
import { useInitialTalentState } from 'screens/talent/shared/profile/hooks';
import {
  mapFormToTalentCreation,
  mapFormToTalentUpdate,
  mapTalentToClient,
} from 'screens/talent/shared/profile/mappers';
import { noop } from 'utils/common';

import {
  Mutation,
  MutationCreateUntrustedTalentProfileArgs,
  MutationUpdateTalentProfileArgs,
  Query,
  QueryGetTempDataItemArgs,
} from '@libs/graphql-types';

import { CreatingFormState } from '../../shared/profile/types';
import { getTmpDataArgs } from '../utils';

const useLoadTalent = (): null | CreatingFormState => {
  const { loading, data } = useQuery<Query>(GET_CURRENT_TALENT);
  const noData = loading || !data;

  const talent = data?.currentTalent;

  return useMemo(
    () =>
      !noData
        ? {
            talentData: {
              id: talent?.id || '',
            },
            ...mapTalentToClient(talent),
          }
        : null,
    [noData, talent],
  );
};

const useUnauthorizedTalentState = () => {
  const initialValues = useInitialTalentState();
  const { data, called, loading } = useQuery<Query, QueryGetTempDataItemArgs>(
    GET_TMP_DATA,
    {
      variables: getTmpDataArgs(),
      onError: noop,
      errorPolicy: 'ignore',
    },
  );
  const shouldBeData = called && !loading;

  const dataFromServer = data?.getTempDataItem?.result?.data || '';
  const finalInitialValues = useMemo(() => {
    const parsed = dataFromServer ? JSON.parse(dataFromServer) : {};
    return mergeDeep(initialValues, parsed as CreatingFormState);
  }, [dataFromServer, initialValues]);

  return shouldBeData ? finalInitialValues : null;
};

export const useInitialState = (): CreatingFormState | null => {
  const { isAuthorized } = useCurrentUser();
  const useDataHook = isAuthorized ? useLoadTalent : useUnauthorizedTalentState;
  const initialValues = useDataHook();

  return initialValues;
};

const useTalentCreation = () => {
  const { onSubmit: updateTalent, loading: isUpdating } = useTalentUpdate();
  const [submitFrom, { loading }] = useMutation<
    Mutation,
    MutationCreateUntrustedTalentProfileArgs
  >(CREATE_UNTRUSTED_TALENT);
  const onSubmit: FormikConfig<CreatingFormState>['onSubmit'] = useCallback(
    async (formData) => {
      const mappedData = mapFormToTalentCreation(formData);
      const res = await submitFrom({ variables: mappedData });
      const talentId = res?.data?.createUntrustedTalentProfile || '';

      await updateTalent({ ...formData, talentData: { id: talentId } });
    },
    [submitFrom, updateTalent],
  );

  return { loading: loading || isUpdating, onSubmit };
};

const useTalentUpdate = () => {
  const history = useHistory();
  const { enqueueSnackbar } = useSnackbar();
  const { user } = useAuth0();
  const [submitFrom, { loading }] = useMutation<
    Mutation,
    MutationUpdateTalentProfileArgs
  >(UPDATE_TALENT_PROFILE, {
    onCompleted: () => {
      enqueueSnackbar(`Success!`, { variant: 'success' });
      history.push(pathManager.talent.onboarding.finish.generatePath());
    },
    refetchQueries: user ? [{ query: GetTalentLayoutData }] : [],
  });
  const onSubmit = useCallback(
    async (formData: CreatingFormState) => {
      try {
        await submitFrom({ variables: mapFormToTalentUpdate(formData) });
      } catch (e) {
        enqueueSnackbar((e as any).toString(), { variant: 'error' });
      }
    },
    [submitFrom, enqueueSnackbar],
  );

  return { onSubmit, loading };
};

export const useSubmitAction = () => {
  const { isAuthorized } = useCurrentUser();
  const currentSubmitHook = isAuthorized ? useTalentUpdate : useTalentCreation;
  const { loading, onSubmit } = currentSubmitHook();

  return { loading, onSubmit };
};
