import {
  CreateSkillModal,
  SuggestSkillAction,
} from 'components/custom/skill-creation';
import { INPUT_SUGGESTION_LENGTH, SOLUTIONS_WITH_HARD } from 'consts/skills';
import React from 'react';
import { useAddSkill } from 'screens/talent/edit-profile/skills-section/hooks';
import { renderTags } from 'screens/talent/edit-profile/skills-section/utils';

import { Box } from '@mui/material';
import Grid from '@mui/material/Grid';
import { makeStyles } from '@mui/styles';

import { SkillTypeEnum } from '@libs/graphql-types';
import { modelPath } from '@libs/helpers/form';
import { ConnectedMultipleSelect } from '@libs/ui/components/form/multiple-select';
import Typography, { OuterLink } from '@libs/ui/components/typography';
import InfoLinkContainer from '@libs/ui/components/typography/InfoLinkContainer';

import { CreatingFormState } from '../../../shared/profile/types';

const useStyles = makeStyles((theme) => ({
  wrapper: {
    maxWidth: 496,
    margin: '0 auto',
  },
  subtitle: {
    marginBottom: theme.spacing(6),
    lineHeight: '20px',
  },
  info: {
    paddingTop: theme.spacing(4),
    fontStyle: 'italic',
    justifyContent: 'center',

    '& svg': {
      color: theme.palette.tertiary.main,
    },
  },

  iconContainer: {
    display: 'flex',
    alignItems: 'center',
  },
}));

const Skills: React.FC = () => {
  const {
    onInputBlur,
    onInputFocus,
    onInputChange,
    inputValue = '',
    onSelectSkill,
    skillsSuggest,
    closeCreateSkillDialog,
    initialSkillDialogValues,
    openCreateSkillDialog,
    skillsLoading,
    dialogOpened,
    onSubmitSkillDialog,
  } = useAddSkill();
  const classes = useStyles();
  const getNoOption = (defaultText: string) =>
    !skillsLoading &&
    inputValue?.length > INPUT_SUGGESTION_LENGTH &&
    skillsSuggest.length === 0 ? (
      <SuggestSkillAction onCreate={openCreateSkillDialog} />
    ) : (
      defaultText
    );

  return (
    <Box className={classes.wrapper}>
      <Typography variant="subtitle2" className={classes.subtitle}>
        Select the skills that best represent you. We’ll use this information to
        best match you with new job opportunities.
      </Typography>
      <Grid spacing={4} direction="column" container>
        <Grid item>
          <ConnectedMultipleSelect
            options={skillsSuggest}
            name={modelPath<CreatingFormState>(
              (m) => m.skills.SOLUTIONS_WITH_HARD,
            )}
            noOptionsText={getNoOption(
              'e.g. React, .NET, Photoshop, IT Security, UX Design',
            )}
            chipProps={{
              color: 'tertiary',
              size: 'small',
            }}
            autoCompleteProps={{
              onChange: onSelectSkill,
              filterSelectedOptions: true,
              popupIcon: null,
            }}
            inputProps={{
              variant: 'filled',
              label: 'Technologies & Hard Skills',
              onFocus: onInputFocus(SOLUTIONS_WITH_HARD),
              onBlur: onInputBlur,
              onChange: onInputChange,
            }}
            renderTags={renderTags}
          />
        </Grid>
        <Grid item>
          <ConnectedMultipleSelect
            options={skillsSuggest}
            name={modelPath<CreatingFormState>((m) => m.skills.SOFT_SKILLS)}
            data-test-id={`skills.${SkillTypeEnum.SoftSkills}`}
            noOptionsText={getNoOption(
              'e.g. Communication, Leadership, Problem-solving',
            )}
            chipProps={{
              color: 'lightBlue',
              size: 'small',
            }}
            autoCompleteProps={{
              onChange: onSelectSkill,
              filterSelectedOptions: true,
              popupIcon: null,
            }}
            inputProps={{
              variant: 'filled',
              label: 'Soft Skills',
              onFocus: onInputFocus(SkillTypeEnum.SoftSkills),
              onBlur: onInputBlur,
              onChange: onInputChange,
            }}
          />
        </Grid>
      </Grid>

      <CreateSkillModal
        close={closeCreateSkillDialog}
        initialValues={initialSkillDialogValues}
        isOpen={dialogOpened}
        onSubmit={onSubmitSkillDialog}
      />

      <Box pt={4}>
        <InfoLinkContainer centered>
          <OuterLink
            target="_blank"
            rel="noreferrer"
            href="https://www.notion.so/Understanding-different-skill-types-7d42866a9ca64a51b94d3a8a5d05e5aa"
            variant="caption"
          >
            What is meant by Technologies, Hard Skills and Soft Skills?
          </OuterLink>
        </InfoLinkContainer>
      </Box>
    </Box>
  );
};

export default Skills;
