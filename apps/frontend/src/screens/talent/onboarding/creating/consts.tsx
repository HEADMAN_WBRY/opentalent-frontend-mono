import { pathManager } from 'routes';

import { modelPath } from '@libs/helpers/form';

import { CreatingFormState, StepDataType } from '../../shared/profile/types';
import { Profile } from './steps';
import Availability from './steps/Availability';
import CV from './steps/CV';
import Companies from './steps/Companies';
import Describe from './steps/Describe';
import Photo from './steps/Photo';
import Rate from './steps/Rate';
import Skills from './steps/Skills';

export const STEPS_DATA: StepDataType[] = [
  {
    title: `Let's get started!`,
    subTitle: 'Setting up your profile will take roughly 2 minutes',
    step: 1,
    Content: Profile,
    nextLink: pathManager.talent.onboarding.creating.generatePath({ step: 2 }),
    statePath: modelPath<CreatingFormState>((m) => m.profile),
  },
  {
    title: 'Profile picture',
    subTitle: 'Remember...a picture speaks a thousand words!',
    step: 2,
    Content: Photo,
    backLink: pathManager.talent.onboarding.creating.generatePath({ step: 1 }),
    nextLink: pathManager.talent.onboarding.creating.generatePath({ step: 3 }),
    statePath: modelPath<CreatingFormState>((m) => m.picture),
  },
  {
    title: 'Availability',
    subTitle: 'Tell clients when you’re open for new roles.',
    step: 3,
    Content: Availability,
    backLink: pathManager.talent.onboarding.creating.generatePath({ step: 2 }),
    nextLink: pathManager.talent.onboarding.creating.generatePath({ step: 4 }),
    statePath: modelPath<CreatingFormState>((m) => m.availability),
  },
  {
    title: 'Minimum hourly rate (€)',
    subTitle: 'What’s the minimum you expect to make per hour?',
    step: 4,
    Content: Rate,
    backLink: pathManager.talent.onboarding.creating.generatePath({ step: 3 }),
    nextLink: pathManager.talent.onboarding.creating.generatePath({ step: 5 }),
    statePath: modelPath<CreatingFormState>((m) => m.rate),
  },
  {
    title: 'Skills passport',
    subTitle: 'Tell clients which skills pay your bills?',
    step: 5,
    Content: Skills,
    backLink: pathManager.talent.onboarding.creating.generatePath({ step: 4 }),
    nextLink: pathManager.talent.onboarding.creating.generatePath({ step: 6 }),
    statePath: modelPath<CreatingFormState>((m) => m.skills),
  },
  {
    title: 'Companies you worked for',
    subTitle: 'What top companies have you worked for, or served as clients?',
    step: 6,
    Content: Companies,
    backLink: pathManager.talent.onboarding.creating.generatePath({ step: 5 }),
    nextLink: pathManager.talent.onboarding.creating.generatePath({ step: 7 }),
    statePath: modelPath<CreatingFormState>((m) => m.companies),
  },
  {
    title: 'Upload your CV in English',
    subTitle:
      'So we can better match you with new roles based on information we might have missed.',
    Content: CV,
    step: 7,
    backLink: pathManager.talent.onboarding.creating.generatePath({ step: 6 }),
    nextLink: pathManager.talent.onboarding.creating.generatePath({ step: 8 }),
    statePath: modelPath<CreatingFormState>((m) => m.cv),
  },
  {
    title: 'Describe your ideal role',
    subTitle:
      'What you are looking for now? Any companies you want to work for? Anything clients need to know?',
    Content: Describe,
    step: 8,
    backLink: pathManager.talent.onboarding.creating.generatePath({ step: 7 }),
    nextLink: pathManager.talent.onboarding.finish.generatePath(),
    statePath: modelPath<CreatingFormState>((m) => m.describe),
  },
];
