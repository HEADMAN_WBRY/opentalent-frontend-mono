import { Box, Grid } from '@mui/material';
import useMediaQueries from 'hooks/common/useMediaQueries';
import React from 'react';

import { Talent } from '@libs/graphql-types';

import ActionBlock from './action-block';
import InfoBlock from './info-block';
import useStyles from './styles';
import UserBlock from './user-block';

interface ProfileHeaderProps {
  isTalentFlow: boolean;
  talent?: Partial<Talent>;
  refetchTalent: VoidFunction;
}

const ProfileHeader = ({
  isTalentFlow,
  talent = {},
  refetchTalent,
}: ProfileHeaderProps) => {
  const { isSM, isXS } = useMediaQueries();
  const classes = useStyles();

  return (
    <Box pb={4}>
      <Grid
        direction={isSM ? 'column' : 'row'}
        justifyContent="space-between"
        container
        spacing={4}
        wrap="nowrap"
      >
        <Grid item component={Box}>
          <UserBlock isXS={isXS} talent={talent} />
        </Grid>
        <Grid item component={Box}>
          <InfoBlock
            isXS={isXS}
            isSM={isSM}
            isTalentFlow={isTalentFlow}
            talent={talent}
          />
        </Grid>
        <Grid className={classes.actionContainer} sm="auto" item>
          <ActionBlock
            refetchTalent={refetchTalent}
            talent={talent}
            isTalentFlow={isTalentFlow}
          />
        </Grid>
      </Grid>
    </Box>
  );
};

export default ProfileHeader;
