import { makeStyles } from '@mui/styles';

import { Theme } from '@libs/ui/themes/default';

const useStyles = makeStyles((theme: Theme) => ({
  container: {
    [theme.breakpoints.down('md')]: {
      position: 'absolute',
      top: theme.spacing(4),
      right: theme.spacing(6),
    },

    [theme.breakpoints.down('sm')]: {
      top: theme.spacing(6),
    },
  },
}));

export default useStyles;
