import { useQuery } from '@apollo/client';
import { debounce } from '@mui/material';
import { GET_CHAT_MESSAGES } from 'graphql/chat';
import { useCallback, useMemo, useState } from 'react';

import { Query, QueryChatMessagesArgs } from '@libs/graphql-types';

export const useLoadComments = ({
  talentId,
  scrollToBottom,
}: {
  talentId: string;
  scrollToBottom: any;
}) => {
  const [isLoading, setIsLoading] = useState<boolean>();
  const { data, fetchMore, loading, variables } = useQuery<
    Query,
    QueryChatMessagesArgs
  >(GET_CHAT_MESSAGES, {
    variables: { talent_id: talentId, page: 1 },
    onCompleted: scrollToBottom,
    fetchPolicy: 'no-cache',
  });
  const paginatorInfo = data?.chatMessages?.paginatorInfo;
  const currentPage = paginatorInfo?.currentPage ?? 0;
  const hasMorePage = paginatorInfo?.hasMorePages ?? false;
  const comments = data?.chatMessages?.data ?? [];

  const loadMoreComments = useCallback(async () => {
    if (!hasMorePage) {
      return;
    }
    setIsLoading(true);
    await fetchMore({
      variables: {
        ...variables,
        page: currentPage + 1,
      },
    });
    setIsLoading(false);
  }, [hasMorePage, fetchMore, variables, currentPage]);

  return {
    isLoading: loading || isLoading,
    loadMoreComments: hasMorePage ? loadMoreComments : undefined,
    comments,
    hasMorePage,
  };
};

export const useOnScrollListener = ({
  ref,
  loadMoreComments,
}: {
  ref: any;
  loadMoreComments?: any;
}) => {
  const onScroll = useCallback(async () => {
    const scrollTop = ref?.current?.scrollTop;
    const scrollHeight = ref?.current?.scrollHeight;
    const needLoadComments = scrollTop < 100;

    if (needLoadComments && loadMoreComments) {
      await loadMoreComments();
      const newTop = ref?.current?.scrollHeight - scrollHeight;
      // eslint-disable-next-line no-unused-expressions
      ref?.current?.scroll({ top: newTop });
    }
  }, [loadMoreComments, ref]);
  const debouncedOnScroll = useMemo(() => debounce(onScroll), [onScroll]);

  return debouncedOnScroll;
};
