import { useMutation, useQuery } from '@apollo/client';
import { format } from 'date-fns';
import { FormikHelpers } from 'formik';
import { CREATE_CHAT_MESSAGES } from 'graphql/chat';
import { GET_CURRENT_COMPANY_USER } from 'graphql/user';
import { useCallback, useState } from 'react';

import {
  ChatMessage,
  Mutation,
  MutationCreateChatMessageArgs,
  Query,
} from '@libs/graphql-types';

import { MessagesFormType } from './types';

interface Args {
  talentId: string;
  scrollToListBottom: (args: Record<string, string>) => void;
}

export const useAddChatMessage = ({ talentId, scrollToListBottom }: Args) => {
  const [newComments, setNewComments] = useState<ChatMessage[]>([]);
  const { data: companyUserData } = useQuery<Query, never>(
    GET_CURRENT_COMPANY_USER,
  );
  const [createMessageMutation, { loading }] = useMutation<
    Mutation,
    MutationCreateChatMessageArgs
  >(CREATE_CHAT_MESSAGES);
  const createMessage = useCallback(
    async (
      { message }: MessagesFormType,
      {
        resetForm,
        setFieldTouched,
        setSubmitting,
      }: FormikHelpers<MessagesFormType>,
    ) => {
      await createMessageMutation({
        variables: { talent_id: talentId, message },
      });
      resetForm();
      setFieldTouched('message', false);
      setSubmitting(false);
      const newMessage: ChatMessage = {
        id: Date.now().toString(),
        parent_id: '0',
        user: companyUserData?.currentCompanyUser,
        talent_id: talentId,
        message,
        created_at: format(new Date(), 'yyyy-MM-dd HH:mm:ss'),
      };
      setNewComments([...newComments, newMessage]);
      scrollToListBottom({ behavior: 'smooth' });
    },
    [
      companyUserData,
      createMessageMutation,
      newComments,
      scrollToListBottom,
      talentId,
    ],
  );

  return { createMessage, loading, newComments };
};

export const useScrollToListBottom = (ref: any) => {
  return useCallback(
    (opts: { behavior?: string } = {}) => {
      const scrollHeight = ref?.current?.scrollHeight;
      // eslint-disable-next-line no-unused-expressions
      ref?.current?.scroll({ top: scrollHeight, ...opts });
    },
    [ref],
  );
};
