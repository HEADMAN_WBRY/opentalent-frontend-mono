import { useSnackbar } from 'notistack';
import { useCallback } from 'react';

import { useSetTagsToTalentMutation } from '@libs/graphql-types';
import { FormikSubmit } from '@libs/helpers/form';

import { IFormState } from './types';

export const useSubmitHandler = ({ talentId }: { talentId?: string }) => {
  const { enqueueSnackbar } = useSnackbar();
  const [request, { loading }] = useSetTagsToTalentMutation({
    onCompleted: () => {
      enqueueSnackbar('Tags changed successfully', { variant: 'success' });
    },
  });
  const onSubmit: FormikSubmit<IFormState> = useCallback(
    async ({ tags }) => {
      if (!talentId) {
        enqueueSnackbar('No talent id provided', { variant: 'error' });
        return;
      }
      const tagIds = tags.map((i) => String(i.value));
      await request({
        variables: { talent_id: talentId, tags_ids: tagIds },
      });
    },
    [enqueueSnackbar, request, talentId],
  );

  return { loading, onSubmit };
};
