import React, { useMemo } from 'react';

import { Box, Grid } from '@mui/material';

import { Skill, SkillTypeEnum } from '@libs/graphql-types';
import Typography from '@libs/ui/components/typography';

import SkillsSection from './skills-section/SkillsSection';
import useStyles from './styles';

export interface SkillsProps {
  skills?: Skill[];
}

const Skills = (props: SkillsProps) => {
  const classes = useStyles(props);
  const { skills } = props;

  const { solutionsSkills, hardSkills, softSkills } = useMemo(
    () => ({
      solutionsSkills: skills?.filter(
        (item) => item.skill_type === SkillTypeEnum.Solutions,
      ),
      hardSkills: skills?.filter(
        (item) => item.skill_type === SkillTypeEnum.HardSkills,
      ),
      softSkills: skills?.filter(
        (item) => item.skill_type === SkillTypeEnum.SoftSkills,
      ),
    }),
    [skills],
  );

  return (
    <>
      <Box mb={4}>
        <Typography variant="h6">Skills & Category</Typography>
      </Box>
      <Grid container direction="column">
        <Grid
          classes={{
            root: classes.skillsSectionWrapper,
          }}
          item
        >
          <SkillsSection title="Technologies" skillsData={solutionsSkills} />
        </Grid>
        <Grid
          classes={{
            root: classes.skillsSectionWrapper,
          }}
          item
        >
          <SkillsSection
            chipColor="green"
            title="Hard Skills"
            skillsData={hardSkills}
          />
        </Grid>
        <Grid
          classes={{
            root: classes.skillsSectionWrapper,
          }}
          item
        >
          <SkillsSection
            chipColor="lightBlue"
            title="Soft Skills"
            skillsData={softSkills}
          />
        </Grid>
      </Grid>
    </>
  );
};

export default Skills;
