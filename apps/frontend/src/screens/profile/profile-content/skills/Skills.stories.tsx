import Box from '@mui/material/Box';
import React from 'react';

import Skills from './Skills';

export default {
  title: 'Components/Skills',
  component: Skills,
};

export const Default = () => {
  return (
    <>
      <Box mb={4}>
        <Skills />
      </Box>
    </>
  );
};
