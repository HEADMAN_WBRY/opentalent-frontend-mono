import { OperationVariables, useLazyQuery } from '@apollo/client';
import { GET_CURRENT_TALENT } from 'graphql/talents';
import { useCurrentUser } from 'hooks/auth/useCurrentUser';
import { useSnackbar } from 'notistack';
import { useCallback, useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import { pathManager } from 'routes';

import { Query } from '@libs/graphql-types';

import { COMPANY_FLOW_PROFILE_DATA } from './queries';

const useTalentData = (talentId: string) => {
  const history = useHistory();
  const { enqueueSnackbar } = useSnackbar();
  const { data: currentCompanyData } = useCurrentUser();
  const companyId = currentCompanyData?.currentCompanyUser.id;
  const [request, { loading, data }] = useLazyQuery<Query, OperationVariables>(
    COMPANY_FLOW_PROFILE_DATA,
    {
      variables: {
        talent_id: talentId,
        company_id: companyId,
      },
      fetchPolicy: 'network-only',
      onCompleted: ({ talent }) => {
        if (!talent) {
          history.push(pathManager.company.workforce.generatePath());
        }
      },
      onError: (error) => {
        enqueueSnackbar(error.message, {
          variant: 'error',
          preventDuplicate: true,
        });
      },
    },
  );

  const fetchTalent = useCallback(() => {
    request({
      variables: {
        talent_id: talentId,
        company_id: companyId,
      },
    });
  }, [companyId, request, talentId]);

  return { loading, talent: data?.talent, fetchTalent, data };
};

const useCurrentTalentData = () => {
  const [request, { loading, data }] = useLazyQuery<Query, never>(
    GET_CURRENT_TALENT,
    {
      fetchPolicy: 'network-only',
    },
  );

  const fetchTalent = useCallback(() => {
    request();
  }, [request]);

  return { loading, talent: data?.currentTalent, fetchTalent, data };
};

export const useProfileInfo = (talentId?: string) => {
  const useTalent = talentId ? useTalentData : useCurrentTalentData;
  const result = useTalent(talentId as string);
  const { fetchTalent } = result;

  useEffect(() => {
    fetchTalent();
  }, [fetchTalent]);

  return result;
};
