import { Formik } from 'formik';
import React from 'react';
import { DefaultModalProps, withLocationStateModal } from 'utils/modals';
import * as yup from 'yup';

import { Alert } from '@mui/lab';
import { Box, Grid } from '@mui/material';
import { makeStyles } from '@mui/styles';

import { TalentCategoriesDocument } from '@libs/graphql-types';
import { modelPath } from '@libs/helpers/form';
import Button from '@libs/ui/components/button';
import {
  ConnectedGraphSelect,
  ConnectedSelect,
} from '@libs/ui/components/form/select';
import { ConnectedTextField } from '@libs/ui/components/form/text-field';
import { DefaultModal } from '@libs/ui/components/modals';
import Typography from '@libs/ui/components/typography';

import { ChatModals } from '../types';
import { MESSAGE_TYPES_OPTIONS } from './consts';
import { useSubmitAction } from './hooks';
import { FormModel } from './types';

const useStyles = makeStyles(() => ({
  paper: {
    width: 620,
  },
  content: {
    textAlign: 'left',
  },
}));

const validation = yup.object().shape({
  category: yup.string().nullable(),
  messageType: yup.string().nullable().required(),
  message: yup.string().nullable().trim().required(),
});

interface MessageToWorkforceProps {}

const MessageToWorkforce = ({
  modalData,
  isOpen,
  close,
}: DefaultModalProps<MessageToWorkforceProps>) => {
  const classes = useStyles();
  const { onSubmit, loading } = useSubmitAction({ onSuccess: close });

  return (
    <Formik<FormModel>
      initialValues={{ category: '', messageType: '', message: '' } as any}
      onSubmit={onSubmit}
      validationSchema={validation}
    >
      {({ handleSubmit }) => (
        <DefaultModal
          handleClose={close}
          open={isOpen}
          title="Message My Workforce"
          className={classes.paper}
          actions={
            <Grid spacing={4} justifyContent="space-between" container>
              <Grid xs={6} item>
                <Button
                  fullWidth
                  color="primary"
                  variant="contained"
                  onClick={() => handleSubmit()}
                  disabled={loading}
                >
                  Send Message
                </Button>
              </Grid>
              <Grid xs={6} item>
                <Button
                  fullWidth
                  variant="contained"
                  onClick={close}
                  disabled={loading}
                >
                  Cancel
                </Button>
              </Grid>
            </Grid>
          }
        >
          <Box pb={6}>
            <Typography>Send a mass message to My workforce.</Typography>
          </Box>

          <Grid
            className={classes.content}
            container
            spacing={4}
            direction="column"
          >
            <Grid item>
              <ConnectedSelect
                formControlProps={{ size: 'small' }}
                fullWidth
                hideNoneValue
                name={modelPath<FormModel>((m) => m.messageType)}
                variant="filled"
                label="Select type of message"
                options={MESSAGE_TYPES_OPTIONS}
              />
            </Grid>
            <Grid item>
              <ConnectedGraphSelect
                query={TalentCategoriesDocument}
                dataPath="talentCategories"
                formControlProps={{ size: 'small' }}
                fullWidth
                name={modelPath<FormModel>((m) => m.category)}
                variant="filled"
                label="Filter by Category"
              />
            </Grid>
            <Grid item>
              <ConnectedTextField
                fullWidth
                size="small"
                variant="filled"
                multiline
                label="Enter message"
                name={modelPath<FormModel>((m) => m.message)}
                rows={6}
              />
            </Grid>
          </Grid>
          <br />
          {loading && <Alert color="info">Operation in progress</Alert>}
        </DefaultModal>
      )}
    </Formik>
  );
};

export const MessageToWorkforceModal =
  withLocationStateModal<MessageToWorkforceProps>({
    id: ChatModals.MessageToWorkforce,
  })(MessageToWorkforce);
